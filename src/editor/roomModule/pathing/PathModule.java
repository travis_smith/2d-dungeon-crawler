// ================================================================================================
// Class: PathModule
// Primary Author: Travis Smith
// Last Updated: Jan 09, 2014
// ================================================================================================
// The PathModule provides the user with the UI to set the pathing flag on a tile or an area
// of tiles.
// ================================================================================================

package editor.roomModule.pathing;

import java.awt.Point;

import editor.roomModule.tileMap.TileInstruction;

public class PathModule
{
	// ============================================================================================
	// Data Members
	// ============================================================================================

	// Area Creation Members
	private Point startPoint;
	private Point endPoint;
	
	// GUI
	private PathModuleGUI moduleGUI;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public PathModule()
	{
		// Initialize Setting Members
		this.startPoint = null;
		this.endPoint = null;	
		
		this.moduleGUI = new PathModuleGUI();
	}	
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void sendMessage(String msg)
	{
		// TODO
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public TileInstruction tileLeftClicked(Point tileLocation, boolean shiftPressed)
	{
		if(shiftPressed)
		{
			if(startPoint == null)
			{
				sendMessage("Place Second Corner");
				startPoint = new Point(tileLocation);
				return null;
			}
			else
			{
				endPoint = new Point(tileLocation);
				sendMessage("Done");
				TileInstruction instruction = TileInstruction.buildAddPathAreaInstruction(startPoint, endPoint);

				startPoint = null;
				endPoint = null;
				
				return instruction;
			}				
		}
		else
		{
			startPoint = null;
		
			return TileInstruction.buildAddPathAreaInstruction(tileLocation, tileLocation);
		}
	}
	
	public TileInstruction tileRightClicked(Point tileLocation, boolean shiftPressed)
	{
		if(shiftPressed)
		{
			if(startPoint == null)
			{
				sendMessage("Place Second Corner");
				startPoint = new Point(tileLocation);
				return null;
			}
			else
			{
				endPoint = new Point(tileLocation);
				sendMessage("Done");
				
				TileInstruction instruction = TileInstruction.buildRemovePathAreaInstruction(startPoint, endPoint);

				startPoint = null;
				endPoint = null;
				
				return instruction;			
			}				
		}
		else
		{
			startPoint = null;
			
			return TileInstruction.buildRemovePathAreaInstruction(tileLocation, tileLocation);
		}
	}
	
	public PathModuleGUI getGUI()
	{
		return moduleGUI;
	}
}
