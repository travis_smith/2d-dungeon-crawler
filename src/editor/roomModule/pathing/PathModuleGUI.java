package editor.roomModule.pathing;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PathModuleGUI extends JInternalFrame
{
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected PathModuleGUI()
	{
		super(); 

		// Set-Up Main Panel
		JPanel mainContent = new JPanel();
		mainContent.setFocusable(false);
		mainContent.setLayout(new BorderLayout());
		mainContent.add(buildInstructionPanel(), BorderLayout.CENTER);
			
		// Set-Up Internal Frame
		this.setFocusable(false);
		this.setTitle("Pathing");
		this.setClosable(false);
		this.setContentPane(mainContent);
		this.pack();
		this.setVisible(false);			
	}	
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================

	private JPanel buildInstructionPanel()
	{
		String placeSingleText = "Left-Click to set Tiles as Pathable";
		String removeSingleText = "Right-Click to set Tiles as Un-Pathable";
		String shiftCommentOne = "Holding SHIFT while clicking will create an area";
		String shiftCommentTwo = "between two clicked tiles. The area will be";
		String shiftCommentThree = "defined by the second click type.";
		
		JLabel plcSingleField = new JLabel(placeSingleText);
		JLabel rmvSingleField = new JLabel(removeSingleText);
		JLabel shiftFieldOne = new JLabel(shiftCommentOne);
		JLabel shiftFieldTwo = new JLabel(shiftCommentTwo);
		JLabel shiftFieldThree = new JLabel(shiftCommentThree);
		
		JPanel msgPanel = new JPanel();
		msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.Y_AXIS));
		msgPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		

		
		msgPanel.add(Box.createVerticalStrut(3));
		msgPanel.add(plcSingleField);
		msgPanel.add(Box.createVerticalStrut(10));
		msgPanel.add(rmvSingleField);
		msgPanel.add(Box.createVerticalStrut(10));
		msgPanel.add(shiftFieldOne);
		msgPanel.add(shiftFieldTwo);
		msgPanel.add(shiftFieldThree);
		msgPanel.add(Box.createVerticalStrut(3));
		
		return msgPanel;
	}
}
