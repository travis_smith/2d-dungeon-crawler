// ================================================================================================
// Class: RoomKit
// Primary Author: Travis Smith
// Last Updated: Jan 18, 2014
// ================================================================================================
// The RoomKit is used to save ObjectKits and TileData for a specific room. The kits are created
// inside the editor and are sent to the Exporter when finished to generate a DungeonRoom for the
// engine. RoomKits should only be instanced to save the data and should not be modified directly. 
// ================================================================================================

package editor.roomModule;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import editor.roomModule.objects.ObjectKit;
import editor.roomModule.tileMap.TileData;

public class RoomKit implements Serializable
{
	// Version
	private static final long serialVersionUID = 1L;
	
	// Size Constants
	public static final int ROOM_COLS = 30;
	public static final int ROOM_ROWS = 30;
	
	// Room Members
	private String name;
	private TileData tileData[][];
	private ArrayList<ObjectKit> objectKits;
	private int objectIdCode;
	
	// Constructor
	public RoomKit(String name, TileData[][] tileData, ArrayList<ObjectKit> objectKits, int objectIdCode)
	{
		this.name = name;
		this.tileData = tileData;
		this.objectKits = objectKits;
		this.objectIdCode = objectIdCode;
	}
	
	//Getters
	public String getName()
	{
		return name;
	}
	public TileData[][] getTileData()
	{
		return tileData;
	}
	public ArrayList<ObjectKit> getObjectKits()
	{
		return objectKits;
	}
	public int getObjectIdCode()
	{
		return objectIdCode;
	}

	// I/O Methods
	public void saveRoom()
	{
		String filePath = new String("resources\\rooms\\" + this.getName());
		File fileName = new File(filePath);

		try 
		{
			FileOutputStream outStream = new FileOutputStream(fileName);
			
			try 
			{
				ObjectOutputStream save = new ObjectOutputStream(outStream);
				save.writeObject(this);
				save.flush();
				save.close();
			} 
			catch (InvalidClassException e) 
			{
				System.err.println("Invalid Class");
			}
			catch (IOException e) 
			{
				System.err.println("Error Starting Object Stream");
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println("Error With File Stream");
		}
	}
	public String saveRoomAs()
	{
		File filePath = new File("resources\\rooms\\");

		// Set-Up File Chooser Frame
		JFrame saveFrame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(filePath);
		int returnVal;
		File fileName;
		
		returnVal = fileChooser.showSaveDialog(saveFrame);
		
		// Check User Selection and Load File
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			fileName = new File(fileChooser.getSelectedFile() + ".rmk");
			
			try 
			{
				FileOutputStream outStream = new FileOutputStream(fileName);
				
				try 
				{
					ObjectOutputStream save = new ObjectOutputStream(outStream);
					save.writeObject(this);
					save.flush();
					save.close();
					
					return fileName.getName();
				} 
				catch (InvalidClassException e) 
				{
					System.err.println("Invalid Class");
					return null;
				}
				catch (IOException e) 
				{
					System.err.println("Error Starting Object Stream");
					return null;
				}
			} 
			catch (FileNotFoundException e) 
			{
				System.err.println("Error With File Stream");
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public static RoomKit loadRoom()
	{
		File filePath = new File("resources\\rooms\\");

		// Set-Up File Chooser Frame
		JFrame loadFrame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(filePath);
		int returnVal;
		File fileName;
		
		returnVal = fileChooser.showOpenDialog(loadFrame);
		
		// Check User Selection and Load File
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			fileName = fileChooser.getSelectedFile();

			try
			{
				FileInputStream inStream = new FileInputStream(fileName);
				ObjectInputStream load = new ObjectInputStream(inStream);

				RoomKit newRoom = (RoomKit)load.readObject();
				load.close();
			
				return newRoom;				
			}
			catch (FileNotFoundException e)
			{
				System.err.println(fileName.toString() + " :: was not Found");
				return null;
			} 
			catch (IOException e) 
			{
				System.err.println("IO Exception");
				return null;
			} 
			catch (ClassNotFoundException e) 
			{
				System.err.println("Class Not Found Exception");
				return null;
			}		
		}	
		else
		{
			return null;
		}
	}
	public static RoomKit loadRoom(String roomName)
	{
		String pathString = "resources\\rooms\\";
		File filePath = new File(pathString.concat(roomName));

		try
		{
			FileInputStream inStream = new FileInputStream(filePath);
			ObjectInputStream load = new ObjectInputStream(inStream);

			RoomKit newRoom = (RoomKit)load.readObject();
			load.close();
			
			return newRoom;				
		}
		catch (FileNotFoundException e)
		{
			System.out.println(filePath.toString() + " :: was not Found");
			return null;
		} 
		catch (IOException e) 
		{
			System.out.println("IO Exception");
			return null;
		} 
		catch (ClassNotFoundException e) 
		{
			System.out.println("Class Not Found Exception");
			return null;
		}		
	}
}
