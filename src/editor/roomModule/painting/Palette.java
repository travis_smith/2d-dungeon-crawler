// ================================================================================================
// Class: Palette
// Primary Author: Travis Smith
// Last Updated: Jan 07, 2014
// ================================================================================================
// The Palette will be used to hold an array of PaletteButtons. It will create and display a 
// PaletteButton for each tile in the editorTileSheet. 
// ================================================================================================

package editor.roomModule.painting;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import editor.roomModule.tileMap.TileData;

@SuppressWarnings("serial")
public class Palette extends JPanel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size Constants
	private static final int PALETTE_MARGIN = 5;
	private static final int SCROLL_WIDTH = ((4 * PaletteButton.BUTTON_WIDTH) + (5 * PALETTE_MARGIN) + 18);
	private static final int SCROLL_HEIGHT = ((4 * PaletteButton.BUTTON_HEIGHT) + (5 * PALETTE_MARGIN));
		
	// Data Members
	private PaintModule parentWindow;
	private BufferedImage editorTilesPtr;
	private PaletteButton paletteButtons[][];

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected Palette(PaintModule parentWindow, BufferedImage editorTileSheet)
	{
		super();
		
		// Initialize Data Members
		this.parentWindow = parentWindow;
		this.editorTilesPtr = editorTileSheet;
		
		// Initialize Buttons
		initializePaletteButtons();
		
		// Set-Up Main Panel		
		this.setLayout(new BorderLayout());
		this.add(buildScrollPane(), BorderLayout.CENTER);		
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		this.setFocusable(false);	
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void initializePaletteButtons()
	{
		int tileSheetCols = (editorTilesPtr.getWidth() / TileData.TILE_WIDTH);
		int tileSheetRows = (editorTilesPtr.getHeight() / TileData.TILE_HEIGHT);
		
		paletteButtons = new PaletteButton[tileSheetCols][tileSheetRows];
	
		for(int rIndex = 0; rIndex < tileSheetRows; rIndex++)
		{
			for(int cIndex = 0; cIndex < tileSheetCols; cIndex++)
			{
				paletteButtons[cIndex][rIndex] = new PaletteButton(this, editorTilesPtr, new Point(cIndex, rIndex));
			}
		}
	}
	
	private JScrollPane buildScrollPane()
	{
		JScrollPane paletteScroll = new JScrollPane(buildPaletteButtonPanel());
		paletteScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		paletteScroll.setWheelScrollingEnabled(true);
		paletteScroll.setPreferredSize(new Dimension(SCROLL_WIDTH, SCROLL_HEIGHT));	
		
		return paletteScroll;
	}
	
	private JPanel buildPaletteButtonPanel()
	{	
		// Set-Up Tile Panel
		JPanel palettePanel = new JPanel();
		palettePanel.setLayout(new GridLayout(0, 4, PALETTE_MARGIN, PALETTE_MARGIN));

		int tileSheetCols = (editorTilesPtr.getWidth() / TileData.TILE_WIDTH);
		int tileSheetRows = (editorTilesPtr.getHeight() / TileData.TILE_HEIGHT);
		
		// Add PaletteButtons to palettePanel
		for(int rIndex = 0; rIndex < tileSheetRows; rIndex++)
		{
			for(int cIndex = 0; cIndex < tileSheetCols; cIndex++)
			{
				palettePanel.add(paletteButtons[cIndex][rIndex]);			
			}

		}
		
		return palettePanel;
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void buttonClicked(PaletteButton button)
	{
		parentWindow.paletteButtonClicked(button);
	}
}
