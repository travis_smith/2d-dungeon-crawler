// ================================================================================================
// Class: PaletteButton
// Primary Author: Travis Smith
// Last Updated: Jan 07, 2014
// ================================================================================================
// The PaletteButton will be used to put a specific image on the editors "paint brush". Each button
// will be created with the coordinates of a Tile and a reference to the editor's tile sprite sheet.
// These buttons will then be added to a palette and displayed inside the editor so that the user
// can select the graphic they want and then "paint" it on the proper TileGUI in the TileWindow. 
// ================================================================================================

package editor.roomModule.painting;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

import editor.roomModule.tileMap.TileData;

@SuppressWarnings("serial")
public class PaletteButton extends JButton
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Constants
	public static final int BUTTON_WIDTH = 64;
	public static final int BUTTON_HEIGHT = 64;
	
	// Members
	private Point imageLoc;
	private BufferedImage editorTilesPtr;
	private Palette palette;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected PaletteButton(Palette palette, BufferedImage editorTiles, Point imageLoc)
	{	
		super();
		
		this.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMaximumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMinimumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setFocusable(false);
		
		this.palette = palette;
		this.editorTilesPtr = editorTiles;
		this.imageLoc = imageLoc;
		
		this.addActionListener(new ActionListener() 
		{		
			public void actionPerformed(ActionEvent click)
			{
				clicked();
			}
		
		});	
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void clicked()
	{
		palette.buttonClicked(this);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void paint(Graphics gfx)
	{
		if((editorTilesPtr == null) || (imageLoc == null))
		{
			gfx.setColor(Color.BLACK);
			gfx.drawRect(this.getLocation().x, this.getLocation().y, (this.getWidth() - 1), (this.getHeight() - 1));
		}
		else
		{
			gfx.drawImage(editorTilesPtr, 0, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
						 (imageLoc.x * TileData.TILE_WIDTH), (imageLoc.y * TileData.TILE_HEIGHT), ((imageLoc.x + 1) * TileData.TILE_WIDTH), ((imageLoc.y + 1) * TileData.TILE_HEIGHT), null);
		}
	}
	
	// Setters
	public void setImageLocation(Point imageLoc)
	{
		this.imageLoc = imageLoc;
	}
	
	// Getters
	public Point getImageLocation()
	{
		return imageLoc;
	}
}
