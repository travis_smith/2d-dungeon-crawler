// ================================================================================================
// Class: PaintModule
// Primary Author: Travis Smith
// Last Updated: Jan 09, 2014
// ================================================================================================
// The PaintModule is an extended JInternalFrame that displays options associated with "painting"
// the tiles in the EditRoom. The PaletteWindow will maintain a palette containing tile image data
// and provide that data to the MainEditor when the user "paints" a tile. The PaletteWindow will 
// also contain methods that automatically paint rooms and hallways to speed the painting process. 
// ================================================================================================

package editor.roomModule.painting;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import editor.roomModule.tileMap.TileInstruction;

@SuppressWarnings("serial")
public class PaintModule extends JInternalFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Editor TileSheet Location Paths
	public static final String EDITOR_TILES_PATH = "/Tiles/editorTiles.png";
	public static final String EDITOR_OBJECT_TILES_PATH = "/Tiles/editorObjectTiles.png";
	
	// AutoPaint Members
	private boolean autoPainting;
	private int autoMode;
	private Point startPoint;
	private Point endPoint;
	
	// AutoPaint GUI
	private JTextField messageField;
	private JButton outRoomButton;
	private JButton inRoomButton;
	private JButton vertHallButton;
	private JButton horizHallButton;
	private JButton cancelButton;
	
	// Tabbed Pane
	private JTabbedPane mainContent;
	
	// Brush Preview
	private PaletteButton brushPreview;
	private Point brushCoordinates;
	
	// Image Data
	private BufferedImage editorTiles;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public PaintModule(BufferedImage editorTiles)
	{
		super();

		// Initialize AutoPaint Data
		this.autoPainting = false;
		this.autoMode = (-1);
		this.startPoint = null;
		this.endPoint = null;

		// Initialize Paint Data
		this.brushCoordinates = null;
		this.editorTiles = editorTiles;
		
		// Set-Up Main Content Pane
		mainContent = new JTabbedPane();
		mainContent.setFocusable(false);
		mainContent.addTab("Auto-Paint", buildAutoPaintTab());
		mainContent.addTab("Manual", buildManualPaintTab());
			
		// Set-Up Internal Frame
		this.setFocusable(false);
		this.setTitle("Painting");
		this.setClosable(false);
		this.setContentPane(mainContent);
		this.pack();
		this.setVisible(false);
	}	
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildAutoPaintTab()
	{
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.Y_AXIS));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(0, 1, 0, 3));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		outRoomButton = new JButton("Paint Outer Room");
		outRoomButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if((!autoPainting) && (autoMode != TileInstruction.PAINT_OUTER_ROOM))
				{
					startAutoPaint(TileInstruction.PAINT_OUTER_ROOM);
				}
			}
		});
		
		inRoomButton = new JButton("Paint Inner Room");
		inRoomButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if((!autoPainting) && (autoMode != TileInstruction.PAINT_INNER_ROOM))
				{
					startAutoPaint(TileInstruction.PAINT_INNER_ROOM);
				}
			}
		});
		
		vertHallButton = new JButton("Paint Vertical Hall");
		vertHallButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if((!autoPainting) && (autoMode != TileInstruction.PAINT_VERT_HALL))
				{
					startAutoPaint(TileInstruction.PAINT_VERT_HALL);
				}
			}
		});
		
		horizHallButton = new JButton("Paint Horizontal Hall");
		horizHallButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if((!autoPainting) && (autoMode != TileInstruction.PAINT_HORIZ_HALL))
				{
					startAutoPaint(TileInstruction.PAINT_HORIZ_HALL);
				}
			}
		});
		
		cancelButton = new JButton("Cancel Current");
		cancelButton.setEnabled(false);
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(autoPainting)
				{
					messageField.setText("Canceled Auto-Paint");
					stopAutoPaint();
				}
			}
		});
		
		buttonPanel.add(outRoomButton);
		buttonPanel.add(inRoomButton);
		buttonPanel.add(vertHallButton);
		buttonPanel.add(horizHallButton);
		buttonPanel.add(Box.createVerticalStrut(1));
		buttonPanel.add(cancelButton);
	
		optionPanel.add(buildAutoMessagePanel());
		optionPanel.add(buttonPanel);
		optionPanel.add(Box.createVerticalGlue());
		
		return optionPanel;
	}
	
	private JPanel buildAutoMessagePanel()
	{
		JPanel msgPanel = new JPanel();
		msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.Y_AXIS));
		msgPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		
		messageField = new JTextField("Messages");
		messageField.setMaximumSize(new Dimension(500, 50));
		messageField.setHorizontalAlignment(JTextField.CENTER);
		messageField.setEditable(false);
		
		msgPanel.add(messageField);
		return msgPanel;
	}
	
	private JPanel buildManualPaintTab()
	{
		Palette palette = new Palette(this, editorTiles);
		
		JPanel manualTab = new JPanel();
		manualTab.setLayout(new BoxLayout(manualTab, BoxLayout.Y_AXIS));	
		
		manualTab.add(buildBrushMainPanel());
		manualTab.add(palette);
		
		return manualTab;
	}
	
	private JPanel buildBrushMainPanel()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
				, BorderFactory.createMatteBorder(2, 0, 2, 0, Color.GRAY)));
		
		panel.add(buildBrushPreviewPanel());
		
		return panel;
	}
	
	private JPanel buildBrushPreviewPanel()
	{
		// Set-Up Brush Preview
		brushPreview = new PaletteButton(null, editorTiles, null);
		brushPreview.setEnabled(false);
		
		JPanel previewPanel = new JPanel();
		previewPanel.add(new JLabel("Current Brush: "));
		previewPanel.add(brushPreview);
		
		return previewPanel;
	}
	
	private void startAutoPaint(int newMode)
	{
		autoPainting = true;
		autoMode = newMode;
		startPoint = null;
		endPoint = null;
		mainContent.setEnabledAt(1, false);
		messageField.setText("Place First Corner");
		
		if(autoMode == TileInstruction.PAINT_OUTER_ROOM)
		{
			outRoomButton.setEnabled(true);
			inRoomButton.setEnabled(false);
			vertHallButton.setEnabled(false);
			horizHallButton.setEnabled(false);
			cancelButton.setEnabled(true);	
		}
		else if(autoMode == TileInstruction.PAINT_INNER_ROOM)
		{
			outRoomButton.setEnabled(false);
			inRoomButton.setEnabled(true);
			vertHallButton.setEnabled(false);
			horizHallButton.setEnabled(false);
			cancelButton.setEnabled(true);	
		}
		else if(autoMode == TileInstruction.PAINT_VERT_HALL)
		{
			outRoomButton.setEnabled(false);
			inRoomButton.setEnabled(false);
			vertHallButton.setEnabled(true);
			horizHallButton.setEnabled(false);
			cancelButton.setEnabled(true);	
		}
		else if(autoMode == TileInstruction.PAINT_HORIZ_HALL)
		{
			outRoomButton.setEnabled(false);
			inRoomButton.setEnabled(false);
			vertHallButton.setEnabled(false);
			horizHallButton.setEnabled(true);
			cancelButton.setEnabled(true);	
		}
	}
	
	private void stopAutoPaint()
	{		
		autoPainting = false;
		autoMode = (-1);
		startPoint = null;
		endPoint = null;
		mainContent.setEnabledAt(1, true);
		
		outRoomButton.setEnabled(true);
		inRoomButton.setEnabled(true);
		vertHallButton.setEnabled(true);
		horizHallButton.setEnabled(true);
		cancelButton.setEnabled(false);	
	}
		
	// ============================================================================================
	// Proteced Methods
	// ============================================================================================
	
	protected void paletteButtonClicked(PaletteButton button)
	{	
		if(button != null)
		{
			brushCoordinates = new Point(button.getImageLocation());
			brushPreview.setImageLocation(brushCoordinates);
			brushPreview.repaint();
		}
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public TileInstruction tileLeftClicked(Point tileLocation, boolean shiftPressed)
	{
		if(autoPainting)
		{
			if(startPoint == null)
			{
				messageField.setText("Place Second Corner");
				startPoint = new Point(tileLocation);
				return null;
			}
			else
			{
				endPoint = new Point(tileLocation);
				messageField.setText("");
				
				TileInstruction instruction = TileInstruction.buildAutoPaintInstruction(autoMode, startPoint, endPoint);
				stopAutoPaint();
				
				return instruction;
			}
		}
		else if(brushCoordinates != null)
		{
			if(shiftPressed)
			{
				if(startPoint == null)
				{
					messageField.setText("Place Second Corner");
					startPoint = new Point(tileLocation);
					return null;
				}
				else
				{
					endPoint = new Point(tileLocation);
					messageField.setText("");
					
					TileInstruction instruction = TileInstruction.buildPaintAreaInstruction(startPoint, endPoint, brushCoordinates);
					stopAutoPaint();
					
					return instruction;
				}				
			}
			else
			{
				startPoint = null;

				return TileInstruction.buildPaintSingleInstruction(tileLocation, brushCoordinates);
			}
		}
		else
		{
			return null;
		}
	}
	
	public TileInstruction tileRightClicked(Point tileLocation, boolean shiftPressed)
	{
		if(!autoPainting)
		{
			if(shiftPressed)
			{
				if(startPoint == null)
				{
					messageField.setText("Place Second Corner");
					startPoint = new Point(tileLocation);
					return null;
				}
				else
				{
					endPoint = new Point(tileLocation);
					messageField.setText("");
					
					TileInstruction instruction = TileInstruction.buildRemovePaintAreaInstruction(startPoint, endPoint);
					stopAutoPaint();
					
					return instruction;
				}				
			}
			else
			{
				startPoint = null;
				
				return TileInstruction.buildRemovePaintSingleInstruction(tileLocation);
			}
		}
		else
		{
			stopAutoPaint();
			return null;
		}
	}
	
	public void resetAutoPaint()
	{
		stopAutoPaint();
	}
}
