// ================================================================================================
// Class: ObjectModule
// Primary Author: Travis Smith
// Last Updated: Jan 12, 2014
// ================================================================================================
// ObjectModule will control all methods and data related to the creation, modification, and
// placement of ObjectKits. When a new RoomKit is started, this Module will create an empty array
// of ObjectKits and the user will be able to create new kits and add them to the array. When the
// RoomKit is saved, the ObjectKit array can be retrieved through a getter function and saved with the
// RoomKit. If editing an already saved RoomKit, the ObjectKit array will be initialized with the 
// existing array contained in the RoomKit.
// ================================================================================================

package editor.roomModule.objects;

import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import editor.roomModule.RoomEditorMain;
import editor.roomModule.tileMap.TileInstruction;

public class ObjectModule
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Button Command Constants
	public static final int CREATE = 0;
	public static final int EDIT = 1;
	public static final int PLACE = 2;
	public static final int DELETE = 3;
	
	// Main Editor
	private RoomEditorMain control;
	
	// Data Members
	private ArrayList<ObjectKit> objectKits;
	private boolean placing;
	private ObjectKit selectedKit;
	
	// GUI
	private ObjModuleGUI moduleGUI;
	
	// Unique ID Counter
	private final int ID_START = 101;
	private int idCounter;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public ObjectModule(RoomEditorMain control)
	{
		this.control = control;
		this.objectKits = new ArrayList<ObjectKit>(0);
		this.placing = false;
		this.selectedKit = null;
		this.moduleGUI = new ObjModuleGUI(this);
		this.idCounter = ID_START;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void createNewKit()
	{
		ObjectEditor editor = new ObjectEditor(objectKits);
		
		// Create Pop-up and get user input
		int optionChoice = JOptionPane.showConfirmDialog(null, editor, "Create Object", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		
		if(optionChoice == JOptionPane.OK_OPTION)
		{	
			objectKits.add(editor.buildNewKit(idCounter));
			idCounter++;
			moduleGUI.updateTable();		
		}			
	}
	
	private void editKitAtIndex(int kitIndex)
	{
		ObjectEditor editor = new ObjectEditor(objectKits, kitIndex);
		
		// Create Pop-up and get user input
		int optionChoice = JOptionPane.showConfirmDialog(null, editor, "Edit Object", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		
		if(optionChoice == JOptionPane.OK_OPTION)
		{	
			editor.updatePassedKit();
			resetObjectLocation(kitIndex);	
			moduleGUI.updateTable();
		}			
	}
	
	private void deleteKit(int kitIndex)
	{
		resetObjectLocation(kitIndex);
		
		// Remove object from ObjectTrigger Arrays and Move Trigger Destinations
		int kitID = objectKits.get(kitIndex).getIdNumber();
		
		for(int index = 0; index < objectKits.size(); index++)
		{
			if(objectKits.get(index).getObjTriggerTargetIDs().contains(kitID));
			{
				objectKits.get(index).getObjTriggerTargetIDs().remove((Integer)kitID);
			}
			
			if(objectKits.get(index).getMoveTriggerDestID() == kitID)
			{
				objectKits.get(index).setMoveTriggerDest(-1);
			}
		}
		
		objectKits.remove(kitIndex);
		moduleGUI.updateTable();
	}
	
	private void resetObjectLocation(int kitIndex)
	{	
		control.issueInstruction(TileInstruction.buildRemoveObjectInstruction(objectKits.get(kitIndex).getLocation()));
		objectKits.get(kitIndex).setLocation(null);
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void buttonPressed(int command, int kitIndex)
	{
		if(command == CREATE)
		{
			createNewKit();
		}
		else if(command == EDIT)
		{
			editKitAtIndex(kitIndex);
		}
		else if(command == PLACE)
		{
			if(!placing)
			{
				placing = true;
				selectedKit = objectKits.get(kitIndex);
				resetObjectLocation(kitIndex);
				moduleGUI.displayMessage("Click a Tile");
			}
		}
		else if(command == DELETE)
		{
			deleteKit(kitIndex);
		}
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void loadRoomData(ArrayList<ObjectKit> objectKits, int objectIdCode)
	{
		this.objectKits = objectKits;
		this.idCounter = objectIdCode;
		
		moduleGUI.updateTable();
	}
	
	public void newRoom()
	{
		this.objectKits = new ArrayList<ObjectKit>(0);
		this.placing = false;
		this.selectedKit = null;
		this.idCounter = ID_START;		
	}
	
	public void stopPlacing()
	{
		placing = false;
		selectedKit = null;
	}
	
	public ObjModuleGUI getGUI()
	{
		return moduleGUI;
	}
	
	public ArrayList<ObjectKit> getObjectKits()
	{
		return objectKits;
	}
	
	public int getObjectIdCode()
	{
		return idCounter;
	}
	
	public boolean isPlacing()
	{
		return placing;
	}
	
	public ObjectKit getSelectedKit()
	{
		return selectedKit;
	}
	
	public TileInstruction tileLeftClicked(Point clickLocation)
	{	
		if((placing) && (selectedKit != null))
		{	
			return TileInstruction.buildAddObjectInstruction(clickLocation, selectedKit);
		}
		else
		{
			return null;
		}
	}
	
	public TileInstruction tileRightClicked(Point clickLocation)
	{
		if((placing) && (selectedKit != null))
		{
			stopPlacing();
		}
		
		// Find Kit with matching coordinates and reset its' location
		for(int index = 0; index < objectKits.size(); index++)
		{
			Point tempLocation = objectKits.get(index).getLocation();
			
			if(tempLocation != null)
			{
				if(tempLocation.equals(clickLocation))
				{
					objectKits.get(index).setLocation(null);
					moduleGUI.updateTable();
					return TileInstruction.buildRemoveObjectInstruction(clickLocation);
				}
			}
		}
		
		return null;
	}
	
	public void placeSuccessful(Point location)
	{
		selectedKit.setLocation(location);
		placing = false;
		selectedKit = null;
		moduleGUI.updateTable();
	}
}
