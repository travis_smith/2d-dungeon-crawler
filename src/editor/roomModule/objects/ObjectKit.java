// ================================================================================================
// Class: ObjectCreationKit
// Primary Author: Travis Smith
// Last Updated: Dec 26, 2013
// ================================================================================================
// The ObjectCreationKit is used to pass all needed values to the ObjectFactory in order to create
// an instance of any RoomObject. It will function as the blueprint for the desired RoomObject.
// ================================================================================================

package editor.roomModule.objects;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

import objects.roomObjects.RoomObject;
import objects.roomObjects.hazards.MoveInstruction;

public class ObjectKit implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// Shared Data
	private String name;
	private int idNumber;
	private int objectType;
	private boolean usesGraphics;
	private boolean startEnabled;
	private Point location;
	
	// Trigger Data
	private boolean useOnce;
	private boolean enabler;
	private boolean disabler;
	private ArrayList<Integer> objTriggerTargetIDs;
	private int	moveTriggerDestID;
	
	// Hazard Data
	private int minDamage;
	private int maxDamage;
	private int timedInterval;
	private int moveRate;
	private ArrayList<MoveInstruction> moveInstructions;
	
	// Door || Hallway
	private boolean isVertical;
	
	// Hallway Data
	private int hallType;
	private int hallWidth;
	
	// Spawner Data
	private int spawnType;
	private int waveSize;
	private int spawnRate;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected ObjectKit(final int idNumber)
	{
		// Initialize Shared Data
		this.name = "No Name";
		this.idNumber = idNumber;
		this.objectType = (-1);
		this.usesGraphics = true;
		this.startEnabled = true;
		this.location = null;
		this.hallWidth = 0;
		
		// Initialize Trigger Data
		this.useOnce = true;
		this.enabler = false;
		this.disabler = false;
		this.objTriggerTargetIDs = new ArrayList<Integer>(0);
		this.moveTriggerDestID = (-1);
		
		// Initialize Hazard Data
		this.minDamage = 0;
		this.maxDamage = 0;
		this.timedInterval = 0;
		this.moveRate = 0;
		this.moveInstructions = new ArrayList<MoveInstruction>(0);
		
		// Initialize Door Data
		this.isVertical = false;
		
		// Initialize Hallway Data
		this.hallType = (-1);
		
		// Initialize Spawner Data
		this.spawnType = (-1);
		this.waveSize = 0;
		this.spawnRate = 0;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Getters
	public String getName()
	{
		return name;
	}

	public int getIdNumber()
	{
		return idNumber;
	}
	
	public int getObjectType()
	{
		return objectType;
	}

	public boolean isUsingGraphics()
	{
		return usesGraphics;
	}

	public boolean isStartingEnabled()
	{
		return startEnabled;
	}
	
	public Point getLocation()
	{
		return location;
	}

	public int getHallWidth()
	{
		return hallWidth;
	}

	public boolean isUseOnce()
	{
		return useOnce;
	}

	public boolean isEnabler()
	{
		return enabler;
	}

	public boolean isDisabler()
	{
		return disabler;
	}

	public ArrayList<Integer> getObjTriggerTargetIDs()
	{
		return objTriggerTargetIDs;
	}

	public int getMoveTriggerDestID()
	{
		return moveTriggerDestID;
	}

	public int getMinDamage()
	{
		return minDamage;
	}

	public int getMaxDamage()
	{
		return maxDamage;
	}

	public int getTimedInterval()
	{
		return timedInterval;
	}

	public int getMoveRate()
	{
		return moveRate;
	}
	
	public ArrayList<MoveInstruction> getMoveInstructions()
	{
		return moveInstructions;
	}

	public boolean isVertical()
	{
		return isVertical;
	}

	public int getHallType()
	{
		return hallType;
	}

	public int getSpawnType()
	{
		return spawnType;
	}

	public int getWaveSize()
	{
		return waveSize;
	}

	public int getSpawnRate()
	{
		return spawnRate;
	}
	
	public String getTypeString()
	{
		if(objectType == RoomObject.OBJ_TRIGGER)
		{
			return "Object Trigger";
		}
		else if(objectType == RoomObject.MOVE_TRIGGER)
		{
			return "Move Trigger";
		}
		else if(objectType == RoomObject.STATIC_HAZARD)
		{
			return "Static Hazard";
		}
		else if(objectType == RoomObject.TIMED_HAZARD)
		{
			return "Timed Hazard";
		}
		else if(objectType == RoomObject.MOVING_HAZARD)
		{
			return "Moving Hazard";
		}
		else if(objectType == RoomObject.SPAWNER)
		{
			return "Spawner";
		}
		else if(objectType == RoomObject.DOOR)
		{
			return "Door";
		}
		else if(objectType == RoomObject.HALLWAY)
		{
			return "Hallway";
		}
		else if(objectType == RoomObject.ROOM_GOAL)
		{
			return "Room Goal";
		}
		else
		{
			return "No Type String";
		}
	}

	
	// Setters
	public void setName(String name)
	{
		this.name = name;
	}

	public void setObjectType(int objectType)
	{
		this.objectType = objectType;
	}

	public void setUsesGraphics(boolean usesGraphics)
	{
		this.usesGraphics = usesGraphics;
	}

	public void setStartEnabled(boolean startEnabled)
	{
		this.startEnabled = startEnabled;
	}
	
	public void setLocation(Point location)
	{
		this.location = location;
	}

	public void setHallWidth(int hallWidth)
	{
		this.hallWidth = hallWidth;
	}

	public void setUseOnce(boolean useOnce)
	{
		this.useOnce = useOnce;
	}

	public void setEnabler(boolean enabler)
	{
		this.enabler = enabler;
	}

	public void setDisabler(boolean disabler)
	{
		this.disabler = disabler;
	}

	public void setObjTriggerTargets(ArrayList<Integer> objTriggerTargets)
	{
		this.objTriggerTargetIDs = objTriggerTargets;
	}

	public void setMoveTriggerDest(int moveTriggerDestID)
	{
		this.moveTriggerDestID = moveTriggerDestID;
	}

	public void setMinDamage(int minDamage)
	{
		this.minDamage = minDamage;
	}

	public void setMaxDamage(int maxDamage)
	{
		this.maxDamage = maxDamage;
	}

	public void setTimedInterval(int timedInterval)
	{
		this.timedInterval = timedInterval;
	}

	public void setMoveRate(int moveRate)
	{
		this.moveRate = moveRate;
	}
	
	public void setMoveInstructions(ArrayList<MoveInstruction> moveInstructions)
	{
		this.moveInstructions = moveInstructions;
	}

	public void setVertical(boolean isVertical)
	{
		this.isVertical = isVertical;
	}

	public void setHallType(int hallType)
	{
		this.hallType = hallType;
	}

	public void setSpawnType(int spawnType)
	{
		this.spawnType = spawnType;
	}

	public void setWaveSize(int waveSize)
	{
		this.waveSize = waveSize;
	}

	public void setSpawnRate(int spawnRate)
	{
		this.spawnRate = spawnRate;
	}
}
