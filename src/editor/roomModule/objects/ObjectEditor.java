// ================================================================================================
// Class: ObjectEditor
// Primary Author: Travis Smith
// Last Updated: Jan 10, 2014
// ================================================================================================
// ObjectEditor provides the GUI used as a Pop-Up to set the values of a new ObjectKit or change
// the values of an existing one.
// ================================================================================================

package editor.roomModule.objects;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import objects.roomObjects.Hallway;
import objects.roomObjects.RoomObject;
import objects.roomObjects.Spawner;
import objects.roomObjects.hazards.MoveInstruction;
import objects.roomObjects.hazards.MovingHazard;
import objects.roomObjects.hazards.TimedHazard;
import editor.roomModule.tileMap.TileData;

@SuppressWarnings("serial")
public class ObjectEditor extends JPanel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Engine Constants
	public static final int ENGINE_UPDATE_RATE = 60; // Change this if the Engine rate Changes
	
	// Option Constants
	public static final String OBJECT_TYPES[] = {"Object Trigger", "Move Trigger", "Static Hazard", "Timed Hazard"
												, "Moving Hazard", "Spawner", "Door", "Hallway", "Room Goal"};
	public static final String TIMED_INTERVALS[] = {"Half Second", "One Second", "Two Seconds"};
	public static final String MOVE_RATES[] = {"Slow", "Medium", "Fast"};
	public static final String MOB_TYPES[] = { "Minion", "Captain", "Boss" };
	public static final String WAVE_SIZES[] = { "1", "2", "3", "4", "5" };
	public static final String SPAWN_RATES[] = { "1 Spawn Every 3 Seconds", "1 Spawn Every 2 Seconds", "1 Spawn Every Second", "2 Spawns a Second" };
	public static final String HALL_WIDTHS[] = {"4 Tiles", "5 Tiles", "6 Tiles"};
	
	// ObjectCreationKits
	private ArrayList<ObjectKit> allObjectKits;
	private ObjectKit passedKit;
	private ArrayList<ObjectKit> objTriggerTargets;
	private ArrayList<ObjectKit> moveTriggers;
	
	// Shared Data GUI
	private JTextField nameBox;
	private JCheckBox enabledBox;
	private JCheckBox useGraphicsBox;
	private JComboBox<String> objectTypeBox;
	
	// Object Trigger GUI
	private JPanel objectTriggerPanel;
	private JCheckBox useOnceBoxOT;
	private JCheckBox willEnableBox;
	private JCheckBox willDisableBox;
	
	// Move Trigger GUI
	private JPanel moveTriggerPanel;
	private JCheckBox useOnceBoxMT;
	private JComboBox<String> moveTriggersBox;
	
	// Hazard GUI Members
	private JPanel timedHazardPanel;
	private JPanel movingHazardPanel;
	private JComboBox<String> timedIntervalBox;
	private JComboBox<String> movingRateBox;
	private ArrayList<MoveInstruction> moveInstructions;	
	
	// Spawner GUI Members
	private JPanel spawnerPanel;
	private JComboBox<String> typeBox;
	private JComboBox<String> sizeBox;
	private JComboBox<String> rateBox;
	
	// Door GUI
	private JPanel doorPanel;
	private JCheckBox doorVertical;
	
	// Hallway GUI Members
	private JPanel hallwayPanel;
	private JRadioButton entranceButton;
	private JRadioButton exitButton;
	private JRadioButton eitherButton;
	private JCheckBox hallVertical;
	private JComboBox<String> hallWidthBox;
	
	// Table Panel Members
	private JPanel tableCardPanel;
	private CardLayout tableCards;
	private JTable objectTriggerTable;
	private JTable moveTable;
	
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected ObjectEditor(final ArrayList<ObjectKit> allObjectKits)
	{
		super();
		this.allObjectKits = allObjectKits;
		this.passedKit = null;
		this.objTriggerTargets = new ArrayList<ObjectKit>(0);
		this.moveTriggers = new ArrayList<ObjectKit>(0);
		
		for(int index = 0; index < allObjectKits.size(); index++)
		{
			if(allObjectKits.get(index).getObjectType() == RoomObject.MOVE_TRIGGER)
			{
				moveTriggers.add(allObjectKits.get(index));
			}
		}
		
		buildMainPanel();
	}
	
	protected ObjectEditor(final ArrayList<ObjectKit> allObjectKits, int editKitIndex)
	{
		super();
		this.allObjectKits = allObjectKits;
		this.passedKit = allObjectKits.get(editKitIndex);
		this.objTriggerTargets = new ArrayList<ObjectKit>(0);
		
		for(int tIndex = 0; tIndex < passedKit.getObjTriggerTargetIDs().size(); tIndex++)
		{
			for(int aIndex = 0; aIndex < allObjectKits.size(); aIndex++)
			{
				if(allObjectKits.get(aIndex).getIdNumber() == passedKit.getObjTriggerTargetIDs().get(tIndex))
				{
					objTriggerTargets.add(allObjectKits.get(aIndex));
				}
			}
		}
		
		this.moveTriggers = new ArrayList<ObjectKit>(0);
		
		for(int mIndex = 0; mIndex < allObjectKits.size(); mIndex++)
		{
			if(allObjectKits.get(mIndex).getObjectType() == RoomObject.MOVE_TRIGGER)
			{
				moveTriggers.add(allObjectKits.get(mIndex));
			}
		}		
		
		buildMainPanel();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void objectTypeSelected(int typeIndex)
	{
		if(typeIndex == 0) // Object Trigger
		{
			tableCards.show(tableCardPanel, "OT Card");
			
			setComponentsEnabled(objectTriggerPanel, true);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 1) // Move Trigger
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, true);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 2) // Static Hazard
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 3) // Timed Hazard
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, true);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 4) // Moving Hazard
		{
			tableCards.show(tableCardPanel, "MH Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, true);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 5) // Spawner
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, true);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 6) // Door
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, true);
			setComponentsEnabled(hallwayPanel, false);
		}
		else if(typeIndex == 7) // Hallway
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, true);
		}
		else if(typeIndex == 8) // Room Goal
		{
			tableCards.show(tableCardPanel, "Blank Card");
			
			setComponentsEnabled(objectTriggerPanel, false);
			setComponentsEnabled(moveTriggerPanel, false);
			setComponentsEnabled(timedHazardPanel, false);
			setComponentsEnabled(movingHazardPanel, false);
			setComponentsEnabled(spawnerPanel, false);
			setComponentsEnabled(doorPanel, false);
			setComponentsEnabled(hallwayPanel, false);
		}		
	}
	
	private void setComponentsEnabled(Container container, boolean state)
	{	
		Component components[] = container.getComponents();
		for(Component component : components)
		{
			component.setEnabled(state);
			if(component instanceof Container)
			{
				setComponentsEnabled((Container)component, state);
			}
		}
		
		container.setEnabled(state);
	}
	
	// Panel Building Methods
	private void buildMainPanel()
	{
		buildTableCardPanel();
		this.add(buildDataPanel());
		this.add(tableCardPanel);
	}
	
	private JPanel buildDataPanel()
	{	
		// Build Triggers/Door Panel
		JPanel trigPanel = new JPanel();
		trigPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		trigPanel.setLayout(new BoxLayout(trigPanel, BoxLayout.Y_AXIS));
		trigPanel.add(buildObjTrigDataPanel());
		trigPanel.add(Box.createVerticalGlue());
		trigPanel.add(buildMoveTrigDataPanel());
		trigPanel.add(Box.createVerticalGlue());
		trigPanel.add(buildDoorDataPanel());
		
		// Build Hazard/Hall Panel
		JPanel hazPanel = new JPanel();
		hazPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		hazPanel.setLayout(new BoxLayout(hazPanel, BoxLayout.Y_AXIS));
		hazPanel.add(buildTimeHazDataPanel());
		hazPanel.add(Box.createVerticalGlue());
		hazPanel.add(buildMoveHazDataPanel());
		hazPanel.add(Box.createVerticalGlue());
		hazPanel.add(buildHallDataPanel());
		
		// Build Spawn Panel
		JPanel spawnPanel = new JPanel();
		spawnPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		spawnPanel.setLayout(new BoxLayout(spawnPanel, BoxLayout.Y_AXIS));
		spawnPanel.add(buildSpawnerPanel());
		
		// Build Shared Panel
		JPanel sharedPanel = new JPanel();
		sharedPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		sharedPanel.setLayout(new BoxLayout(sharedPanel, BoxLayout.Y_AXIS));
		sharedPanel.add(buildSharedDataPanel());

		JPanel dataPanel = new JPanel();
		dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.X_AXIS));
		dataPanel.add(sharedPanel);
		dataPanel.add(trigPanel);
		dataPanel.add(hazPanel);
		dataPanel.add(spawnPanel);
		
		return dataPanel;
	}
	
	private void buildTableCardPanel()
	{
		tableCardPanel = new JPanel();
		tableCardPanel.setLayout(new CardLayout());
	
		tableCardPanel.add(new JPanel(), "Blank Card");	
		tableCardPanel.add(buildObjTrigTablePanel(), "OT Card");	
		tableCardPanel.add(buildMoveTrigTablePanel(), "MH Card");
				
		tableCards = (CardLayout)(tableCardPanel.getLayout());	
	}

	private JPanel buildSharedDataPanel()
	{
		JPanel sharedPanel = new JPanel();
		sharedPanel.setLayout(new BoxLayout(sharedPanel, BoxLayout.Y_AXIS));
		sharedPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Shared Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		
		JLabel nameLabel = new JLabel("Name: ");
		nameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		nameBox = new JTextField("No Name");
		nameBox.setMaximumSize(new Dimension(500, 50));
		nameBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		enabledBox = new JCheckBox("Start Enabled", true);
		enabledBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		useGraphicsBox = new JCheckBox("Use Graphics", true);
		useGraphicsBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		objectTypeBox = new JComboBox<String>(OBJECT_TYPES);
		objectTypeBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		objectTypeBox.addItemListener(new ItemListener() 
		{		
			public void itemStateChanged(ItemEvent state)
			{			
				if(state.getStateChange() == ItemEvent.SELECTED)
				{
					objectTypeSelected(objectTypeBox.getSelectedIndex());
				}
			}	
		});
		
		if(passedKit != null)
		{
			nameBox.setText(passedKit.getName());
			enabledBox.setSelected(passedKit.isStartingEnabled());
			useGraphicsBox.setSelected(passedKit.isUsingGraphics());
			
			if(passedKit.getObjectType() == RoomObject.OBJ_TRIGGER)
			{
				objectTypeBox.setSelectedIndex(0);
			}
			else if(passedKit.getObjectType() == RoomObject.MOVE_TRIGGER)
			{
				objectTypeBox.setSelectedIndex(1);
			}
			else if(passedKit.getObjectType() == RoomObject.STATIC_HAZARD)
			{
				objectTypeBox.setSelectedIndex(2);
			}
			else if(passedKit.getObjectType() == RoomObject.TIMED_HAZARD)
			{
				objectTypeBox.setSelectedIndex(3);
			}
			else if(passedKit.getObjectType() == RoomObject.MOVING_HAZARD)
			{
				objectTypeBox.setSelectedIndex(4);
			}
			else if(passedKit.getObjectType() == RoomObject.SPAWNER)
			{
				objectTypeBox.setSelectedIndex(5);
			}
			else if(passedKit.getObjectType() == RoomObject.DOOR)
			{
				objectTypeBox.setSelectedIndex(6);
			}
			else if(passedKit.getObjectType() == RoomObject.HALLWAY)
			{
				objectTypeBox.setSelectedIndex(7);
			}
			else if(passedKit.getObjectType() == RoomObject.ROOM_GOAL)
			{
				objectTypeBox.setSelectedIndex(8);
			}
		}
		else
		{
			objectTypeBox.setSelectedIndex(1);
		}
		
		JLabel typeLabel = new JLabel("Type: ");
		typeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		sharedPanel.add(nameLabel);
		sharedPanel.add(nameBox);
		sharedPanel.add(Box.createVerticalGlue());
		sharedPanel.add(enabledBox);
		sharedPanel.add(useGraphicsBox);
		sharedPanel.add(Box.createVerticalGlue());
		sharedPanel.add(typeLabel);
		sharedPanel.add(objectTypeBox);
		sharedPanel.add(Box.createVerticalStrut(30));
		
		return sharedPanel;
	}
	
	private JPanel buildObjTrigDataPanel()
	{
		// Build Option Panel
		objectTriggerPanel = new JPanel();
		objectTriggerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		objectTriggerPanel.setMaximumSize(new Dimension(500, 0));
		objectTriggerPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Object Trigger Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		objectTriggerPanel.setLayout(new BoxLayout(objectTriggerPanel, BoxLayout.Y_AXIS));
		
		// Initialize Check Boxes
		useOnceBoxOT = new JCheckBox("Use Once", true);
		useOnceBoxOT.setAlignmentX(Component.LEFT_ALIGNMENT);
		willEnableBox = new JCheckBox("Enabler", false);
		willEnableBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		willDisableBox = new JCheckBox("Disabler", false);
		willDisableBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		// Add Listeners so Enabler and Disabler are Mutually Exclusive
		willEnableBox.addItemListener(new ItemListener() 
		{		
			public void itemStateChanged(ItemEvent state)
			{			
				if(state.getStateChange() == ItemEvent.SELECTED)
				{
					willDisableBox.setSelected(false);
				}
			}			
		});
		willDisableBox.addItemListener(new ItemListener() 
		{		
			public void itemStateChanged(ItemEvent state)
			{			
				if(state.getStateChange() == ItemEvent.SELECTED)
				{
					willEnableBox.setSelected(false);
				}
			}			
		});

		objectTriggerPanel.add(useOnceBoxOT);
		objectTriggerPanel.add(willEnableBox);
		objectTriggerPanel.add(willDisableBox);

		return objectTriggerPanel;
	}
	
	private JPanel buildObjTrigTablePanel()
	{
		int tableWidth = 200;
		int tabelHeight = 150;

		// Create Table
		objectTriggerTable = new JTable(new ObjectTriggerTableModel());
		objectTriggerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		objectTriggerTable.setFillsViewportHeight(true);
		
		// Create the Table ScollPane
		JScrollPane scroller = new JScrollPane(objectTriggerTable);
		scroller.setPreferredSize(new Dimension(tableWidth, tabelHeight));
		scroller.setMaximumSize(new Dimension(tableWidth, tabelHeight));
		
		// Create Table Button Panel
		JPanel tableButtons = new JPanel();
		tableButtons.setLayout(new BoxLayout(tableButtons, BoxLayout.X_AXIS));
		
		String objectIDs[] = new String[allObjectKits.size()];
		
		for(int index = 0; index < allObjectKits.size(); index++)
		{
			objectIDs[index] = allObjectKits.get(index).getName();
		}
		
		final JComboBox<String> objectIDComboBox = new JComboBox<String>(objectIDs);
		
		JButton addObject = new JButton("Add");
		addObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				objTriggerTargets.add(allObjectKits.get(objectIDComboBox.getSelectedIndex()));
				((ObjectTriggerTableModel)objectTriggerTable.getModel()).fireTableDataChanged();
			}
		});
		JButton removeObject = new JButton("Remove Selected");
		removeObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(objectTriggerTable.getSelectedRow() != (-1))
				{
					objTriggerTargets.remove(objectTriggerTable.getSelectedRow());
					((ObjectTriggerTableModel)objectTriggerTable.getModel()).fireTableDataChanged();
				}
			}
		});
		
		tableButtons.add(addObject);
		tableButtons.add(objectIDComboBox);
		tableButtons.add(Box.createHorizontalGlue());
		tableButtons.add(removeObject);
		
		// Set-Up Table Panel
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout(10, 10));
		tablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		tablePanel.add(scroller, BorderLayout.CENTER);
		tablePanel.add(tableButtons, BorderLayout.SOUTH);
		
		return tablePanel;
	}
	
	private JPanel buildMoveTrigDataPanel()
	{			
		moveTriggerPanel = new JPanel();
		moveTriggerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		moveTriggerPanel.setLayout(new BoxLayout(moveTriggerPanel, BoxLayout.Y_AXIS));
		moveTriggerPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Move Trigger Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));

		
		useOnceBoxMT = new JCheckBox("Use Once", true);
		useOnceBoxMT.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		String moveTriggerNames[] = new String[moveTriggers.size() + 1];
		moveTriggerNames[0] = "No Destination";
		
		for(int index = 0; index < moveTriggers.size(); index++)
		{
			moveTriggerNames[index + 1] = moveTriggers.get(index).getName();
		}
		
		moveTriggersBox = new JComboBox<String>(moveTriggerNames);
		moveTriggersBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		if((passedKit != null) && (passedKit.getMoveTriggerDestID() != (-1)))
		{
			for(int index = 0; index < moveTriggers.size(); index ++)
			{
				if(moveTriggers.get(index).getIdNumber() == passedKit.getMoveTriggerDestID())
				{
					moveTriggersBox.setSelectedIndex(index + 1);
					break;
				}
			}
		}
		
		moveTriggerPanel.add(useOnceBoxMT);
		moveTriggerPanel.add(moveTriggersBox);
		
		return moveTriggerPanel;
	}
	
	private JPanel buildTimeHazDataPanel()
	{
		timedHazardPanel = new JPanel();
		timedHazardPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		timedHazardPanel.setLayout(new BoxLayout(timedHazardPanel, BoxLayout.Y_AXIS));
		timedHazardPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Timed Hazard Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));

		JLabel intervalLabel = new JLabel("Interval: ");
		intervalLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		timedIntervalBox = new JComboBox<String>(TIMED_INTERVALS);
		timedIntervalBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		timedHazardPanel.add(intervalLabel);
		timedHazardPanel.add(timedIntervalBox);
		
		return timedHazardPanel;
	}
	
	private JPanel buildMoveHazDataPanel()
	{
		movingHazardPanel = new JPanel();
		movingHazardPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		movingHazardPanel.setLayout(new BoxLayout(movingHazardPanel, BoxLayout.Y_AXIS));
		movingHazardPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Moving Hazard Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));

		JLabel rateLabel = new JLabel("Move Rate: ");
		rateLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

		movingRateBox = new JComboBox<String>(MOVE_RATES);
		movingRateBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		movingHazardPanel.add(rateLabel);
		movingHazardPanel.add(movingRateBox);
		
		return movingHazardPanel;
	}
	
	private JPanel buildMoveTrigTablePanel()
	{
		int tableWidth = 200;
		int tabelHeight = 150;
		
		// Create Table to display Object Data
		moveTable = new JTable(new MovingHazardTableModel());
		moveTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		moveTable.setFillsViewportHeight(true);
		
		// Create the ScollPane
		JScrollPane scroller = new JScrollPane(moveTable);
		scroller.setPreferredSize(new Dimension(tableWidth, tabelHeight));
		
		// Create Move Instruction Panel
		JPanel moveInstPanel = new JPanel();
		moveInstPanel.setLayout(new BoxLayout(moveInstPanel, BoxLayout.X_AXIS));
		
		final JTextField xMoveBox = new JTextField();
		final JTextField yMoveBox = new JTextField();
				
		moveInstPanel.add(new JLabel("X-Move:  "));
		moveInstPanel.add(xMoveBox);
		moveInstPanel.add(new JLabel(" Y-Move:  "));
		moveInstPanel.add(yMoveBox);
		
		// Create Button Panel
		JPanel buttons = new JPanel();
		
		JButton addMove = new JButton("Add Move");
		addMove.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				int xMove = 0;
				int yMove = 0;
				
				try
				{
					xMove = (Integer.parseInt(xMoveBox.getText()));
					yMove = (Integer.parseInt(yMoveBox.getText()));
				}
				catch(NumberFormatException e)
				{
					xMoveBox.setText("0");
					yMoveBox.setText("0");
					return;
				}

				moveInstructions.add(new MoveInstruction(xMove, yMove));
					
				((MovingHazardTableModel)moveTable.getModel()).fireTableDataChanged();
				xMoveBox.setText("0");
				yMoveBox.setText("0");
			}
		});
		JButton removeMove = new JButton("Remove Selected Move");
		removeMove.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if((moveTable.getSelectedRow() != (-1)) && (moveInstructions != null))
				{
					moveInstructions.remove(moveTable.getSelectedRow());
					((MovingHazardTableModel)moveTable.getModel()).fireTableDataChanged();
				}
			}
		});
		
		buttons.add(addMove);
		buttons.add(removeMove);
		
		// Create Bottom Panel
		JPanel bottom = new JPanel();
		bottom.setLayout(new BoxLayout(bottom, BoxLayout.Y_AXIS));
		bottom.add(moveInstPanel);
		bottom.add(buttons);
		
		// Set-Up Panel
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout(10, 10));
		tablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));	
		tablePanel.add(scroller, BorderLayout.CENTER);
		tablePanel.add(bottom, BorderLayout.SOUTH);	
		
		return tablePanel;
	}
	
	private JPanel buildSpawnerPanel()
	{
		spawnerPanel = new JPanel();
		spawnerPanel.setLayout(new BoxLayout(spawnerPanel, BoxLayout.Y_AXIS));
		spawnerPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Spawner Data")
																, BorderFactory.createEmptyBorder(3, 3, 3, 3)));

		// Set Up Labels
		JLabel typeL = new JLabel("Monster Type: ");
		typeL.setAlignmentX(Component.LEFT_ALIGNMENT);
		JLabel sizeL = new JLabel("Wave Size: ");
		sizeL.setAlignmentX(Component.LEFT_ALIGNMENT);
		JLabel rateL = new JLabel("Spawn Rate: ");
		rateL.setAlignmentX(Component.LEFT_ALIGNMENT);

		// Initialize ComboBoxes
		typeBox = new JComboBox<String>(MOB_TYPES);
		typeBox.setAlignmentX(Component.LEFT_ALIGNMENT);	
		sizeBox = new JComboBox<String>(WAVE_SIZES);
		sizeBox.setAlignmentX(Component.LEFT_ALIGNMENT);		
		rateBox = new JComboBox<String>(SPAWN_RATES);
		rateBox.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		if(passedKit != null)
		{
			calculateSpawnOptionSelections();
		}
		else
		{
			typeBox.setSelectedIndex(0);
			sizeBox.setSelectedIndex(0);
			rateBox.setSelectedIndex(2);
		}
		
		spawnerPanel.add(typeL);
		spawnerPanel.add(typeBox);
		spawnerPanel.add(Box.createVerticalStrut(15));
		spawnerPanel.add(sizeL);
		spawnerPanel.add(sizeBox);
		spawnerPanel.add(Box.createVerticalStrut(15));
		spawnerPanel.add(rateL);
		spawnerPanel.add(rateBox);	
		spawnerPanel.add(Box.createVerticalStrut(50));
		
		return spawnerPanel;
	}
	
	private void calculateSpawnOptionSelections()
	{
		// Type
		if (passedKit.getObjectType() == Spawner.MINION) 
		{
			typeBox.setSelectedIndex(0);
		}
		else if (passedKit.getObjectType() == Spawner.CAPTAIN) 
		{
			typeBox.setSelectedIndex(1);
		} 
		else // if(passedKit.getObjectType() == Spawner.BOSS)
		{
			typeBox.setSelectedIndex(2);
		}	
		
		// Wave Size
		sizeBox.setSelectedIndex(passedKit.getWaveSize() - 1);
		
		// Rate
		if (passedKit.getSpawnRate() == (ENGINE_UPDATE_RATE * 3)) 
		{
			rateBox.setSelectedIndex(0);
		} 
		else if (passedKit.getSpawnRate() == (ENGINE_UPDATE_RATE * 2)) 
		{
			rateBox.setSelectedIndex(1);
		} 
		else if (passedKit.getSpawnRate() == ENGINE_UPDATE_RATE) 
		{
			rateBox.setSelectedIndex(2);
		} 
		else // if(passedKit.getSpawnRate() == (ENGINE_UPDATE_RATE / 2))
		{
			rateBox.setSelectedIndex(3);
		}
	}
	
	private JPanel buildDoorDataPanel()
	{
		doorPanel = new JPanel();
		doorPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		doorPanel.setMaximumSize(new Dimension(500, 0));
		doorPanel.setLayout(new BoxLayout(doorPanel, BoxLayout.X_AXIS));
		doorPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Door Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
		
		doorVertical = new JCheckBox("Vertical", false);
		doorVertical.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		if(passedKit != null)
		{
			doorVertical.setSelected(passedKit.isVertical());
		}
		
		doorPanel.add(doorVertical);
		
		return doorPanel;
	}
	
	private JPanel buildHallDataPanel()
	{

		entranceButton = new JRadioButton("Entrance", false);
		exitButton = new JRadioButton("Exit", false);
		eitherButton = new JRadioButton("Neither", true);
		hallVertical = new JCheckBox("Vertical", false);
		hallVertical.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		ButtonGroup typeGroup = new ButtonGroup();
		typeGroup.add(entranceButton);
		typeGroup.add(exitButton);
		typeGroup.add(eitherButton);
		
		hallWidthBox = new JComboBox<String>(HALL_WIDTHS);
		hallWidthBox.setAlignmentX(LEFT_ALIGNMENT);
		
		if(passedKit != null)
		{
			hallVertical.setSelected(passedKit.isVertical());
			
			// Set Type Selection
			if(passedKit.getHallType() == Hallway.ENTRANCE)
			{
				entranceButton.setSelected(true);
			}
			else if(passedKit.getHallType() == Hallway.EXIT)
			{
				exitButton.setSelected(true);
			}
			else // if(passedKit.getHallType() == Hallway.EITHER)
			{
				eitherButton.setSelected(true);
			}
			
			// Set Width Box Index
			if(passedKit.getHallWidth() == (4 * TileData.TILE_WIDTH))
			{
				hallWidthBox.setSelectedIndex(0);
			}
			else if(passedKit.getHallWidth() == (5 * TileData.TILE_WIDTH))
			{
				hallWidthBox.setSelectedIndex(1);
			}
			else // if(passedKit.getHallWidth() == (6 * Tile.WIDTH))
			{
				hallWidthBox.setSelectedIndex(2);
			}
		}
		
		JPanel typePanel = new JPanel();
		typePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		typePanel.setLayout(new BoxLayout(typePanel, BoxLayout.X_AXIS));

		typePanel.add(entranceButton);
		typePanel.add(exitButton);
		typePanel.add(eitherButton);
		
		JLabel widthLabel = new JLabel("Width including walls: ");
		widthLabel.setAlignmentX(LEFT_ALIGNMENT);
		
		hallwayPanel = new JPanel();
		hallwayPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		hallwayPanel.setLayout(new BoxLayout(hallwayPanel, BoxLayout.Y_AXIS));
		hallwayPanel.setBorder(BorderFactory.createTitledBorder("Hallway Data"));
		hallwayPanel.add(typePanel);
		hallwayPanel.add(hallVertical);
		hallwayPanel.add(widthLabel);
		hallwayPanel.add(hallWidthBox);
		
		return hallwayPanel;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	// Kit Methods
	protected ObjectKit buildNewKit(int id)
	{
		ObjectKit newKit = new ObjectKit(id);
		newKit.setObjectType(getObjectType());
		newKit.setName(getKitName());
		newKit.setStartEnabled(isStartingEnabled());
		newKit.setUsesGraphics(isUsingGraphics());
		
		if(newKit.getObjectType() == RoomObject.OBJ_TRIGGER)
		{
			newKit.setUseOnce(isOTUseOnce());
			newKit.setEnabler(isEnabler());
			newKit.setDisabler(isDisabler());
			newKit.setObjTriggerTargets(getTriggerTargetIDs());
		}
		else if(newKit.getObjectType() == RoomObject.MOVE_TRIGGER)
		{
			newKit.setUseOnce(isMTuseOnce());
			newKit.setMoveTriggerDest(getDestinationID());
		}
		else if(newKit.getObjectType() == RoomObject.TIMED_HAZARD)
		{
			newKit.setTimedInterval(getTimedInterval());
		}
		else if(newKit.getObjectType() == RoomObject.MOVING_HAZARD)
		{
			newKit.setMoveRate(getMoveRate());
			newKit.setMoveInstructions(getMoveInstructions());
		}
		else if(newKit.getObjectType() == RoomObject.SPAWNER)
		{
			newKit.setSpawnType(getMonsterType());
			newKit.setWaveSize(getWaveSize());
			newKit.setSpawnRate(getSpawnRate());
		}
		else if(newKit.getObjectType() == RoomObject.DOOR)
		{
			newKit.setVertical(isDoorVertical());
		}
		else if(newKit.getObjectType() == RoomObject.HALLWAY)
		{
			newKit.setVertical(isHallVertical());
			newKit.setHallType(getHallType());
			newKit.setHallWidth(getHallWidth());
		}

		return newKit;
	}
	
	protected void updatePassedKit()
	{
		// Reset arrays for ObjectTypes that have them if the type has changed
		if((passedKit.getObjectType() == RoomObject.OBJ_TRIGGER) && (getObjectType() != RoomObject.OBJ_TRIGGER))
		{
			passedKit.setObjTriggerTargets(new ArrayList<Integer>(0));
		}		
		else if((passedKit.getObjectType() == RoomObject.MOVING_HAZARD) && (getObjectType() != RoomObject.MOVING_HAZARD))
		{
			passedKit.setMoveInstructions(new ArrayList<MoveInstruction>(0));
		}
		
		passedKit.setObjectType(getObjectType());
		passedKit.setName(getKitName());
		passedKit.setStartEnabled(isStartingEnabled());
		passedKit.setUsesGraphics(isUsingGraphics());
		
		if(passedKit.getObjectType() == RoomObject.OBJ_TRIGGER)
		{
			passedKit.setUseOnce(isOTUseOnce());
			passedKit.setEnabler(isEnabler());
			passedKit.setDisabler(isDisabler());
			passedKit.setObjTriggerTargets(getTriggerTargetIDs());
		}
		else if(passedKit.getObjectType() == RoomObject.MOVE_TRIGGER)
		{
			passedKit.setUseOnce(isMTuseOnce());
			passedKit.setMoveTriggerDest(getDestinationID());
		}
		else if(passedKit.getObjectType() == RoomObject.TIMED_HAZARD)
		{
			passedKit.setTimedInterval(getTimedInterval());
		}
		else if(passedKit.getObjectType() == RoomObject.MOVING_HAZARD)
		{
			passedKit.setMoveRate(getMoveRate());
			passedKit.setMoveInstructions(getMoveInstructions());
		}
		else if(passedKit.getObjectType() == RoomObject.SPAWNER)
		{
			passedKit.setSpawnType(getMonsterType());
			passedKit.setWaveSize(getWaveSize());
			passedKit.setSpawnRate(getSpawnRate());
		}
		else if(passedKit.getObjectType() == RoomObject.DOOR)
		{
			passedKit.setVertical(isDoorVertical());
		}
		else if(passedKit.getObjectType() == RoomObject.HALLWAY)
		{
			passedKit.setVertical(isHallVertical());
			passedKit.setHallType(getHallType());
			passedKit.setHallWidth(getHallWidth());
		}		
	}
	
	// Shared Data Getters
	protected int getObjectType()
	{
		if(objectTypeBox.getSelectedIndex() == 0)
		{
			return RoomObject.OBJ_TRIGGER;
		}
		else if(objectTypeBox.getSelectedIndex() == 1)
		{
			return RoomObject.MOVE_TRIGGER;
		}
		else if(objectTypeBox.getSelectedIndex() == 2)
		{
			return RoomObject.STATIC_HAZARD;
		}
		else if(objectTypeBox.getSelectedIndex() == 3)
		{
			return RoomObject.TIMED_HAZARD;
		}
		else if(objectTypeBox.getSelectedIndex() == 4)
		{
			return RoomObject.MOVING_HAZARD;
		}
		else if(objectTypeBox.getSelectedIndex() == 5)
		{
			return RoomObject.SPAWNER;
		}
		else if(objectTypeBox.getSelectedIndex() == 6)
		{
			return RoomObject.DOOR;
		}
		else if(objectTypeBox.getSelectedIndex() == 7)
		{
			return RoomObject.HALLWAY;
		}
		else if(objectTypeBox.getSelectedIndex() == 8)
		{
			return RoomObject.ROOM_GOAL;
		}
		else
		{
			return (-1);
		}
	}

	protected String getKitName()
	{
		return nameBox.getText();
	}
	
	protected boolean isStartingEnabled()
	{
		return enabledBox.isSelected();
	}
	
	protected boolean isUsingGraphics()
	{
		return useGraphicsBox.isSelected();
	}
	
	// Object Trigger Getters
	protected boolean isOTUseOnce()
	{
		return useOnceBoxOT.isSelected();
	}

	protected boolean isEnabler()
	{
		return willEnableBox.isSelected();
	}

	protected boolean isDisabler()
	{
		return willDisableBox.isSelected();
	}

	protected ArrayList<Integer> getTriggerTargetIDs()
	{
		ArrayList<Integer> targetIDs = new ArrayList<Integer>(0);
		
		for(int index = 0; index < objTriggerTargets.size(); index ++)
		{
			targetIDs.add(objTriggerTargets.get(index).getIdNumber());
		}
		
		return targetIDs;
	}
	
	// Move Trigger Getters
	protected boolean isMTuseOnce()
	{
		return useOnceBoxMT.isSelected();
	}
	
	protected int getDestinationID()
	{
		if(moveTriggersBox.getSelectedIndex() == 0)
		{
			return (-1);
		}
		else
		{
			return (moveTriggers.get(moveTriggersBox.getSelectedIndex() - 1).getIdNumber());
		}
	}
	
	// Hazard Getters
	protected int getTimedInterval()
	{
		if(timedIntervalBox.getSelectedIndex() == 0)
		{
			return TimedHazard.HALF_SECOND;
		}
		else if(timedIntervalBox.getSelectedIndex() == 1)
		{
			return TimedHazard.SECOND;
		}
		else // if(timedIntervalBox.getSelectedIndex() == 2)
		{
			return TimedHazard.TWO_SECONDS;
		}
	}
	
	protected int getMoveRate()
	{
		if(movingRateBox.getSelectedIndex() == 0)
		{
			return MovingHazard.SLOW;
		}
		else if(movingRateBox.getSelectedIndex() == 1)
		{
			return MovingHazard.MEDIUM;
		}
		else // if(movingRateBox.getSelectedIndex() == 2)
		{
			return MovingHazard.FAST;
		}		
	}
	
	protected ArrayList<MoveInstruction> getMoveInstructions()
	{
		return moveInstructions;
	}
	
	// Spawner Getters
	protected int getMonsterType() 
	{
		if (typeBox.getSelectedIndex() == 0) 
		{
			return Spawner.MINION;
		} 
		else if (typeBox.getSelectedIndex() == 1) 
		{
			return Spawner.CAPTAIN;
		} 
		else 
		{
			return Spawner.BOSS;
		}
	}

	protected int getWaveSize() 
	{
		return (sizeBox.getSelectedIndex() + 1);
	}

	protected int getSpawnRate() 
	{
		// Spawn Rate is The number of update loops that must occur before the Spawner calls its spawnWave() function.
		// As of 26 Dec, The game engine calls update 60 times a second.

		int rate = 0;

		if (rateBox.getSelectedIndex() == 0) // 1 Every 3 Seconds
		{
			rate = (ENGINE_UPDATE_RATE * 3);
		} 
		else if (rateBox.getSelectedIndex() == 1) // 1 Every 2 Seconds
		{
			rate = (ENGINE_UPDATE_RATE * 2);
		} 
		else if (rateBox.getSelectedIndex() == 2) // 1 Every Second
		{
			rate = ENGINE_UPDATE_RATE;
		} 
		else // if(rateBox.getSelectedIndex() == 3) // 2 Every Second
		{
			rate = (ENGINE_UPDATE_RATE / 2);
		}

		return rate;
	}
	
	// Door Getters
	protected boolean isDoorVertical()
	{
		return doorVertical.isSelected();
	}
	
	// Hallway Getters
	protected boolean isHallVertical()
	{
		return hallVertical.isSelected();
	}
	
	protected int getHallType()
	{
		if(entranceButton.isSelected())
		{
			return Hallway.ENTRANCE;
		}
		else if(exitButton.isSelected())
		{
			return Hallway.EXIT;
		}
		else
		{
			return Hallway.EITHER;
		}
	}
	
	protected int getHallWidth()
	{
		if(hallWidthBox.getSelectedIndex() == 0)
		{
			return 4;
		}
		else if(hallWidthBox.getSelectedIndex() == 1)
		{
			return 5;
		}
		else // if(hallWidthBox.getSelectedIndex() == 2)
		{
			return 6;
		}
	}
	
	// ============================================================================================
	// Contained Classes
	// ============================================================================================
	
	/* ===== ObjectTriggerTableModel ==============================================================
	 * The AbstractTableModel is used to tell the table how to read and display its' data. This
	 * class extends the ATM and adds functionality for ObjectCreationKits.
	 * ========================================================================================= */
	private class ObjectTriggerTableModel extends AbstractTableModel
	{
		private String[] colNames = {"Name", "Type"};

		public ObjectTriggerTableModel()
		{
			super();
		}
		
		@Override
		public int getColumnCount() 
		{
			return colNames.length;
		}
		
		@Override
		public String getColumnName(int col)
		{
			return colNames[col];
		}

		@Override
		public int getRowCount() 
		{
			if(allObjectKits != null)
			{
				return objTriggerTargets.size();
			}
			else
			{
				return 0;
			}
		}

		@Override
		public Object getValueAt(int row, int col) 
		{
			if(objTriggerTargets != null)
			{
				if(col == 0)
				{
					return objTriggerTargets.get(row).getName();
				}
				else if(col == 1)
				{
					return objTriggerTargets.get(row).getTypeString();
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}
	}
	
	/* ===== MovingHazardTableModel ===================================================================
	 * The AbstractTableModel is used to tell the table how to read and display its' data. This
	 * class extends the ATM and is used to display the MoveInstructions for a MovingTrigger.
	 * ========================================================================================= */
	private class MovingHazardTableModel extends AbstractTableModel
	{
		private String[] colNames = {"X-Move", "Y-Move"};

		public MovingHazardTableModel()
		{
			super();
		}
		
		@Override
		public int getColumnCount() 
		{
			return colNames.length;
		}
		
		@Override
		public String getColumnName(int col)
		{
			return colNames[col];
		}

		@Override
		public int getRowCount() 
		{
			if(moveInstructions != null)
			{
				return moveInstructions.size();
			}
			else
			{
				return 0;
			}
		}

		@Override
		public Object getValueAt(int row, int col) 
		{
			if(moveInstructions != null)
			{
				if(col == 0)
				{
					return moveInstructions.get(row).xMove;
				}
				else // if(col == 1)
				{
					return moveInstructions.get(row).yMove;
				}
			}
			else
			{
				return null;
			}
		}
	}
}
