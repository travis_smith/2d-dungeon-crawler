// ================================================================================================
// Class: ObjModuleGUI
// Primary Author: Travis Smith
// Last Updated: Jan 10, 2014
// ================================================================================================
// ObjModuleGUI is an extended JInternalFrame that will contain a table of the ObjectCreationKits 
// for the current room and the buttons needed to create new, edit selected, delete selected, and
// place selected.
// ================================================================================================

package editor.roomModule.objects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class ObjModuleGUI extends JInternalFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Controller
	private ObjectModule controller;
	
	// GUI Members
	private JTextField messageField;
	private JTable objectTable;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected ObjModuleGUI(final ObjectModule controller)
	{
		super(); 

		this.controller = controller;

		// Set-Up Content Pane
		JPanel mainContent = new JPanel();
		mainContent.setLayout(new BorderLayout());
		mainContent.setFocusable(false);
		mainContent.add(buildMessagePanel(), BorderLayout.NORTH);
		mainContent.add(buildTablePanel(), BorderLayout.CENTER);
		mainContent.add(buildButtonPanel(), BorderLayout.SOUTH);
			
		// Set-Up Internal Frame
		this.setTitle("Objects");
		this.setClosable(false);
		this.setContentPane(mainContent);
		this.pack();
		this.setVisible(false);			
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildMessagePanel()
	{
		JPanel msgPanel = new JPanel();
		msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.Y_AXIS));
		msgPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		
		messageField = new JTextField("Messages");
		messageField.setMaximumSize(new Dimension(500, 50));
		messageField.setHorizontalAlignment(JTextField.CENTER);
		messageField.setEditable(false);
		
		msgPanel.add(messageField);
		return msgPanel;
	}
	
	private JPanel buildTablePanel()
	{
		int tableWidth = 300;
		int tabelHeight = 250;
		
		// Set-Up Panel
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(10, 10));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// Create Table to display Object Data
		objectTable = new JTable(new ObjectKitTableModel());
		objectTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
		objectTable.setFillsViewportHeight(true);
		
		// Create the ScollPane
		JScrollPane scroller = new JScrollPane(objectTable);
		scroller.setPreferredSize(new Dimension(tableWidth, tabelHeight));
		
		panel.add(scroller, BorderLayout.CENTER);
		
		return panel;
	}
	
	private JPanel buildButtonPanel()
	{
		JPanel panel = new JPanel();
		
		JButton createObject = new JButton("Create New");
		createObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				controller.buttonPressed(ObjectModule.CREATE, (-1));
			}
		});
		
		JButton editObject = new JButton("Edit");
		editObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(objectTable.getSelectedRow() != (-1))
				{			
					controller.buttonPressed(ObjectModule.EDIT, objectTable.getSelectedRow());
				}
			}
		});
		
		JButton placeObject = new JButton("Place");
		placeObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(objectTable.getSelectedRow() != (-1))
				{
					controller.buttonPressed(ObjectModule.PLACE, objectTable.getSelectedRow());
				}
			}
		});
			
		JButton deleteObject = new JButton("Delete");
		deleteObject.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(objectTable.getSelectedRow() != (-1))
				{
					controller.buttonPressed(ObjectModule.DELETE, objectTable.getSelectedRow());
				}
			}
		});
		
		panel.add(createObject);
		panel.add(editObject);
		panel.add(placeObject);
		panel.add(deleteObject);
		
		return panel;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void displayMessage(String msg)
	{
		messageField.setText(msg);
	}
	
	protected void updateTable()
	{
		((ObjectKitTableModel)objectTable.getModel()).fireTableDataChanged();
	}
	
	// ============================================================================================
	// Contained Classes
	// ============================================================================================
	
	/* ===== ObjectKitTableModel ==================================================================
	 * The AbstractTableModel is used to tell the table how to read and display its' data. This
	 * class extends the ATM and adds functionality for ObjectCreationKits.
	 * ========================================================================================= */
	
	private class ObjectKitTableModel extends AbstractTableModel
	{
		private String[] colNames = {"Name", "Type", "Location", "ID"};

		public ObjectKitTableModel()
		{
			super();
		}
		
		@Override
		public int getColumnCount() 
		{
			return colNames.length;
		}
		
		@Override
		public String getColumnName(int col)
		{
			return colNames[col];
		}

		@Override
		public int getRowCount() 
		{
			if(controller.getObjectKits() != null)
			{
				return controller.getObjectKits().size();
			}
			else
			{
				return 0;
			}
		}

		@Override
		public Object getValueAt(int row, int col) 
		{
			if(controller.getObjectKits() != null)
			{
				if(col == 0)
				{
					return controller.getObjectKits().get(row).getName();
				}
				else if(col == 1)
				{
					return controller.getObjectKits().get(row).getTypeString();
				}
				else if(col == 2)
				{
					Point loc = controller.getObjectKits().get(row).getLocation();
				
					if(loc == null)
					{
					return "(?, ?)";
					}
					else
					{
						return ("(" + Integer.toString(loc.x) + ", " + Integer.toString(loc.y) + ")");
					}
				}
				else // if(col == 3)
				{
					return controller.getObjectKits().get(row).getIdNumber();
				}
			}
			else
			{
				return null;
			}
		}
	}
}
