// ================================================================================================
// Class: TileModuleGUI
// Primary Author: Travis Smith
// Last Modified: Jan 15, 2014
// ================================================================================================
// The TileModuleGUI is an extended InternalFrame that will contain the interface that the users 
// manipulates to edit Tile data.
// ================================================================================================

package editor.roomModule.tileMap;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import objects.roomObjects.RoomObject;

@SuppressWarnings("serial")
public class TileModuleGUI extends JInternalFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Direction Constants
	public static final int UP = 0;
	public static final int LEFT = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;
	
	// Scroll Size Constants
	private static final int SCROLL_WIDTH = 600;
	private static final int SCROLL_HEIGHT = 600;
	
	// Zoom Constants
	private static final String ZOOM_OPTIONS[] = {"100%", "75%", "50%"};
	private static final int NORMAL_ZOOM = TileData.TILE_WIDTH;
	private static final int THREEQRT_ZOOM = ((TileData.TILE_WIDTH * 3) / 4);
	private static final int HALF_ZOOM = (TileData.TILE_WIDTH / 2);

	// Button Array
	private int currTileZoom;
	private TileButton buttonArray[][];
	
	// Controls
	private JTextField messageField;
	private static final String SCROLL_SPEEDS[] = {"1", "3", "5", "10"};
	private JComboBox<String> scrollBox;
	private int scrollIncrement;
	
	// GUI Members
	private JPanel roomView;
	private JScrollPane roomScroll;
	private JComboBox<String> zoomBox;
	private JCheckBox showGridBox;
	private JCheckBox showPathingBox;
	
	// Image Data
	private BufferedImage editorTiles;
	private BufferedImage editorObjectTiles;
	
	// TileModule Editor
	private TileModule controller;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected TileModuleGUI(final TileModule controller, BufferedImage editorTiles, BufferedImage editorObjectTiles)
	{
		super();
		
		this.controller = controller;

		// Initialize Members
		this.currTileZoom = NORMAL_ZOOM; 
		this.scrollIncrement = (1 * currTileZoom);
		this.buttonArray = null;
		this.roomView = null;
		this.roomScroll = null;
		
		this.editorTiles = editorTiles;
		this.editorObjectTiles = editorObjectTiles;
		
		// Set-Up Main Content Pane
		JPanel mainContent = new JPanel();
		mainContent.setFocusable(true);
		mainContent.setLayout(new BorderLayout());
		mainContent.add(buildControlPanel(), BorderLayout.NORTH);
		mainContent.add(buildTileMapPanel(), BorderLayout.CENTER);
		
		createInputMap();
		
		// Set-Up Internal Frame
		this.setFocusable(true);
		this.setResizable(true);
		this.setTitle("Tile Map");
		this.setClosable(false);
		this.setContentPane(mainContent);
		this.setLocation(350, 0);
		this.pack();
		this.setVisible(true);
	}	
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildControlPanel()
	{
		JPanel optionsPanel = new JPanel();
		
		zoomBox = new JComboBox<String>(ZOOM_OPTIONS);
		zoomBox.setFocusable(false);
		zoomBox.addItemListener(new ItemListener() 
		{
			@Override
			public void itemStateChanged(ItemEvent event)  
			{
				if(zoomBox.getSelectedIndex() == 0)
				{
					setZoom(NORMAL_ZOOM);
				}
				else if(zoomBox.getSelectedIndex() == 1)
				{
					setZoom(THREEQRT_ZOOM);
				}
				else if(zoomBox.getSelectedIndex() == 2)
				{
					setZoom(HALF_ZOOM);
				}
			}
		});
		
		scrollBox = new JComboBox<String>(SCROLL_SPEEDS);
		scrollBox.setFocusable(false);
		scrollBox.addItemListener(new ItemListener() 
		{
			@Override
			public void itemStateChanged(ItemEvent event)  
			{
				if(scrollBox.getSelectedIndex() == 0)
				{
					scrollIncrement = (1 * currTileZoom);
				}
				else if(scrollBox.getSelectedIndex() == 1)
				{
					scrollIncrement = (3 * currTileZoom);
				}
				else if(scrollBox.getSelectedIndex() == 2)
				{
					scrollIncrement = (5 * currTileZoom);
				}
				else if(scrollBox.getSelectedIndex() == 3)
				{
					scrollIncrement = (10 * currTileZoom);
				}
			}
		});
		
		showGridBox = new JCheckBox("Show Grid", true);
		showGridBox.setFocusable(false);
		showGridBox.addItemListener(new ItemListener() 
		{
			@Override
			public void itemStateChanged(ItemEvent event)  
			{
				repaint();
			}
		});
		
		showPathingBox = new JCheckBox("Show Pathing", true);
		showPathingBox.setFocusable(false);
		showPathingBox.addItemListener(new ItemListener() 
		{
			@Override
			public void itemStateChanged(ItemEvent event)  
			{
				repaint();
			}
		});
		
		optionsPanel.add(new JLabel("Zoom:"));
		optionsPanel.add(zoomBox);
		optionsPanel.add(Box.createHorizontalStrut(5));
		optionsPanel.add(new JLabel("Scroll Speed:"));
		optionsPanel.add(scrollBox);
		optionsPanel.add(Box.createHorizontalStrut(5));
		optionsPanel.add(showGridBox);
		optionsPanel.add(Box.createHorizontalStrut(5));
		optionsPanel.add(showPathingBox);
		
		JPanel msgPanel = new JPanel();
		msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.Y_AXIS));
		msgPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		
		messageField = new JTextField("Messages");
		messageField.setFocusable(false);
		messageField.setMaximumSize(new Dimension(1000, 50));
		messageField.setHorizontalAlignment(JTextField.CENTER);
		messageField.setEditable(false);
		
		msgPanel.add(messageField);
		
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		controlPanel.add(msgPanel);
		controlPanel.add(optionsPanel);
		
		return controlPanel;
	}
	
	private JScrollPane buildTileMapPanel()
	{
		// Initialize Button Array
		buttonArray = new TileButton[TileModule.ROOM_COLS][TileModule.ROOM_ROWS];
		
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_COLS; cIndex++)
			{
				buttonArray[cIndex][rIndex] = new TileButton(this, new Point(cIndex, rIndex));
			}
		}
		
		// Set-Up RoomView Panel
		roomView = new JPanel();
		roomView.setLayout(null);
		roomView.setMinimumSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		roomView.setMaximumSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		roomView.setPreferredSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		roomView.setBackground(Color.WHITE);

		addButtons();
		
		// Set-Up Scroll Pane
		roomScroll = new JScrollPane(roomView);
		roomScroll.setPreferredSize(new Dimension(SCROLL_WIDTH, SCROLL_HEIGHT));
		
		return roomScroll;
	}
	
	private void addButtons()
	{
		int xOffset = currTileZoom;
		int yOffset = currTileZoom;
		
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_COLS; cIndex++)
			{
				buttonArray[cIndex][rIndex].setBounds((cIndex * xOffset), (rIndex * yOffset), xOffset, yOffset);
				roomView.add(buttonArray[cIndex][rIndex]);
			}
		}
	}	
	
	private void setZoom(int zoom)
	{
		this.setVisible(false);
		
		currTileZoom = zoom;
		
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_COLS; cIndex++)
			{
				roomView.remove(buttonArray[cIndex][rIndex]);
			}
		}
		
		addButtons();
		
		roomView.setMinimumSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		roomView.setMaximumSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		roomView.setPreferredSize(new Dimension((TileModule.ROOM_COLS * currTileZoom), (TileModule.ROOM_ROWS * currTileZoom)));
		
		this.setVisible(true);
	}
	
	private void createInputMap()
	{
		InputMap inputMap = this.getInputMap();
		ActionMap actionMap = this.getActionMap();
			
		Action wPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				scroll(UP);
			}
		};
		
		Action aPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				scroll(LEFT);
			}
		};
		
		Action sPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				scroll(DOWN);
			}
		};
		
		Action dPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				scroll(RIGHT);
			}
		};

		inputMap.put(KeyStroke.getKeyStroke('w'), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke('a'), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke('s'), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke('d'), "D Pressed");
		
		
		actionMap.put("W Pressed", wPressed);
		actionMap.put("A Pressed", aPressed);
		actionMap.put("S Pressed", sPressed);
		actionMap.put("D Pressed", dPressed);	
	}

	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void resetAllData()
	{
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_ROWS; cIndex++)
			{
				buttonArray[cIndex][rIndex].resetAllData();
			}
		}
	}
	
	protected void tileLeftClick(TileButton source, boolean shiftDown)
	{
		controller.tileButtonClicked(source, TileButton.LEFT_CLICK, shiftDown);
	}

	protected void tileRightClick(TileButton source, boolean shiftDown)
	{
		controller.tileButtonClicked(source, TileButton.RIGHT_CLICK, shiftDown);
	}
	
	protected void paintArea(Point topLeft, Point botRight, Point imageCoordinates)
	{
		for(int rIndex = topLeft.y; rIndex <= botRight.y; rIndex ++)
		{
			for(int cIndex = topLeft.x; cIndex <= botRight.x; cIndex++)
			{
				buttonArray[cIndex][rIndex].setImageCoordinates(imageCoordinates);
			}
		}
	}

	protected void paintOuterRoom(Point topLeft, Point botRight)
	{
		// Tile Coordinates
		Point topLeftTile = new Point(0, 0);
		Point topWallTile = new Point(1, 0);
		Point topRightTile = new Point(2, 0);
		Point rightWallTile = new Point(2, 1);
		Point leftWallTile = new Point(0, 1);
		Point botLeftTile = new Point(0, 2);
		Point botRightTile = new Point(2, 2);
		Point botWallTile = new Point(1, 2);
		Point floorTile = new Point(1, 1);
		
		
		for(int hIndex = topLeft.y; hIndex <= botRight.y; hIndex++)
		{
			for(int wIndex = topLeft.x; wIndex <= botRight.x; wIndex++)
			{
				if(hIndex == topLeft.y) // Draw Top Wall
				{
					if(wIndex == topLeft.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(topLeftTile);
					}
					else if(wIndex == botRight.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(topRightTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(topWallTile);
					}
				}
				else if(hIndex == botRight.y) // Draw Bottom Wall
				{
					if(wIndex == topLeft.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(botLeftTile);
					}
					else if(wIndex == botRight.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(botRightTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(botWallTile);
					}
				}
				else // Draw Middle of Room
				{
					if(wIndex == topLeft.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(leftWallTile);
					}
					else if(wIndex == botRight.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(rightWallTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(floorTile);
					}
				}

			}
		}	
	}

	protected void paintInnerRoom(Point topLeft, Point botRight)
	{
		// Tile Coordinates
		Point topLeftTile = new Point(3, 5);
		Point horWallTile = new Point(1, 6);
		Point topRightTile = new Point(0, 6);
		Point botLeftTile = new Point(2, 5);
		Point botRightTile = new Point(1, 5);
		Point vertWallTile = new Point(2, 6);
		Point floorTile = new Point(1, 1);
		
		
		for(int hIndex = topLeft.y; hIndex <= botRight.y; hIndex++)
		{
			for(int wIndex = topLeft.x; wIndex <= botRight.x; wIndex++)
			{
				if(hIndex == topLeft.y) // Draw Top Wall
				{
					if(wIndex == topLeft.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(topLeftTile);
					}
					else if(wIndex == botRight.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(topRightTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(horWallTile);
					}
				}
				else if(hIndex == botRight.y) // Draw Bottom Wall
				{
					if(wIndex == topLeft.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(botLeftTile);
					}
					else if(wIndex == botRight.x)
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(botRightTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(horWallTile);
					}
				}
				else // Draw Middle of Room
				{
					if((wIndex == topLeft.x) || (wIndex == botRight.x) )
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(vertWallTile);
					}
					else
					{
						buttonArray[wIndex][hIndex].setImageCoordinates(floorTile);
					}
				}

			}
		}	
	}

	protected void paintVertHallway(Point topLeft, Point botRight)
	{
		// Tile Coordinates
		Point rightWallTile = new Point(2, 1);
		Point leftWallTile = new Point(0, 1);
		Point floorTile = new Point(1, 1);
		
		
		for(int hIndex = topLeft.y; hIndex <= botRight.y; hIndex++)
		{
			for(int wIndex = topLeft.x; wIndex <= botRight.x; wIndex++)
			{
				if(wIndex == topLeft.x) // Draw Left Wall
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(leftWallTile);
				}
				else if(wIndex == botRight.x) // Draw Right Wall
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(rightWallTile);
				}
				else // Draw Floor
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(floorTile);
				}

			}
		}	
	}

	protected void paintHorizHallway(Point topLeft, Point botRight)
	{
		// Tile Coordinates
		Point topWallTile = new Point(1, 0);
		Point botWallTile = new Point(1, 2);
		Point floorTile = new Point(1, 1);
		
		
		for(int hIndex = topLeft.y; hIndex <= botRight.y; hIndex++)
		{
			for(int wIndex = topLeft.x; wIndex <= botRight.x; wIndex++)
			{
				if(hIndex == topLeft.y) // Draw Top Wall
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(topWallTile);
				}
				else if(hIndex == botRight.y) // Draw Bottom Wall
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(botWallTile);
				}
				else // Draw Floor
				{
					buttonArray[wIndex][hIndex].setImageCoordinates(floorTile);
				}

			}
		}		
	}

	protected void unpaintArea(Point topLeft, Point botRight)
	{
		for(int rIndex = topLeft.y; rIndex <= botRight.y; rIndex ++)
		{
			for(int cIndex = topLeft.x; cIndex <= botRight.x; cIndex++)
			{
				buttonArray[cIndex][rIndex].clearImageCoordinates();
			}
		}
	}
	
	protected void setAreaPathingAs(boolean state, Point topLeft, Point botRight)
	{
		for(int rIndex = topLeft.y; rIndex <= botRight.y; rIndex ++)
		{
			for(int cIndex = topLeft.x; cIndex <= botRight.x; cIndex++)
			{
				buttonArray[cIndex][rIndex].setPathing(state);
			}
		}
	}

	protected boolean placeObject(Point gridLocation, int objectType, boolean objectEnabled, boolean isVertical, int hallType, int hallWidth)
	{
		if(!(buttonArray[gridLocation.x][gridLocation.y].hasObject()))
		{
			if(objectType == RoomObject.DOOR)
			{
				// Check for Right/Bottom edge of Map
				if((gridLocation.x == TileModule.ROOM_COLS - 1) && (!isVertical) ||
				   (gridLocation.y == TileModule.ROOM_ROWS - 1) && (isVertical))
				{
					return false;
				}
				
				if(isVertical)
				{
					if(buttonArray[gridLocation.x][gridLocation.y + 1].hasObject())
					{
						return false;
					}
					else
					{
						// Add to Top Button
						buttonArray[gridLocation.x][gridLocation.y].placeDoor(objectType, objectEnabled, true, true);
						// Add to Bottom Button
						buttonArray[gridLocation.x][gridLocation.y + 1].placeDoor(objectType, objectEnabled, true, false);
						
						repaint();
						return true;
					}
				}
				else
				{
					if(buttonArray[gridLocation.x + 1][gridLocation.y].hasObject())
					{
						return false;
					}
					else
					{
						// Add to Left Button
						buttonArray[gridLocation.x][gridLocation.y].placeDoor(objectType, objectEnabled, false, true);					
						// Add to Right Button
						buttonArray[gridLocation.x + 1][gridLocation.y].placeDoor(objectType, objectEnabled, false, false);
						
						repaint();
						return true;
					}
				}				
			}
			else if(objectType == RoomObject.HALLWAY)
			{
				System.out.println("Entered Add Hallway");
				// Check for Right/Bottom edge of Map
				if((!isVertical) && (gridLocation.x >= (TileModule.ROOM_COLS - hallWidth + 1)))
				{
					System.out.println("Outside Right Bounds: HallWidth = " + hallWidth);
					return false;
				}
				else if((isVertical) && (gridLocation.y >= (TileModule.ROOM_ROWS - hallWidth + 1)))
				{
					System.out.println("Outside Bottom Bounds: HallWidth = " + hallWidth);
					return false;
				}
				
				// Prepare Increments
				int yIncrement = 0;
				int xIncrement = 0;
						
				if(isVertical)
				{
					yIncrement = 1;
				}
				else
				{
					xIncrement = 1;
				}

				// Check for Objects on all Buttons
				for(int index = 0; index < hallWidth; index++)
				{
					if(buttonArray[gridLocation.x + (index * xIncrement)][gridLocation.y + (index * yIncrement)].hasObject())
					{
						System.out.println("Object On Tile: " + index);
						return false;
					}
				}
					
				// Add Hallway to each Button
				for(int index = 0; index < hallWidth; index++)
				{		
					TileButton temp = buttonArray[gridLocation.x + (index * xIncrement)][gridLocation.y + (index * yIncrement)];
					
					if(isVertical)
					{
						if(index == 0)
						{
							temp.placeHallway(objectType, objectEnabled, true, true, hallType, hallWidth);
						}
						else
						{
							temp.placeHallway(objectType, objectEnabled, true, false, hallType, hallWidth);
						}
					}
					else
					{
						if(index == 0)
						{
							temp.placeHallway(objectType, objectEnabled, false, true, hallType, hallWidth);
						}
						else
						{
							temp.placeHallway(objectType, objectEnabled, false, false, hallType, hallWidth);
						}						
					}

				}		
					
				repaint();
				System.out.println("Hallway Added");
				return true;
				
			}
			else
			{
				buttonArray[gridLocation.x][gridLocation.y].placeObject(objectType, objectEnabled);
				repaint();
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	protected void removeObject(Point gridLocation)
	{	
		if((gridLocation != null) && (buttonArray[gridLocation.x][gridLocation.y].hasObject()))
		{
			if(buttonArray[gridLocation.x][gridLocation.y].getObjectType() == RoomObject.DOOR)
			{
				TileButton secondHalf;
				
				if(buttonArray[gridLocation.x][gridLocation.y].isTopOrLeft())
				{
					if(buttonArray[gridLocation.x][gridLocation.y].isObjectVertical())
					{
						secondHalf = buttonArray[gridLocation.x][gridLocation.y + 1];
					}
					else
					{
						secondHalf = buttonArray[gridLocation.x + 1][gridLocation.y];
					}
				}
				else
				{
					if(buttonArray[gridLocation.x][gridLocation.y].isObjectVertical())
					{
						secondHalf = buttonArray[gridLocation.x][gridLocation.y - 1];
					}
					else
					{
						secondHalf = buttonArray[gridLocation.x - 1][gridLocation.y];
					}
				}
				
				buttonArray[gridLocation.x][gridLocation.y].removeObject();
				secondHalf.removeObject();
			}
			else if(buttonArray[gridLocation.x][gridLocation.y].getObjectType() == RoomObject.HALLWAY)
			{
				// Prepare Increments
				int yIncrement = 0;
				int xIncrement = 0;
						
				if(buttonArray[gridLocation.x][gridLocation.y].isObjectVertical())
				{
					yIncrement = 1;
				}
				else
				{
					xIncrement = 1;
				}

				// Remove Hallway from all TileButtons
				int hallWidth = buttonArray[gridLocation.x][gridLocation.y].getHallWidth();
				
				for(int index = 0; index < hallWidth; index++)
				{
					buttonArray[gridLocation.x + (index * xIncrement)][gridLocation.y + (index * yIncrement)].removeObject();
				}
				
			}
			else
			{
				buttonArray[gridLocation.x][gridLocation.y].removeObject();
			}
		}
	}
	
	protected void updateTileData(TileData[][] newTileData)
	{
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_ROWS; cIndex++)
			{
				buttonArray[cIndex][rIndex].updateTileData(newTileData[cIndex][rIndex]);
			}
		}
	}
	
	protected void scroll(int direction)
	{
		if(direction == UP)
		{	
			Point currLoc = roomScroll.getViewport().getViewPosition();
			
			if(currLoc.y >= (currTileZoom))
			{
				roomScroll.getViewport().setViewPosition(new Point(currLoc.x, (currLoc.y - scrollIncrement)));
			}
			else
			{
				roomScroll.getViewport().setViewPosition(new Point(currLoc.x, 0));
			}
		}
		else if(direction == LEFT)
		{	
			Point currLoc = roomScroll.getViewport().getViewPosition();
			
			if(currLoc.x >= (currTileZoom))
			{
				roomScroll.getViewport().setViewPosition(new Point((currLoc.x - scrollIncrement), currLoc.y));
			}
			else
			{
				roomScroll.getViewport().setViewPosition(new Point(0, currLoc.y));
			}
		}
		else if(direction == DOWN)
		{	
			Point currLoc = roomScroll.getViewport().getViewPosition();
			
			if(currLoc.y < (roomView.getHeight() - currTileZoom))
			{
				roomScroll.getViewport().setViewPosition(new Point(currLoc.x, (currLoc.y + scrollIncrement)));
			}
		}
		else if(direction == RIGHT)
		{	
			Point currLoc = roomScroll.getViewport().getViewPosition();
			
			if(currLoc.x < (roomView.getWidth() - currTileZoom))
			{
				roomScroll.getViewport().setViewPosition(new Point((currLoc.x + scrollIncrement), currLoc.y));
			}
		}
	}
	
	// Getters
	protected int getTileZoom()
	{
		return currTileZoom;
	}
	protected boolean isShowingPathing()
	{
		return showPathingBox.isSelected();
	}
	protected boolean isShowingGrid()
	{
		return showGridBox.isSelected();
	}
	protected BufferedImage getTileImages()
	{
		return editorTiles;
	}
	protected BufferedImage getObjectTileImages()
	{
		return editorObjectTiles;
	}
	protected TileData[][] buildDataArray()
	{
		TileData dataArray[][] = new TileData[TileModule.ROOM_COLS][TileModule.ROOM_ROWS];
		
		for(int rIndex = 0; rIndex < TileModule.ROOM_ROWS; rIndex++)
		{
			for(int cIndex = 0; cIndex < TileModule.ROOM_ROWS; cIndex++)
			{
				dataArray[cIndex][rIndex] = buttonArray[cIndex][rIndex].getTileData();
			}
		}
		
		return dataArray;
	}
}
