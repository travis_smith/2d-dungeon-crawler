// ================================================================================================
// Class: TileModule
// Primary Author: Travis Smith
// Last Modified: Jan 15, 2014
// ================================================================================================
// The TileModule will control the Tiles currently being edited and the GUI used to display
// them. It will also be responsible for communicating with the MainEditor. When creating a new
// RoomKit, the TileModule will create a new Tile array to edit. When the RoomKit is saved, it will
// retrieve the Tile array using a getter function. When an already saved RoomKit is loaded into the
// MainEditor, the Tile array will be initialized with the loaded rooms array.
// ================================================================================================

package editor.roomModule.tileMap;

import java.awt.image.BufferedImage;

import editor.roomModule.RoomEditorMain;

public class TileModule
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Constants
	public static final int ROOM_COLS = 30;
	public static final int ROOM_ROWS = 30;
	
	// Main Editor
	private RoomEditorMain mainEditor;
	
	// Module GUI
	private TileModuleGUI moduleGUI;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public TileModule(final RoomEditorMain mainEditor, final BufferedImage editorTiles, final BufferedImage editorObjectTiles)
	{
		this.mainEditor = mainEditor;
		this.moduleGUI = new TileModuleGUI(this, editorTiles, editorObjectTiles);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private boolean decodeAndExecute(TileInstruction instruction)
	{
		if(instruction.getInstructionType() == TileInstruction.PAINT_AREA)
		{
			moduleGUI.paintArea(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner(), 
								instruction.getTileImageCoordinates());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.PAINT_OUTER_ROOM)
		{
			moduleGUI.paintOuterRoom(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.PAINT_INNER_ROOM)
		{
			moduleGUI.paintInnerRoom(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.PAINT_VERT_HALL)
		{
			moduleGUI.paintVertHallway(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.PAINT_HORIZ_HALL)
		{
			moduleGUI.paintHorizHallway(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.UNPAINT_AREA)
		{
			moduleGUI.unpaintArea(instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());
			
			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.ADD_PATH_AREA)
		{
			moduleGUI.setAreaPathingAs(true, instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.RMV_PATH_AREA)
		{
			moduleGUI.setAreaPathingAs(false, instruction.getAreaTopLeftCorner(), instruction.getAreaBottomRightCorner());

			return true;
		}
		else if(instruction.getInstructionType() == TileInstruction.ADD_OBJECT)
		{
			return moduleGUI.placeObject(instruction.getObjectLocation(), instruction.getObjectType(), instruction.isObjectEnabled(), 
								instruction.isObjectVertical(), instruction.getHallType(), instruction.getHallTileWidth());
		}
		else if(instruction.getInstructionType() == TileInstruction.REMOVE_OBJECT)
		{
			moduleGUI.removeObject(instruction.getObjectLocation());

			return true;
		}
		else
		{
			return false;
		}
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void tileButtonClicked(TileButton tileButton, int clickType, boolean shiftState)
	{
		if(clickType == TileButton.LEFT_CLICK)
		{
			mainEditor.tileLeftClicked(tileButton.getTileGridCoordinates(), shiftState);
		}
		else
		{
			mainEditor.tileRightClicked(tileButton.getTileGridCoordinates(), shiftState);
		}
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void newRoom()
	{
		moduleGUI.resetAllData();
	}
	
	public boolean executeInstruction(TileInstruction instruction)
	{
		if(instruction != null)
		{
			return decodeAndExecute(instruction);
		}
		else
		{
			return false;
		}
	}
	
	public void updateTileData(TileData[][] newTileData)
	{
		moduleGUI.updateTileData(newTileData);
	}
	
	// Getters
	public TileModuleGUI getGUI()
	{
		return moduleGUI;
	}
	
	public TileData[][] getTileDataArray()
	{
		return moduleGUI.buildDataArray();
	}
}
