// ================================================================================================
// Class: TileInstruction
// Primary Author: Travis Smith
// Last Modified: Jan 16, 2014
// ================================================================================================
// TileInstructions are used by the TileModule to perform specific actions requested by other
// modules on the TileModule's contained tiles. The TileInstructions cannot be instantiated 
// directly, and the static factory methods should be used to ensure the correct data is passed 
// and assigned.
// ================================================================================================

package editor.roomModule.tileMap;

import java.awt.Point;

import objects.roomObjects.RoomObject;

import editor.roomModule.objects.ObjectKit;

public class TileInstruction
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Painting Instruction Type Constants
	public static final int PAINT_AREA = 10;
	public static final int PAINT_OUTER_ROOM = 11;
	public static final int PAINT_INNER_ROOM = 12;
	public static final int PAINT_VERT_HALL = 13;
	public static final int PAINT_HORIZ_HALL = 14;
	public static final int UNPAINT_AREA = 15;
	
	// Pathing Instruction Type Constants
	public static final int ADD_PATH_AREA = 20;
	public static final int RMV_PATH_AREA = 21;
	
	// Object Instruction Type Constants
	public static final int ADD_OBJECT = 30;
	public static final int REMOVE_OBJECT = 31;

	// Shared Data Members
	private int instructionType;
	private Point areaTopLeftCorner;
	private Point areaBottomRightCorner;
	
	// Painting Instruction Data Members
	private Point tileImageCoordinates;
	
	// Object Instruction Data Members
	private int objectType;
	private boolean objectEnabled;
	private boolean objectVertical;
	private int hallType;
	private int hallTileWidth;
	private Point objectLocation;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	private TileInstruction()
	{
		this.instructionType = (-1);
		this.areaTopLeftCorner = null;
		this.areaBottomRightCorner = null;
		
		this.tileImageCoordinates = null;
		
		this.objectType = (-1);
		this.objectEnabled = false;
		this.objectVertical = false;
		this.hallType = (-1);
		this.hallTileWidth = (-1);
		this.objectLocation = null;
	}
	
	// ============================================================================================
	// Static Factory Methods
	// ============================================================================================
	
	// Painting Instruction Methods
	public static TileInstruction buildAutoPaintInstruction(int autoPaintType, Point firstCorner, Point secondCorner)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(autoPaintType);
		instruction.setAreaCorners(firstCorner, secondCorner);
		
		return instruction;
	}
	public static TileInstruction buildPaintAreaInstruction(Point firstCorner, Point secondCorner, Point tileImageCoordinates)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(PAINT_AREA);
		instruction.setAreaCorners(firstCorner, secondCorner);
		instruction.setTileImageCoordinates(tileImageCoordinates);
		
		return instruction;
	}
	public static TileInstruction buildPaintSingleInstruction(Point tileGridCoordinates, Point tileImageCoordinates)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(PAINT_AREA);
		instruction.setAreaCorners(tileGridCoordinates, tileGridCoordinates);
		instruction.setTileImageCoordinates(tileImageCoordinates);
		
		return instruction;		
	}
	public static TileInstruction buildRemovePaintAreaInstruction(Point firstCorner, Point secondCorner)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(UNPAINT_AREA);
		instruction.setAreaCorners(firstCorner, secondCorner);
		
		return instruction;	
	}
	public static TileInstruction buildRemovePaintSingleInstruction(Point tileGridCoordinates)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(UNPAINT_AREA);
		instruction.setAreaCorners(tileGridCoordinates, tileGridCoordinates);
		
		return instruction;	
	}
	
	// Pathing Instruction Methods
	public static TileInstruction buildAddPathAreaInstruction(Point firstCorner, Point secondCorner)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(ADD_PATH_AREA);
		instruction.setAreaCorners(firstCorner, secondCorner);
		
		return instruction;	
	}
	public static TileInstruction buildRemovePathAreaInstruction(Point firstCorner, Point secondCorner)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(RMV_PATH_AREA);
		instruction.setAreaCorners(firstCorner, secondCorner);
		
		return instruction;	
	}
	
	// Object Instruction Methods
	public static TileInstruction buildAddObjectInstruction(Point tileGridCoordinates, final ObjectKit kit)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(ADD_OBJECT);
		instruction.setObjectLocation(tileGridCoordinates);
		instruction.setObjectType(kit.getObjectType());
		instruction.setObjectEnabled(kit.isStartingEnabled());
		instruction.setObjectVertical(kit.isVertical());
		
		if(kit.getObjectType() == RoomObject.HALLWAY)
		{
			instruction.setHallType(kit.getHallType());
			instruction.setHallTileWidth(kit.getHallWidth());
		}

		return instruction;	
	}
	public static TileInstruction buildRemoveObjectInstruction(Point tileGridCoordinates)
	{
		TileInstruction instruction = new TileInstruction();
		instruction.setInstructionType(REMOVE_OBJECT);
		instruction.setObjectLocation(tileGridCoordinates);
		
		return instruction;	
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void setAreaCorners(Point firstCorner, Point secondCorner)
	{
		// Compare passed corners; set the top left and bottom right corners for easy array traversal
		int topLeftX;
		int topLeftY;
		int botRightX;
		int botRightY;
		
		if(firstCorner.x <= secondCorner.x)
		{
			topLeftX = firstCorner.x;
			botRightX = secondCorner.x;
		}
		else
		{
			topLeftX = secondCorner.x;
			botRightX = firstCorner.x;
		}
		
		if(firstCorner.y <= secondCorner.y)
		{
			topLeftY = firstCorner.y;
			botRightY = secondCorner.y;
		}
		else
		{
			topLeftY = secondCorner.y;
			botRightY = firstCorner.y;
		}
		
		this.areaTopLeftCorner = new Point(topLeftX, topLeftY);
		this.areaBottomRightCorner = new Point(botRightX, botRightY);	
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Getters
	public int getInstructionType()
	{
		return instructionType;
	}
	public Point getAreaTopLeftCorner()
	{
		return areaTopLeftCorner;
	}
	public Point getAreaBottomRightCorner()
	{
		return areaBottomRightCorner;
	}
	public Point getTileImageCoordinates()
	{
		return tileImageCoordinates;
	}
	public int getObjectType()
	{
		return objectType;
	}
	public boolean isObjectEnabled()
	{
		return objectEnabled;
	}
	public boolean isObjectVertical()
	{
		return objectVertical;
	}
	public int getHallType()
	{
		return hallType;
	}
	public int getHallTileWidth()
	{
		return hallTileWidth;
	}
	public Point getObjectLocation()
	{
		return objectLocation;
	}

	// Setters
	public void setInstructionType(int instructionType)
	{
		this.instructionType = instructionType;
	}
	public void setAreaTopLeftCorner(Point areaTopLeftCorner)
	{
		this.areaTopLeftCorner = areaTopLeftCorner;
	}
	public void setAreaBottomRightCorner(Point areaBottomRightCorner)
	{
		this.areaBottomRightCorner = areaBottomRightCorner;
	}
	public void setTileImageCoordinates(Point tileImageCoordinates)
	{
		this.tileImageCoordinates = tileImageCoordinates;
	}
	public void setObjectType(int objectType)
	{
		this.objectType = objectType;
	}
	public void setObjectEnabled(boolean objectEnabled)
	{
		this.objectEnabled = objectEnabled;
	}
	public void setObjectVertical(boolean objectVertical)
	{
		this.objectVertical = objectVertical;
	}
	public void setHallType(int hallType)
	{
		this.hallType = hallType;
	}
	public void setHallTileWidth(int hallTileWidth)
	{
		this.hallTileWidth = hallTileWidth;
	}
	public void setObjectLocation(Point objectLocation)
	{
		this.objectLocation = objectLocation;
	}
}
