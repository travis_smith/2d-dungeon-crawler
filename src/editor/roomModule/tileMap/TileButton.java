// ================================================================================================
// Class: TileButton
// Primary Author: Travis Smith
// Last Updated: Jan 15, 2014
// ================================================================================================
// The TileButton class serves to graphically represent each 64x64 pixel portion of the EditRoom 
// class. It will contain a Tile member and use it as a controller for what the TileGUI displays. 
// TileGUI will also handle mouse clicks that occur within its' area. This class will not be saved
// or exported. It will be initialized with a Tile when the editor loads or creates an EditRoom.
// ================================================================================================

package editor.roomModule.tileMap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import objects.roomObjects.Hallway;
import objects.roomObjects.RoomObject;

@SuppressWarnings("serial")
public class TileButton extends JComponent
{	
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Button Click Constants
	public static final int RIGHT_CLICK = 0;
	public static final int LEFT_CLICK = 1;
	
	// Color Highlights Constants
	public static final Color RED_HIGHLIGHT = new Color(255, 0, 0, 100);
	public static final Color GREEN_HIGHLIGHT = new Color(0, 255, 0, 100);
	public static final Color BLUE_HIGHLIGHT = new Color(0, 0, 255, 100);
	public static final Color YELLOW_HIGHLIGHT = new Color(255, 255, 0, 100);
	
	// Controller and Location
	private TileModuleGUI controller;
	private Point tileGridCoordinates;
	
	// Image Data
	private Point tileImageLocation;
	private boolean isPathing;
	
	// Object Data
	private boolean hasObject;
	private boolean objectEnabled;
	private int objectType;
	private boolean isObjectVertical;
	private boolean isTopOrLeft;
	
	// Hallway Data
	private int hallType;
	private int hallWidth;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected TileButton(final TileModuleGUI controller, Point tileGridCoordinates)
	{	
		super();
		
		// Initialize Controller and Location
		this.controller = controller;
		this.tileGridCoordinates = tileGridCoordinates;
		
		// Initialize Tile Data
		this.tileImageLocation = null;
		this.isPathing = false;
		
		// Initialize Object Data
		this.hasObject = false;
		this.objectEnabled = false;
		this.objectType = (-1);
		this.isObjectVertical = false;
		this.isTopOrLeft = false;
		
		// Initialize Hall Data
		this.hallType = (-1);
		this.hallWidth = (-1);

		// Set-Up Component and Add Mouse Listener
		this.setFocusable(false);
		this.setPreferredSize(new Dimension(controller.getTileZoom(), controller.getTileZoom()));
		this.addMouseListener(new TileAdapter());	
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private Point getObjectIconLocation()
	{				
		// Coordinates are hard-coded off the most current version of the tileSheet layout.
		// These Coordinates are current as of Jan 16, 2014
		
		if(objectType == RoomObject.OBJ_TRIGGER)
		{
			if(objectEnabled)
			{
				return (new Point(1, 2));
			}
			else
			{
				return (new Point(0, 2));
			}
		}
		else if(objectType == RoomObject.MOVE_TRIGGER)
		{	
			if(objectEnabled)
			{
				return (new Point(3, 2));
			}
			else
			{
				return (new Point(2, 2));
			}			
		}
		else if(objectType == RoomObject.STATIC_HAZARD)
		{
			if(objectEnabled)
			{
				return (new Point(3, 4));
			}
			else
			{
				return (new Point(2, 4));
			}			
		}
		else if(objectType == RoomObject.TIMED_HAZARD)
		{
			if(objectEnabled)
			{
				return (new Point(1, 4));
			}
			else
			{
				return (new Point(0, 4));
			}			
		}
		else if(objectType == RoomObject.MOVING_HAZARD)
		{
			if(objectEnabled)
			{
				return (new Point(1, 5));
			}
			else
			{
				return (new Point(0, 5));
			}			
		}
		else if(objectType == RoomObject.SPAWNER)
		{
			if(objectEnabled)
			{
				return (new Point(3, 3));
			}
			else
			{
				return (new Point(2, 3));
			}			
		}	
		else if(objectType == RoomObject.DOOR)
		{
			if(objectEnabled)
			{
				if(isObjectVertical)
				{
					if(isTopOrLeft)
					{
						return (new Point(2, 1));
					}
					else
					{
						return (new Point(3, 1));
					}
				}
				else
				{
					if(isTopOrLeft)
					{
						return (new Point(2, 0));
					}
					else
					{
						return (new Point(3, 0));
					}
				}
			}
			else
			{
				if(isObjectVertical)
				{
					if(isTopOrLeft)
					{
						return (new Point(0, 1));
					}
					else
					{
						return (new Point(1, 1));
					}
				}
				else
				{
					if(isTopOrLeft)
					{
						return (new Point(0, 0));
					}
					else
					{
						return (new Point(1, 0));
					}
				}
			}			
		}
		else if(objectType == RoomObject.ROOM_GOAL)
		{
			return (new Point(2, 5));
		}
		else
		{
			// Error With ObjectType
			return null;
		}
	}

	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void resetAllData()
	{
		// Reset Tile Data
		tileImageLocation = null;
		isPathing = false;
		
		// Reset Object Data
		hasObject = false;
		objectEnabled = false;
		objectType = (-1);
		isObjectVertical = false;
		isTopOrLeft = false;
		
		// Reset Hall Data
		hallType = (-1);
		hallWidth = (-1);	
		
		repaint();
	}
	
	protected void updateTileData(TileData data)
	{
		// Initialize Tile Data
		tileImageLocation = data.getTileImageLocation();
		isPathing = data.isPathing();
		
		// Initialize Object Data
		hasObject = data.hasObject();
		objectEnabled = data.isObjectEnabled();
		objectType = data.getObjectType();
		isObjectVertical = data.isObjectVertical();
		isTopOrLeft = data.isTopOrLeft();
		
		// Initialize Hall Data
		hallType = data.getHallType();
		hallWidth = data.getHallWidth();
		
		repaint();
	}
	
	protected void placeObject(int objectType, boolean objectEnabled)
	{
		this.hasObject = true;
		this.objectType = objectType;
		this.objectEnabled = objectEnabled;		
		this.isObjectVertical = false;
		this.isTopOrLeft = false;
		
		this.hallType = (-1);
		this.hallWidth = (-1);
		
		repaint();
	}
	
	protected void placeDoor(int objectType, boolean objectEnabled, boolean isVertical, boolean isTopOrLeft)
	{
		this.hasObject = true;
		this.objectType = objectType;
		this.objectEnabled = objectEnabled;
		this.isObjectVertical = isVertical;
		this.isTopOrLeft = isTopOrLeft;
		
		this.hallType = (-1);
		this.hallWidth = (-1);
		
		repaint();
	}
	
	protected void placeHallway(int objectType, boolean objectEnabled, boolean isVertical, boolean isTopOrLeft, int hallType, int hallWidth)
	{
		this.hasObject = true;
		this.objectType = objectType;
		this.objectEnabled = objectEnabled;
		this.isObjectVertical = isVertical;
		this.isTopOrLeft = isTopOrLeft;
		
		this.hallType = hallType;
		this.hallWidth = hallWidth;
		
		repaint();
	}
	
	protected void removeObject()
	{
		// Reset Object Data
		hasObject = false;
		objectEnabled = false;
		objectType = (-1);
		
		// Reset Door Data
		isTopOrLeft = false;
		isObjectVertical = false;
		
		// Reset Hall Data
		hallType = (-1);
		hallWidth = (-1);
		
		repaint();
	}
	
	protected Point getTileGridCoordinates()
	{
		return tileGridCoordinates;
	}

	protected void setImageCoordinates(Point imageCoordinates)
	{
		tileImageLocation = imageCoordinates;
		repaint();
	}
	
	protected void clearImageCoordinates()
	{
		tileImageLocation = null;
		repaint();
	}
	
	protected TileData getTileData()
	{
		return new TileData(tileImageLocation, isPathing, hasObject, objectEnabled, objectType, isObjectVertical, isTopOrLeft, hallType, hallWidth);
	}
	
	protected void setPathing(boolean state)
	{
		isPathing = state;
		repaint();
	}
	
	// Getters
	protected boolean hasObject()
	{
		return hasObject;
	}
	protected int getObjectType()
	{
		return objectType;
	}
	protected boolean isObjectVertical()
	{
		return isObjectVertical;
	}
	protected boolean isTopOrLeft()
	{
		return isTopOrLeft;
	}
	protected int getHallType()
	{
		return hallType;
	}
	protected int getHallWidth()
	{
		return hallWidth;
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Painting Methods
	@Override
	public void paint(Graphics gfx)
	{
		// Draw Main Image
		if(tileImageLocation == null)
		{
			gfx.setColor(Color.BLACK);
			gfx.drawRect(0, 0, (controller.getTileZoom() - 1), (controller.getTileZoom() - 1));
		}
		else
		{
			gfx.drawImage(controller.getTileImages(), 0, 0, controller.getTileZoom(), controller.getTileZoom()
							, (tileImageLocation.x * TileData.TILE_WIDTH), (tileImageLocation.y * TileData.TILE_HEIGHT)
							, (((tileImageLocation.x + 1) * TileData.TILE_WIDTH)), (((tileImageLocation.y + 1) * TileData.TILE_HEIGHT)), null);
		}
		
		if(controller.isShowingGrid())
		{
			gfx.drawString(("(" + tileGridCoordinates.x + "," + tileGridCoordinates.y + ")"), 2, 12);
		}

		if((hasObject) && (objectType != RoomObject.HALLWAY))
		{
			Point iconLoc = getObjectIconLocation();
			
			gfx.drawImage(controller.getObjectTileImages(), 0, 0, controller.getTileZoom(), controller.getTileZoom()
							, (iconLoc.x * TileData.TILE_WIDTH), (iconLoc.y * TileData.TILE_HEIGHT)
							, (((iconLoc.x + 1) * TileData.TILE_WIDTH)), (((iconLoc.y  + 1) * TileData.TILE_HEIGHT)), null);				
		}
		
		// Draw Highlights
		if((isPathing) && (controller.isShowingPathing()))
		{	
			gfx.setColor(BLUE_HIGHLIGHT);
			gfx.fillRect(0, 0, (controller.getTileZoom() - 1), (controller.getTileZoom() -1));
		}
		if(objectType == RoomObject.HALLWAY)
		{
			if(hallType == Hallway.ENTRANCE)
			{
				gfx.setColor(GREEN_HIGHLIGHT);
			}
			else if(hallType == Hallway.EXIT)
			{
				gfx.setColor(RED_HIGHLIGHT);
			}
			else // if(hallType == Hallway.EITHER)
			{
				gfx.setColor(YELLOW_HIGHLIGHT);
			}
			
			gfx.fillRect(0, 0, (controller.getTileZoom() - 1), (controller.getTileZoom() -1));
		}
	}

	// ============================================================================================
	// Contained Class
	// ============================================================================================
	
	// Mouse Adapter Class and Click Methods
	private class TileAdapter extends MouseAdapter
	{	
		public void mousePressed(MouseEvent click) 
		{
			if(SwingUtilities.isLeftMouseButton(click))
			{
				if(click.isShiftDown())
				{
					controller.tileLeftClick((TileButton)click.getSource(), true);
				}
				else 
				{
					controller.tileLeftClick((TileButton)click.getSource(), false);
				}
			}
			else if(SwingUtilities.isRightMouseButton(click))
			{
				if(click.isShiftDown())
				{
					controller.tileRightClick((TileButton)click.getSource(), true);
				}
				else 
				{
					controller.tileRightClick((TileButton)click.getSource(), false);
				}
			}
		}
	}
}
	