// ================================================================================================
// Class: TileData
// Primary Author: Travis Smith
// Last Updated: Jan 18, 2014
// ================================================================================================
// The TileData class holds the image coordinates and object data for each TileButton. The class is 
// saved with a RoomKit and used to initialize TileButtons when a RoomKit it loaded. 
// ================================================================================================

package editor.roomModule.tileMap;

import java.awt.Point;
import java.io.Serializable;

public class TileData implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;

	// Size Constants
	public final static int TILE_WIDTH = 64;
	public final static int TILE_HEIGHT = 64;

	// Image Data
	private Point tileImageLocation;
	private boolean isPathing;
	
	// Object Data
	private boolean hasObject;
	private boolean objectEnabled;
	private int objectType;
	private boolean isObjectVertical;
	private boolean isTopOrLeft;
	
	// Hallway Data
	private int hallType;
	private int hallWidth;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public TileData(Point imageLoc, boolean isPath, boolean hasObject, boolean objEnabled, int objType,
					boolean objVert, boolean topOrLeft, int hallType, int hallWidth)
	{
		// Initialize Image Data
		this.tileImageLocation = imageLoc;
		this.isPathing = isPath;
		
		// Initialize Object Data
		this.hasObject = hasObject;
		this.objectEnabled = objEnabled;
		this.objectType = objType;
		this.isObjectVertical = objVert;
		this.isTopOrLeft = topOrLeft;
		
		// Initialize Hallway Data
		this.hallType = hallType;
		this.hallWidth = hallWidth;		
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Getters
	public Point getTileImageLocation()
	{
		return tileImageLocation;
	}

	public boolean isPathing()
	{
		return isPathing;
	}

	public boolean hasObject()
	{
		return hasObject;
	}

	public boolean isObjectEnabled()
	{
		return objectEnabled;
	}

	public int getObjectType()
	{
		return objectType;
	}

	public boolean isObjectVertical()
	{
		return isObjectVertical;
	}

	public boolean isTopOrLeft()
	{
		return isTopOrLeft;
	}

	public int getHallType()
	{
		return hallType;
	}

	public int getHallWidth()
	{
		return hallWidth;
	}
}
