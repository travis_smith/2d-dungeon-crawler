// ================================================================================================
// Class: RoomEditorMain
// Primary Author: Travis Smith
// Last Modified: Jan 18, 2014
// ================================================================================================
// The class will serve as the parent container for various windows associated with editing rooms. 
// It will also act as a controller and will facilitate communication between the modules.
// ================================================================================================

package editor.roomModule;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import editor.roomModule.objects.ObjectModule;
import editor.roomModule.painting.PaintModule;
import editor.roomModule.pathing.PathModule;
import editor.roomModule.tileMap.TileInstruction;
import editor.roomModule.tileMap.TileModule;
import editor.tester.TesterMain;

public class RoomEditorMain
{		
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Editor TileSheet Location Paths
	public static final String EDITOR_TILES_PATH = "/Tiles/editorTiles.png";
	public static final String EDITOR_OBJECT_TILES_PATH = "/Tiles/editorObjectTiles.png";
	
	// Editing Mode Constants
	public static final int PAINTING = 0;
	public static final int PATHING = 1;
	public static final int OBJECTS = 2;
	
	// Image Data
	private BufferedImage editorTiles;
	private BufferedImage editorObjectTiles;
	
	// Main GUI Members
	private JDesktopPane mainContent;
	private JFrame mainFrame;
	
	// Modules
	private PaintModule paintModule;
	private PathModule pathModule;
	private ObjectModule objectModule;
	private TileModule tileModule;

	// Mode Members
	private int currMode;
	
	// Save/Load Members
	private String roomKitName;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public RoomEditorMain()
	{
		initializeImageData();
		
		// Initialize Modules
		this.paintModule = new PaintModule(editorTiles);
		this.pathModule = new PathModule();
		this.objectModule = new ObjectModule(this);
		this.tileModule = new TileModule(this, editorTiles, editorObjectTiles);
		
		// Set-Up Content Pane
		this.mainContent = new JDesktopPane();
		this.mainContent.setLayout(null);
		this.mainContent.setPreferredSize(new Dimension(1024, 768));
		this.mainContent.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		this.mainContent.add(paintModule);
		this.mainContent.add(pathModule.getGUI());
		this.mainContent.add(objectModule.getGUI());
		this.mainContent.add(tileModule.getGUI());
		
		// Initialize Mode
		setMode(PAINTING);
		
		this.roomKitName = null;
		
		// Set-Up Main Frame
		this.mainFrame = new JFrame();
		this.mainFrame.setTitle("Room Editor");
		this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.mainFrame.setJMenuBar(createMenuBar());
		this.mainFrame.setContentPane(mainContent);
		this.mainFrame.pack();
		this.mainFrame.setVisible(true);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void initializeImageData()
	{
		try 
		{
			editorTiles = ImageIO.read(getClass().getResourceAsStream(EDITOR_TILES_PATH));
		} 
		catch (IOException e) 
		{
			System.err.println("Error Loading Editor Tiles: Exiting");
			System.exit(1);
		}
		
		try 
		{
			editorObjectTiles = ImageIO.read(getClass().getResourceAsStream(EDITOR_OBJECT_TILES_PATH));
		} 
		catch (IOException e) 
		{
			System.err.println("Error Loading Editor Object Tiles: Exiting");
			System.exit(1);
		}
	}
	
	private JMenuBar createMenuBar()
	{
		// Create Menu Option Items
		JMenuItem newRoom = new JMenuItem("New");
		newRoom.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				if(newRoomOptions())
				{
					newRoom();
				}
			}
		});
		
		JMenuItem loadRoom = new JMenuItem("Load");
		loadRoom.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				loadRoom();
			}
		});
		
		JMenuItem saveRoom = new JMenuItem("Save");
		saveRoom.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				saveRoom();
			}
		});
		
		JMenuItem saveRoomAs = new JMenuItem("SaveAs...");
		saveRoomAs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				saveRoomAs();
			}
		});
		
		JMenuItem exitButton = new JMenuItem("Quit");
		exitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				System.exit(0);
			}
		});
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(newRoom);
		fileMenu.add(loadRoom);
		fileMenu.add(saveRoom);
		fileMenu.add(saveRoomAs);
		fileMenu.addSeparator();
		fileMenu.add(exitButton);
		
		JMenuItem testRoom = new JMenuItem("Test Room");
		testRoom.setEnabled(false);
		testRoom.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
			}
		});
		
		JMenuItem exportRoom = new JMenuItem("Export Room");
		exportRoom.setEnabled(false);
		exportRoom.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
			}
		});
		
		JMenu exportMenu = new JMenu("Export Options");
		exportMenu.add(testRoom);
		exportMenu.add(exportRoom);
		
		JMenuItem painting = new JMenuItem("Painting");
		painting.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				setMode(RoomEditorMain.PAINTING);
			}
		});
		
		JMenuItem pathing = new JMenuItem("Pathing");
		pathing.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				setMode(RoomEditorMain.PATHING);
			}
		});
		
		JMenuItem objects = new JMenuItem("Objects");
		objects.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				setMode(RoomEditorMain.OBJECTS);
			}
		});
		
		JMenu modeMenu = new JMenu("Edit Modes");
		modeMenu.add(painting);
		modeMenu.add(pathing);
		modeMenu.add(objects);
		
		JMenuBar menu = new JMenuBar();
		menu.add(fileMenu);	
		menu.add(exportMenu);
		menu.add(modeMenu);
	
		return menu;
	}	
	
	private boolean newRoomOptions()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		String warningText = "Are you sure? All unsaved data will be lost.";
		
		JTextField warningField = new JTextField(warningText);
		warningField.setEditable(false);
		
		panel.add(warningField);
			
		int optionChoice = JOptionPane.showConfirmDialog(mainFrame, panel, "New Room", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		
		if(optionChoice == JOptionPane.OK_OPTION)
		{					
			return true;		
		}
		else
		{
			return false;
		}
	}	
	
	private void newRoom()
	{
		roomKitName = null;
		objectModule.newRoom();
		tileModule.newRoom();
	}
	
	private void loadRoom()
	{
		RoomKit temp = RoomKit.loadRoom();
		
		if(temp != null)
		{
			roomKitName = temp.getName();
			objectModule.loadRoomData(temp.getObjectKits(), temp.getObjectIdCode());
			tileModule.updateTileData(temp.getTileData());
		}
	}
	
	private void saveRoom()
	{
		if(roomKitName == null)
		{
			saveRoomAs();
		}
		else
		{
			RoomKit saveKit = new RoomKit(roomKitName, tileModule.getTileDataArray(), objectModule.getObjectKits(), objectModule.getObjectIdCode());
			saveKit.saveRoom();
		}
	}
	
	private void saveRoomAs()
	{
		RoomKit saveKit = new RoomKit(null, tileModule.getTileDataArray(), objectModule.getObjectKits(), objectModule.getObjectIdCode());
		roomKitName = saveKit.saveRoomAs();
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void tileLeftClicked(Point tileGridCoordinates, boolean shiftDown)
	{
		TileInstruction instruction;
		
		if(currMode == PAINTING)
		{
			instruction = paintModule.tileLeftClicked(tileGridCoordinates, shiftDown);
			
			if(instruction != null)
			{
				tileModule.executeInstruction(instruction);
			}
		}
		else if(currMode == PATHING)
		{
			instruction = pathModule.tileLeftClicked(tileGridCoordinates, shiftDown);
			tileModule.executeInstruction(instruction);
		}
		else if(currMode == OBJECTS)
		{
			instruction = objectModule.tileLeftClicked(tileGridCoordinates);
			
			if(tileModule.executeInstruction(instruction)) // Object Placed
			{
				objectModule.placeSuccessful(tileGridCoordinates);
			}
		}
		else
		{
			System.err.println("Error With Editor Mode Selection");
		}
	}
	
	public void tileRightClicked(Point tileGridCoordinates, boolean shiftDown)
	{
		TileInstruction instruction;
		
		if(currMode == PAINTING)
		{
			instruction = paintModule.tileRightClicked(tileGridCoordinates, shiftDown);
			
			if(instruction != null)
			{
				tileModule.executeInstruction(instruction);	
			}
		}
		else if(currMode == PATHING)
		{
			instruction = pathModule.tileRightClicked(tileGridCoordinates, shiftDown);
			tileModule.executeInstruction(instruction);
		}
		else if(currMode == OBJECTS)
		{
			instruction = objectModule.tileRightClicked(tileGridCoordinates);
			tileModule.executeInstruction(instruction);
		}
		else
		{
			System.err.println("Error With Editor Mode Selection");
		}
	}
	
	public void issueInstruction(TileInstruction instruction)
	{
		tileModule.executeInstruction(instruction);
	}
	
	public void setMode(int newMode)
	{
		paintModule.resetAutoPaint();
		objectModule.stopPlacing();
		
		if(newMode == PAINTING)
		{
			currMode = PAINTING;
			
			paintModule.setVisible(true);
			pathModule.getGUI().setVisible(false);
			objectModule.getGUI().setVisible(false);
		}
		else if(newMode == PATHING)
		{
			currMode = PATHING;
			
			paintModule.setVisible(false);
			pathModule.getGUI().setVisible(true);
			objectModule.getGUI().setVisible(false);
		}
		else if(newMode == OBJECTS)
		{
			currMode = OBJECTS;
			
			paintModule.setVisible(false);
			pathModule.getGUI().setVisible(false);
			objectModule.getGUI().setVisible(true);
		}
	}
	
	public void startTester()
	{
		TesterMain.start(null);
	}		

	

}
