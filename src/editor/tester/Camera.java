package editor.tester;

import entities.Entity;


public class Camera 
{
	// Screen Size
	private int visibleW;
	private int visibleH;
	
	// Map Size
	private int totalW;
	private int totalH;
	
	// Map Location
	private int xCord;
	private int yCord;
	
	public Camera(int visW, int visH, int totW, int totH)
	{
		visibleW = visW;
		visibleH = visH;
		
		totalW = totW;
		totalH = totH;
		
		xCord = 0;
		yCord = 0;
		
	}
	
	public void centerOn(Entity actor)
	{
		// Function places the (x,y) of the camera on the graphical center of the passed sprite.
		// The function will not allow the camera to center "outside" the visible area.
		
		xCord = (actor.getCenter().x);
		yCord = (actor.getCenter().y);
	
		if(xCord < (visibleW / 2))
		{
			xCord = (visibleW / 2);
		}
		else if(xCord > (totalW - (visibleW / 2)))
		{
			xCord = (totalW - (visibleW / 2));
		}
		
		if(yCord < (visibleH / 2))
		{
			yCord = (visibleH / 2);
		}
		else if(yCord > (totalH - (visibleH / 2)))
		{
			yCord = (totalH - (visibleH / 2));
		}
	}
	
	public int getX()
	{
		return xCord;
	}
	
	public int getY()
	{
		return yCord;
	}

}
