package editor.tester;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import objects.manager.ObjectManager;
import editor.roomModule.RoomKit;
import editor.roomModule.tileMap.TileData;

@SuppressWarnings("serial")
public class TesterScreen extends Canvas
{	
	// Main Data
	public static final String GAME_NAME = "Excursion";
	private TesterEngine engine;
	
	// Image Data
	private Camera playerCam;
	private BufferedImage mainImage;
	private BufferStrategy bufferStrat;
	private int canvasWidth;
	private int canvasHeight;
	private BufferedImage backBuffer;
	
	public TesterScreen(final TesterEngine testEngine, int width, int height, RoomKit testRoom)
	{
		super();
		this.engine = testEngine;
		this.canvasWidth = width;
		this.canvasHeight = height;
		
		this.setMinimumSize(new Dimension(width, height));
		this.setPreferredSize(new Dimension(width, height));
		this.setMaximumSize(new Dimension(width, height));
		this.setIgnoreRepaint(true);
		this.requestFocus();
		this.setFocusTraversalKeysEnabled(false);
		this.addMouseListener(new ScreenMouseAdapter());
		
//		mainImage = buildBackground(testRoom.getTileGrid());
		backBuffer = new BufferedImage(mainImage.getWidth(), mainImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		this.playerCam = new Camera(canvasWidth, canvasHeight, mainImage.getWidth(), mainImage.getHeight());
	}
	
	private BufferedImage buildBackground(TileData[][] tileGrid)
	{
		int roomCols = RoomKit.ROOM_COLS;
		int roomRows = RoomKit.ROOM_ROWS;
		
		BufferedImage newBG = new BufferedImage((roomCols * TileData.TILE_WIDTH), (roomRows * TileData.TILE_HEIGHT), BufferedImage.TYPE_INT_RGB);
		BufferedImage tileSet = null;
		
		try 
		{
			tileSet = ImageIO.read(getClass().getResourceAsStream("/Tiles/testTiles.png"));
		} 
		catch (IOException e) 
		{
			System.out.println("Error Loading TileSet");
			System.exit(0);
		} 
		
		Graphics2D gfx = newBG.createGraphics();
		gfx.setColor(Color.BLACK);
		
		int colOffset = 0;
		int rowOffset = 0;
		
		for(int rIndex = 0; rIndex < roomRows; rIndex++)
		{			
			for(int cIndex = 0; cIndex < roomCols; cIndex++)
			{		
				if(tileGrid[cIndex][rIndex].getTileImageLocation() != null)
				{
					gfx.drawImage(tileSet, colOffset, rowOffset, colOffset + 64, rowOffset + 64, (tileGrid[cIndex][rIndex].getTileImageLocation().x * 64), 
						(tileGrid[cIndex][rIndex].getTileImageLocation().y * 64), ((tileGrid[cIndex][rIndex].getTileImageLocation().x + 1) * 64),
						((tileGrid[cIndex][rIndex].getTileImageLocation().y + 1) * 64), null);
				}

				colOffset += TileData.TILE_WIDTH;
			}
			
			colOffset = 0;
			rowOffset += TileData.TILE_HEIGHT;
		}
		
		gfx.dispose();
		
		return newBG;		
	}
	
	public void initialize()
	{
		this.createBufferStrategy(3);
		bufferStrat = this.getBufferStrategy();
	}

	public void render(ObjectManager objectManager)
	{
		int index = 0;
		playerCam.centerOn(engine.getHero());
		Graphics bufferGfx = backBuffer.createGraphics();
		
		// Draw BackGround on Back Buffer
		bufferGfx.drawImage(mainImage,
								(playerCam.getX() - (canvasWidth / 2)), (playerCam.getY() - (canvasHeight / 2)), 
								(playerCam.getX() + (canvasWidth / 2)), (playerCam.getY() + (canvasHeight / 2)),
								(playerCam.getX() - (canvasWidth / 2)), (playerCam.getY() - (canvasHeight / 2)), 
								(playerCam.getX() + (canvasWidth / 2)), (playerCam.getY() + (canvasHeight / 2)), null);
		
		engine.getHeroSprite().render(bufferGfx);
		
		objectManager.renderObjects(bufferGfx);
		
		for(index = 0; index < engine.getMonsterSprites().size(); index++)
		{
			engine.getMonsterSprites().get(index).render(bufferGfx);
		}
		
		for(index = 0; index < engine.getPlayerProjectiles().size(); index++)
		{
			engine.getPlayerProjectiles().get(index).render(bufferGfx);
		}
		
		for(index = 0; index < engine.getPlayerSectors().size(); index++)
		{
			engine.getPlayerSectors().get(index).render(bufferGfx);
		}
		
		Graphics gfx = bufferStrat.getDrawGraphics();
		
		gfx.drawImage(backBuffer, 0, 0, canvasWidth, canvasHeight,
				(playerCam.getX() - (canvasWidth / 2)), (playerCam.getY() - (canvasHeight / 2)), 
				(playerCam.getX() + (canvasWidth / 2)), (playerCam.getY() + (canvasHeight / 2)), null);
		
		bufferStrat.show();
		
		bufferGfx.dispose();
		gfx.dispose();
	}

	private class ScreenMouseAdapter extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent event) 
		{
			if(SwingUtilities.isLeftMouseButton(event))
			{
				engine.leftClick(translateToMapCoords(event.getPoint()));
			}
			else
			{
				engine.rightClick(translateToMapCoords(event.getPoint()));
			}
		}
		
		private Point translateToMapCoords(Point click)
		{
			int mapX = playerCam.getX() - (canvasWidth / 2) + click.x;
			int mapY = playerCam.getY() - (canvasHeight / 2) + click.y;
			
			return new Point(mapX, mapY);
		}
	}
}

