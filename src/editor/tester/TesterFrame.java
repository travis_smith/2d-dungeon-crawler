package editor.tester;

import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import editor.roomModule.RoomEditorMain;
import editor.roomModule.RoomKit;

@SuppressWarnings("serial")
public class TesterFrame extends JFrame
{
	// Size Constants
	public static final int GAME_WIDTH = 1024;
	public static final int GAME_HEIGHT = 768;
	
	// Canvas
	private TesterScreen screen;
	private TesterEngine engine;
	
	RoomEditorMain control;
	GraphicsDevice gDev;
	
	public TesterFrame(GraphicsDevice gDev, RoomKit room, RoomEditorMain control)
	{
		super(gDev.getDefaultConfiguration());
		this.setResizable(false);
		this.setUndecorated(true);
		this.setIgnoreRepaint(true);	
		this.getContentPane().setLayout(new BorderLayout());
		this.engine = new TesterEngine(room);
		this.control = control;
		this.gDev = gDev;
	
		createInputMap();
		
		boolean canFullScreen = gDev.isFullScreenSupported();
		
		if(canFullScreen)
		{
			this.screen = new TesterScreen(engine, gDev.getDisplayMode().getWidth(), gDev.getDisplayMode().getHeight(), room);
			this.getContentPane().add(screen, BorderLayout.CENTER);	
			gDev.setFullScreenWindow(this);
			this.screen.initialize();
			this.engine.setMainScreen(screen);
			this.engine.start();
		}
		else
		{
			this.setUndecorated(false);
			this.screen = new TesterScreen(engine, GAME_WIDTH, GAME_HEIGHT, room);
			this.getContentPane().add(screen, BorderLayout.CENTER);	
			this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			this.pack();
			this.setVisible(true);
			this.screen.initialize();
			this.engine.setMainScreen(screen);
			this.engine.start();
		}
	}
	
	private void endTest()
	{
		this.setVisible(false);
		engine.stop();
		gDev.setFullScreenWindow(null);
	}
	
	private void createInputMap()
	{
		InputMap inputMap = ((JPanel)this.getContentPane()).getInputMap();
		ActionMap actionMap = ((JPanel)this.getContentPane()).getActionMap();
		
		Action escapePressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{			
				endTest();
			}
		};
		Action enterPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// TODO
			}
		};
		Action wPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingUp(true);
			}
		};
		Action wReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingUp(false);
			}
		};
		
		Action aPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingLeft(true);
			}
		};
		Action aReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingLeft(false);
			}
		};
		
		Action sPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingDown(true);
			}
		};
		Action sReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingDown(false);
			}
		};
		
		Action dPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingRight(true);
			}
		};
		Action dReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				engine.getHero().movingRight(false);
			}
		};
		
		inputMap.put(KeyStroke.getKeyStroke("ESCAPE"), "Escape Pressed");
		inputMap.put(KeyStroke.getKeyStroke("ENTER"), "Enter Pressed");
		inputMap.put(KeyStroke.getKeyStroke('w'), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released W"), "W Released");
		inputMap.put(KeyStroke.getKeyStroke('a'), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released A"), "A Released");
		inputMap.put(KeyStroke.getKeyStroke('s'), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released S"), "S Released");
		inputMap.put(KeyStroke.getKeyStroke('d'), "D Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released D"), "D Released");
		
		actionMap.put("Escape Pressed", escapePressed);
		actionMap.put("Enter Pressed", enterPressed);
		actionMap.put("W Pressed", wPressed);
		actionMap.put("W Released", wReleased);
		actionMap.put("A Pressed", aPressed);
		actionMap.put("A Released", aReleased);
		actionMap.put("S Pressed", sPressed);
		actionMap.put("S Released", sReleased);
		actionMap.put("D Pressed", dPressed);	
		actionMap.put("D Released", dReleased);
	}
}

