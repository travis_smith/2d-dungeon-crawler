// ================================================================================================
// Class: TesterEngine
// ================================================================================================
// The engine class will be responsible for controlling the game loops, updating data, and calling
// graphical class' render functions.
// ================================================================================================
package editor.tester;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import objects.roomObjects.Spawner;

import sprites.EntitySprite;
import sprites.Projectile;
import sprites.SectorSprite;

import combat.CombatMath;
import combat.Melee;
import combat.Ranged;

import editor.roomModule.RoomKit;
import editor.roomModule.tileMap.TileData;
import entities.Hero;
import entities.Monster;
import entities.MonsterFactory;
import entities.Profession;

public class TesterEngine implements Runnable
{
	// Loop Constants
	private static final double UPDATE_RATE = 60D; 
	
	// Entities, Sprites, and Objects
	private Hero player;
	private EntitySprite playerSprite;
	private ArrayList<Monster> monsters;
	private ArrayList<EntitySprite> monsterSprites;
	private ArrayList<Projectile> playerProj;
	private ArrayList<SectorSprite> playerSectors;
	
	// Pathing
	private ArrayList<Rectangle> roomPathing;
	
	// Test Members
	private boolean running;
	private RoomKit testRoom;
	private TesterScreen screen;
	private Melee meleeAbility;
	private Ranged rangedAbility;
	private static BufferedImage projImage;
	
	public TesterEngine(RoomKit testRoom)
	{
		this.testRoom = testRoom;
		this.running = false;
		this.roomPathing = calculatePathing();

		this.player = new Hero("Test Hero", Profession.HERO_WIZARD, "/Entities/Heroes/Wizard/WizardSpriteSheet4D.png");
		this.player.setLocation(new Point(128, 128)); // TODO Start at entry Hallway
		this.playerSprite = new EntitySprite(player);
		
		this.monsters = new ArrayList<Monster>(0);
		this.monsterSprites = new ArrayList<EntitySprite>(0);
		
		this.playerProj = new ArrayList<Projectile>(0);
		this.playerSectors = new ArrayList<SectorSprite>(0);

		createTestAbilities();
		initializeProjectileImage();
//		this.testRoom.getObjectManager().initializeRendererImages("/Tiles/testObjectTiles.png");
	}
	
	private void initializeProjectileImage()
	{
		try 
		{
			projImage = ImageIO.read(getClass().getResourceAsStream("/Entities/Effects/Ranged/Fireball.png"));
		} 
		catch (IOException e) 
		{
			projImage = null;
		}	
	}
	
	private void createTestAbilities()
	{
		meleeAbility = new Melee("Test Melee", 1, 5, (Math.PI / 4), 5, null, 96);
	
		
		rangedAbility = new Ranged("Test Ranged", 1, 5, (Math.PI / 6), 5, null, true, Ranged.ARC_AOE
									, false, false, 500, 7, 0, 64);
	}
	
	private ArrayList<Rectangle> calculatePathing() 
	{		
/*		ArrayList<TileArea> pathingAreas = testRoom.getPathingAreas();
		ArrayList<Rectangle> pathing = new ArrayList<Rectangle>(1);
		

		for(int index = 0; index < pathingAreas.size(); index++)
		{
			TileArea temp = pathingAreas.get(index);
			int width = (temp.getWidth() * TileData.TILE_WIDTH);
			int height = (temp.getHeight() * TileData.TILE_HEIGHT);

			Rectangle newRect = new Rectangle((TileData.TILE_WIDTH * temp.getStartCol()), (TileData.TILE_HEIGHT * temp.getStartRow()), width, height);
				
			pathing.add(newRect);
		}
			
			return pathing;
		*/
		return null;
	}
	
	/* ==== run ===================================================================================
	 * This function contains the main game loop.
	 * ========================================================================================= */
	@Override
	public void run() 
	{
		// Prepare Loop Control Variables
		long lastLoopStartTime = System.nanoTime();				// Use NanoTime for More Accuracy; used for function calls
		double nanoSecPerLoop = (1000000000D / UPDATE_RATE); 	// One Billion nanoseconds = 1 second
																// This updates "UPDATE_RATE" times per second
		double loopTimer = 0;
		long currentLoopStartTime;
		
		// Prepare Counter Variables
		long counterStartTime = System.currentTimeMillis();
		int frames = 0;
		int updates = 0;

		// Main Loop
		while(running)
		{
			currentLoopStartTime = System.nanoTime();
			loopTimer += (currentLoopStartTime - lastLoopStartTime) / nanoSecPerLoop;
			lastLoopStartTime = currentLoopStartTime;
			
			if(loopTimer >= 1)
			{
				updates++;
				updateData();
				loopTimer -= 1;
			}
			
			// Render 
			frames++;
//			screen.render(testRoom.getObjectManager());
					
			if((System.currentTimeMillis() - counterStartTime) >= 1000)
			{
				counterStartTime = System.currentTimeMillis();
				System.out.println("Updates: " + updates + " Frames: " + frames);
				updates = 0;
				frames = 0;
			}
		}
	}
	
	/* ==== updateData ============================================================================
	 * The update function will be called after the game loops reach the UPDATE_RATE constant. It
	 * will tell all contained classes to update their data such as locations and if they are dead.
	 * ========================================================================================= */
	private void updateData()
	{
		// Update Objects
//		testRoom.getObjectManager().updateObjects();
//		spawnMonsterWaves();
		
		// Player Turn
		movePlayer();
		getNextAbility();
		useNextAbility();
//		testRoom.getObjectManager().checkCollisionsWith(playerSprite);
		
		// Abilities
		moveProjectiles();
		checkProjectileMonsterCollisions();
		checkSectorMonsterCollisions();

		// Monster Turn
		takeMonsterTurns();
		checkMonsterHazardCollisions();
		checkMonsterPlayerCollisions();
	}
	
	// Get Monster Waves from "Ready" Spawners
	public void spawnMonsterWaves()
	{
//		ArrayList<Spawner> readySpawns = testRoom.getObjectManager().getReadySpawns();
		ArrayList<Monster> tempMonsters = new ArrayList<Monster>(0);
			
//		for(int index = 0; index < readySpawns.size(); index++)
		{
//			tempMonsters = MonsterFactory.spawnMonsterWave(readySpawns.get(index).getWaveSize(), readySpawns.get(index).getSpawnType());
					
			/* TODO
			 * 	An algorithm needs to be written to assign each Monster in the wave a position relative
			 * 	to the spawners position. As of Dec 26, the exact spawner position is used.
			 */
					
			for(int tIndex = 0; tIndex < tempMonsters.size(); tIndex++)
			{
//				tempMonsters.get(tIndex).setLocation(readySpawns.get(index).getLocation());
				monsterSprites.add(new EntitySprite(tempMonsters.get(tIndex)));
			}
			
			monsters.addAll(tempMonsters);		
		}	
	}
	private void movePlayer()
	{
		player.move(null);
	}
	private void getNextAbility()
	{
		//TODO
	}
	private void useNextAbility()
	{
		// TODO
	}
	private void takeMonsterTurns()
	{
		for(int index = 0; index < monsters.size(); index++)
		{
			Monster tempMonster = monsters.get(index);
			
			if(tempMonster.isDead())
			{
				monsters.remove(tempMonster);
			}
			else
			{
				tempMonster.takeTurn();
				
				if(tempMonster.needsTarget())
				{
					tempMonster.targetHero(player);
				}
				else if(tempMonster.canMelee())
				{
					// TODO
				}
				else if(tempMonster.canRanged())
				{
					// TODO
				}
				else if (tempMonster.needsToMove())
				{
					tempMonster.move(null);
				}
			}
		}
	}
	private void checkMonsterHazardCollisions()
	{
		for(int index = 0; index < monsterSprites.size(); index ++)
		{
//			testRoom.getObjectManager().checkCollisionsWith(monsterSprites.get(index));
		}
	}
	private void checkMonsterPlayerCollisions()
	{
		for(int index = 0; index < monsterSprites.size(); index++)
		{
			if(monsterSprites.get(index).getHitBox().intersects(playerSprite.getHitBox()))
			{
				//	TODO 
			}
		}
	}
	private void moveProjectiles()
	{
		for(int index = 0; index < playerProj.size(); index++)
		{
			Projectile tempProj = playerProj.get(index);
			
			tempProj.move();
			
			if(tempProj.needsRemoval())
			{
				playerProj.remove(tempProj);
			}
			else if(tempProj.desitnationReached())
			{
				if((((Ranged)tempProj.getParent()).isAoe()) && (((Ranged)tempProj.getParent()).getAoeType() == Ranged.ARC_AOE))
				{
					playerSectors.add(tempProj.buildProjectileAoE());
				}
				
				tempProj.removeNextLoop();
			}
		}
	}
	private void checkProjectileMonsterCollisions()
	{	
		for(int pIndex = 0; pIndex < playerProj.size(); pIndex++)
		{
			Projectile tempProj = playerProj.get(pIndex);

			if(tempProj.needsRemoval())
			{
				playerProj.remove(tempProj);
			}
			else if(tempProj.isIgnoringHits())
			{
				return;
			}
			else
			{
				for(int mIndex = 0; mIndex < monsterSprites.size(); mIndex++)
				{
					if(tempProj.getBounds().intersects(monsterSprites.get(mIndex).getHitBox()))
					{
						tempProj.targetHit();
						
						Monster tempMob = (Monster)monsterSprites.get(mIndex).getEntity();
						
						int rawDamage = CombatMath.roller(tempProj.getParent().getMinDamage(), tempProj.getParent().getMaxDamage());					
						tempMob.takeDamage(rawDamage);
						
						if(tempMob.isDead())
						{
							monsters.remove(tempMob);
							monsterSprites.remove(monsterSprites.get(mIndex));
						}
					}
				}
			}
		}	
	}
	private void checkSectorMonsterCollisions()
	{
		for(int sIndex = 0; sIndex < playerSectors.size(); sIndex++)
		{
			SectorSprite tempSector = playerSectors.get(sIndex);

			if(tempSector.needsRemoval())
			{
				playerSectors.remove(tempSector);
			}
			else if(tempSector.hasCheckedCollisions())
			{
				return;
			}
			else
			{
				tempSector.flagCollisionChecked();
				
				for(int mIndex = 0; mIndex < monsterSprites.size(); mIndex++)
				{
					if(tempSector.intersects(monsterSprites.get(mIndex).getHitBox()))
					{
						Monster tempMob = (Monster)monsterSprites.get(mIndex).getEntity();
						
						int rawDamage = CombatMath.roller(tempSector.getParent().getMinDamage(), tempSector.getParent().getMaxDamage());					
						tempMob.takeDamage(rawDamage);
						
						if(tempMob.isDead())
						{
							monsters.remove(tempMob);
							monsterSprites.remove(monsterSprites.get(mIndex));
						}
					}
				}
			}
		}			
	}
	
	/* ==== start =================================================================================
	 * Function will start the game, should only be called after all data has been initialized.
	 * ========================================================================================= */
	public synchronized void start() 
	{
		running = true;
		new Thread(this).start();
	}
	
	/* ==== stop ==================================================================================
	 * Causes the game engine to exit its' infinite loop, this should be called prior to exiting, 
	 * or when switching levels.
	 * ========================================================================================= */
	public synchronized void stop() 
	{
		running = false;
	}
	
	public void removeProjectile(Projectile projectile) 
	{
		playerProj.remove(projectile);
	} 
	
	public Hero getHero()
	{
		return player;
	}
	
	public EntitySprite getHeroSprite()
	{
		return playerSprite;
	}
	
	public ArrayList<EntitySprite> getMonsterSprites()
	{
		return monsterSprites;
	}
	
	public ArrayList<Projectile> getPlayerProjectiles()
	{
		return playerProj;
	}
	
	public ArrayList<SectorSprite> getPlayerSectors()
	{
		return playerSectors;
	}
	
	public void leftClick(Point loc)
	{
		playerSectors.add(meleeAbility.buildSectorSprite(player.getCenter(), loc));
	}
	
	public void rightClick(Point loc)
	{
		playerProj.addAll(rangedAbility.buildProjectiles(player.getCenter(), loc));
	}
	
	public void setMainScreen(TesterScreen screen)
	{
		this.screen = screen;
	}
	
	public RoomKit getRoom()
	{
		return testRoom;
	}
	
	public void addPathingArea(Rectangle area)
	{
		roomPathing.add(area);
	}
	
	public void removePathingArea(Rectangle area)
	{
		roomPathing.remove(area);
	}
	
	public static BufferedImage getProjImage()
	{
		return projImage;
	}
}



