// ================================================================================================
// Class: AbilityTester
// Primary Author: Travis Smith
// Last Updated: Nov 04, 2013
// ================================================================================================
// The AbilityTester class will be used as a minimalistic game screen that hosts a simple level and 
// allows the designer to test the Ability that he/she is currently editing. This class will work
// as a slimmed down version of MainScreen and Engine added together.
// ================================================================================================

package editor.tester;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import objects.roomObjects.Spawner;

import sprites.EntitySprite;
import sprites.Projectile;
import sprites.SectorSprite;

import combat.Ability;
import combat.Melee;
import combat.Ranged;

import editor.abilityModule.AbilityTabs;
import editor.roomModule.RoomKit;
import editor.roomModule.tileMap.TileData;
import entities.Hero;
import entities.Monster;
import entities.Profession;

@SuppressWarnings("serial")
public class AbilityTester extends JInternalFrame implements Runnable
{
	// Size Constants
	public static final int GAME_WIDTH = 500;
	public static final int GAME_HEIGHT = 500;
	
	// Loop Constants
	private static final double UPDATE_RATE = 60D; 
	
	// Components
	private Canvas testScreen;
	private BufferStrategy bufferStrat;
	private AbilityTabs abilityTabs;
	private RoomKit testRoom;
	private BufferedImage mainImage;
	
	// Entities, Sprites, and Objects
	private Hero player;
	private EntitySprite playerSprite;
	private ArrayList<Monster> monsters;
	private ArrayList<EntitySprite> monsterSprites;
	private ArrayList<Projectile> playerProj;
	private ArrayList<Spawner> spawns;
	private ArrayList<SectorSprite> playerSectors;
	
	// Test Members
	private ArrayList<Rectangle> testPathing;
	private Melee testMelee;
	private Ranged testRanged;
	private boolean running;
	private Camera playerCam;
	private BufferedImage backBuffer;
	private BufferedImage objectTiles;
	
	public AbilityTester()
	{
		super();
	
		this.setResizable(false);
		this.setIgnoreRepaint(true);	
		this.getContentPane().setLayout(new BorderLayout(20, 5));
		
		initializeTestRoom();
		initializeEngine();
		createInputMap();
		createRoomCanvas();
		initializeAbilityTabs();
		testMelee = null;
		testRanged = null;

		this.getContentPane().add(testScreen, BorderLayout.CENTER);
		this.getContentPane().add(createLeftPanel(), BorderLayout.WEST);
		this.setClosable(false);
		this.pack();
		this.setVisible(true);
		this.getContentPane().requestFocusInWindow();
	}
	
	public JPanel createLeftPanel()
	{
		JButton setButton = new JButton("Set Values");
		setButton.setAlignmentX(CENTER_ALIGNMENT);
		setButton.setAlignmentY(CENTER_ALIGNMENT);
		setButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				abilityTabs.setAbilityValues();
				
				if(abilityTabs.getAbilityType() == Ability.MELEE)
				{
					testMelee = abilityTabs.buildMeleeAbility();
				}
				else
				{
					testRanged = abilityTabs.buildRangedAbility();
				}
			}
			
		});
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(setButton);
		
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(abilityTabs);
		leftPanel.add(Box.createVerticalStrut(15));
		leftPanel.add(buttonPanel);
		leftPanel.add(Box.createVerticalGlue());
		
		return leftPanel;
	}
	
	private void initializeTestRoom()
	{
		String fileName = "resources/Rooms/AbilityTester.edr";
		
		FileInputStream inStream = null;
		ObjectInputStream load = null;

		try 
		{
			inStream = new FileInputStream(fileName);
			load = new ObjectInputStream(inStream);
			testRoom = (RoomKit)load.readObject();
			load.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Error Finding File");
			e.printStackTrace();
		}
		catch (ClassNotFoundException | IOException e) 
		{
			System.out.println("Error Loading Room");
			e.printStackTrace();
		}
	}
	
	private void initializeEngine()
	{
		running = false;
		testPathing = calculatePathing();
		spawns = new ArrayList<Spawner>(1);

		player = new Hero("Test Hero", Profession.HERO_WIZARD, "/Entities/Heroes/Wizard/WizardSpriteSheet4D.png");
		playerSprite = new EntitySprite(player);
		
		monsters = new ArrayList<Monster>(0);		
		monsterSprites = new ArrayList<EntitySprite>(0);
		
		playerProj = new ArrayList<Projectile>(0);
		playerSectors = new ArrayList<SectorSprite>(0);
	}
	
	private ArrayList<Rectangle> calculatePathing() 
	{
		/*
		ArrayList<TileArea> pathingAreas = testRoom.getPathingAreas();
		ArrayList<Rectangle> pathing = new ArrayList<Rectangle>(1);
		

		for(int index = 0; index < pathingAreas.size(); index++)
		{
			TileArea temp = pathingAreas.get(index);
			int width = (temp.getWidth() * TileData.TILE_WIDTH);
			int height = (temp.getHeight() * TileData.TILE_HEIGHT);

			Rectangle newRect = new Rectangle((TileData.TILE_WIDTH * temp.getStartCol()), (TileData.TILE_HEIGHT * temp.getStartRow()), width, height);
				
			pathing.add(newRect);
		}
			
			return pathing;
			
			*/
		return null;
	}
	
	private void createRoomCanvas()
	{
		testScreen = new Canvas();
		
		testScreen.setMinimumSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		testScreen.setPreferredSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		testScreen.setMaximumSize(new Dimension(GAME_WIDTH, GAME_HEIGHT));
		testScreen.setIgnoreRepaint(true);
		testScreen.setFocusTraversalKeysEnabled(false);
		testScreen.addMouseListener(new TesterMouseAdapter());
		
//		mainImage = buildBackground(testRoom.getTileGrid());	
		backBuffer = new BufferedImage(mainImage.getWidth(), mainImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		this.playerCam = new Camera(GAME_WIDTH, GAME_HEIGHT, mainImage.getWidth(), mainImage.getHeight());
	}
	
	private BufferedImage buildBackground(TileData[][] tileGrid)
	{
		int roomCols = RoomKit.ROOM_COLS;
		int roomRows = RoomKit.ROOM_ROWS;
		
		BufferedImage newBG = new BufferedImage((roomCols * TileData.TILE_WIDTH), (roomRows * TileData.TILE_HEIGHT), BufferedImage.TYPE_INT_RGB);
		BufferedImage tileSet = null;
		
		try 
		{
			tileSet = ImageIO.read(getClass().getResourceAsStream("/Tiles/testTiles.png"));
		} 
		catch (IOException e) 
		{
			System.out.println("Error Loading TileSet");
			System.exit(0);
		} 
		
		try 
		{
			objectTiles = ImageIO.read(getClass().getResourceAsStream("/Tiles/testObjectTiles.png"));
		} 
		catch (IOException e) 
		{
			System.out.println("Error Loading Object TileSet");
			System.exit(0);
		} 
		
		Graphics2D gfx = newBG.createGraphics();
		gfx.setColor(Color.BLACK);
		
		int colOffset = 0;
		int rowOffset = 0;
		
		for(int rIndex = 0; rIndex < roomRows; rIndex++)
		{			
			for(int cIndex = 0; cIndex < roomCols; cIndex++)
			{		
				if(tileGrid[cIndex][rIndex].getTileImageLocation() != null)
				{
					gfx.drawImage(tileSet, colOffset, rowOffset, colOffset + 64, rowOffset + 64, (tileGrid[cIndex][rIndex].getTileImageLocation().x * 64), 
						(tileGrid[cIndex][rIndex].getTileImageLocation().y * 64), ((tileGrid[cIndex][rIndex].getTileImageLocation().x + 1) * 64),
						((tileGrid[cIndex][rIndex].getTileImageLocation().y + 1) * 64), null);
				}

				colOffset += TileData.TILE_WIDTH;
			}
			
			colOffset = 0;
			rowOffset += TileData.TILE_HEIGHT;
		}
		
		gfx.dispose();
		
		return newBG;		
	}
	
	public void initializeRoomCanvas()
	{
		testScreen.createBufferStrategy(3);
		bufferStrat = testScreen.getBufferStrategy();
	}
	
	private void render()
	{
		int index = 0;
		playerCam.centerOn(player);
		Graphics bufferGfx = backBuffer.createGraphics();
		
		// Draw BackGround on Back Buffer
		bufferGfx.drawImage(mainImage,
								(playerCam.getX() - (GAME_WIDTH / 2)), (playerCam.getY() - (GAME_HEIGHT / 2)), 
								(playerCam.getX() + (GAME_WIDTH / 2)), (playerCam.getY() + (GAME_HEIGHT / 2)),
								(playerCam.getX() - (GAME_WIDTH / 2)), (playerCam.getY() - (GAME_HEIGHT / 2)), 
								(playerCam.getX() + (GAME_WIDTH / 2)), (playerCam.getY() + (GAME_HEIGHT / 2)), null);
		
		playerSprite.render(bufferGfx);
		
		for(index = 0; index < spawns.size(); index++)
		{
			//TODO Render Spawners
		}
		
		for(index = 0; index < monsterSprites.size(); index++)
		{
			monsterSprites.get(index).render(bufferGfx);
		}
		
		for(index = 0; index < playerProj.size(); index++)
		{
			playerProj.get(index).render(bufferGfx);
		}
		
		for(index = 0; index < playerSectors.size(); index++)
		{
			playerSectors.get(index).render(bufferGfx); 
		}
		
		Graphics gfx = bufferStrat.getDrawGraphics();
		
		gfx.drawImage(backBuffer, 0, 0, GAME_WIDTH, GAME_HEIGHT,
				(playerCam.getX() - (GAME_WIDTH / 2)), (playerCam.getY() - (GAME_HEIGHT / 2)), 
				(playerCam.getX() + (GAME_WIDTH / 2)), (playerCam.getY() + (GAME_HEIGHT / 2)), null);
		
		bufferStrat.show();
		
		bufferGfx.dispose();
		gfx.dispose();
	}
	
	private void initializeAbilityTabs()
	{
		abilityTabs = new AbilityTabs();
	}
	
	@Override
	public void run() 
	{
		// Prepare Loop Control Variables
		long lastLoopStartTime = System.nanoTime();				// Use NanoTime for More Accuracy; used for function calls
		double nanoSecPerLoop = (1000000000D / UPDATE_RATE); 	// One Billion nanoseconds = 1 second
																// This updates "UPDATE_RATE" times per second
		double loopTimer = 0;
		long currentLoopStartTime;
		
		// Prepare Counter Variables
		long counterStartTime = System.currentTimeMillis();
		int frames = 0;
		int updates = 0;

		// Main Loop
		while(running)
		{
			currentLoopStartTime = System.nanoTime();
			loopTimer += (currentLoopStartTime - lastLoopStartTime) / nanoSecPerLoop;
			lastLoopStartTime = currentLoopStartTime;
			
			if(loopTimer >= 1)
			{
				updates++;
				updateData();
				loopTimer -= 1;
			}
			
			// Render 
			frames++;
			render();
					
			if((System.currentTimeMillis() - counterStartTime) >= 1000)
			{
				counterStartTime = System.currentTimeMillis();
				System.out.println("Updates: " + updates + " Frames: " + frames);
				updates = 0;
				frames = 0;
			}
		}	
	}
	
	private void updateData()
	{
		player.move(null);
		
		for(int index = 0; index < spawns.size(); index++)
		{
			spawns.get(index).activatedBy(null);
		}
		
		for(int index = 0; index < monsters.size(); index++)
		{
			if(monsters.get(index).isDead())
			{
				monsters.remove(monsters.get(index));
			}
			else
			{
				monsters.get(index).move(null);
			}
		}

		for(int index = 0; index < monsterSprites.size(); index++)
		{
			checkCollision(monsterSprites.get(index));
		}
		
		for(int index = 0; index < playerProj.size(); index++)
		{
			playerProj.get(index).move();
			if(playerProj.get(index).needsRemoval())
			{
				playerProj.remove(playerProj.get(index));
			}
			else
			{
				checkCollision(playerProj.get(index));
			}
		}
		
		for(int index = 0; index < playerSectors.size(); index++)
		{
			// TODO Check Sector Collisions
		}
	}
	
	private void checkCollision(Projectile proj) 
	{
		// TODO Check Projectile Collisions
	}
	
	private void checkCollision(EntitySprite mob) 
	{
		// TODO Check Monster Collisions with Player
	}
	
	private void checkCollision(SectorSprite playerArc)
	{
		// TODO Check SectorSprite Collisions
	}
	
	public synchronized void start() 
	{
		running = true;
		new Thread(this).start();
	}
	
	public synchronized void stop() 
	{
		running = false;
		this.dispose();
	}
	
	private void createInputMap()
	{
		InputMap inputMap = ((JPanel)this.getContentPane()).getInputMap();
		ActionMap actionMap = ((JPanel)this.getContentPane()).getActionMap();
		
		Action escapePressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				stop();
			}
		};
		Action enterPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// TODO
			}
		};
		Action wPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingUp(true);
			}
		};
		Action wReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingUp(false);
			}
		};
		
		Action aPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingLeft(true);
			}
		};
		Action aReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingLeft(false);
			}
		};
		
		Action sPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingDown(true);
			}
		};
		Action sReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingDown(false);
			}
		};
		
		Action dPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingRight(true);
			}
		};
		Action dReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				player.movingRight(false);
			}
		};
		
		inputMap.put(KeyStroke.getKeyStroke("ESCAPE"), "Escape Pressed");
		inputMap.put(KeyStroke.getKeyStroke("ENTER"), "Enter Pressed");
		inputMap.put(KeyStroke.getKeyStroke('w'), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released W"), "W Released");
		inputMap.put(KeyStroke.getKeyStroke('a'), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released A"), "A Released");
		inputMap.put(KeyStroke.getKeyStroke('s'), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released S"), "S Released");
		inputMap.put(KeyStroke.getKeyStroke('d'), "D Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released D"), "D Released");
		
		actionMap.put("Escape Pressed", escapePressed);
		actionMap.put("Enter Pressed", enterPressed);
		actionMap.put("W Pressed", wPressed);
		actionMap.put("W Released", wReleased);
		actionMap.put("A Pressed", aPressed);
		actionMap.put("A Released", aReleased);
		actionMap.put("S Pressed", sPressed);
		actionMap.put("S Released", sReleased);
		actionMap.put("D Pressed", dPressed);	
		actionMap.put("D Released", dReleased);
	}
	
	private class TesterMouseAdapter extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent event) 
		{
			if(SwingUtilities.isLeftMouseButton(event))
			{}
			else
			{
				
				rightClick(translateToMapCoords(event.getPoint()));
			}
		}
	}
	
	private Point translateToMapCoords(Point click)
	{
		int mapX = playerCam.getX() - (GAME_WIDTH / 2) + click.x;
		int mapY = playerCam.getY() - (GAME_HEIGHT / 2) + click.y;
		
		return new Point(mapX, mapY);
	}
	
	public void leftClick(Point loc)
	{
	}
	
	public void rightClick(Point loc)
	{
		if((testMelee == null) && (testRanged == null))
		{
			return;
		}
		// TODO
	}
}
