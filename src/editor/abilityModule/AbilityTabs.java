// ================================================================================================
// Class: AbilityTabs
// Primary Author: Travis Smith
// Last Updated: Dec 24, 2013
// ================================================================================================
// The AbilityTabs will be the graphical overlay that interacts with the underlying Ability that
// is being edited. This class will extend a JTabbedPane.
// ================================================================================================

package editor.abilityModule;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import combat.Ability;
import combat.Effect;
import combat.Melee;
import combat.Ranged;

@SuppressWarnings("serial")
public class AbilityTabs extends JTabbedPane
{
	// Constants 
	public static final int ENGINE_UPDATE_RATE = 60;
	
	// Shared Options
	public static final String TYPES[] = {"Melee", "Ranged"};
	public static final String ANGLES[] = {"30", "45", "60", "90", "180", "270", "360"};
	public static final String MIN_DAMAGE[] = {"1", "2", "3", "4"};
	public static final String MAX_DAMAGE[] = {"5", "10", "15"};
	public static final String MAX_HITS[] = {"1", "2", "3", "4", "1000"};
	public static final String SECONDARY[] = {"NONE", "PUSH"};
	
	// Ranged Options
	public static final String AOE_TYPES[] = {"Star", "Arc"};
	public static final String NUM_STARS[] = {"1", "2", "3", "5", "7"};
	public static final String MAX_RANGES[] = {"200", "400", "600"};
	public static final String VELOCITY[] = {"5", "10", "15"};
	
	// Melee Options
	public static final String ARC_RADIUS[] = {"60", "90", "120"};

	// Shared Boxes
	private JComboBox<String> typeBox;
	private JComboBox<String> arcAngleBox;
	private JComboBox<String> minDamBox;
	private JComboBox<String> maxDamBox;
	private JComboBox<String> maxHitsBox;
	private JComboBox<String> secondaryBox;
	
	// Ranged Boxes
	private JCheckBox isAoeBox;
	private JCheckBox isPiercingBox;	
	private JCheckBox ignoreHitsBox;
	private JComboBox<String> aoeTypeBox;
	private JComboBox<String> numStarsBox;	
	private JComboBox<String> maxRangeBox;
	private JComboBox<String> velocityBox;
	
	// Melee Boxes
	private JComboBox<String> arcRadiusBox;	
	
	// New Ability Values	
	// Shared Values
	protected String newName;
	protected int newAbilityType;
	protected int newMinDamage;
	protected int newMaxDamage;
	protected double newArcDegree;
	protected int newMaxHits;
	protected Effect newSecondaryEffect;
	
	// Melee Values
	private int newSectorRadius;
	
	// Ranged Values
	private boolean newIsAoe;
	private int newAoeType;
	private boolean newIgnoreHits;
	private boolean newIsPiercing;
	private int newMaxRange;
	private int newVelocity;
	private int newNumStars;
	
	public AbilityTabs()
	{
		this.setFocusable(false);
		this.addTab("Ability", createEditTab());
		this.addTab("Spawner", createSpawnerTab());
	}
	
	private JPanel createEditTab()
	{
		JPanel tab = new JPanel();
		tab.setLayout(new BoxLayout(tab, BoxLayout.Y_AXIS));
		tab.add(buildSharedPanel());
		tab.add(buildRangedPanel());
		tab.add(buildMeleePanel());

		return tab;
	}
	
	private JPanel buildSharedPanel()
	{
		JPanel shared = new JPanel();
		shared.setLayout(new GridLayout(3, 4, 2, 2));
		shared.setBorder(BorderFactory.createTitledBorder("Shared Attributes"));
		
		// Set-Up Boxes
		typeBox = new JComboBox<String>(TYPES);
		typeBox.setSelectedIndex(0);
		arcAngleBox = new JComboBox<String>(ANGLES);
		arcAngleBox.setSelectedIndex(0);
		minDamBox = new JComboBox<String>(MIN_DAMAGE);
		minDamBox.setSelectedIndex(0);
		maxDamBox = new JComboBox<String>(MAX_DAMAGE);
		maxDamBox.setSelectedIndex(0);
		maxHitsBox = new JComboBox<String>(MAX_HITS);
		maxHitsBox.setSelectedIndex(0);
		secondaryBox = new JComboBox<String>(SECONDARY);
		secondaryBox.setSelectedIndex(0);
		
		// Set Up Labels
		JLabel typeL = new JLabel("Type");
		JLabel arcL = new JLabel("Angle");
		JLabel minDamL = new JLabel("Min Damage");
		JLabel maxDamL = new JLabel("Max Damage");
		JLabel maxHitsL = new JLabel("Max Hits");
		JLabel secondaryL = new JLabel("Secondary Effect");
		
		shared.add(typeL);
		shared.add(typeBox);
		shared.add(arcL);
		shared.add(arcAngleBox);
		shared.add(minDamL);
		shared.add(minDamBox);
		shared.add(maxDamL);
		shared.add(maxDamBox);
		shared.add(maxHitsL);
		shared.add(maxHitsBox);
		shared.add(secondaryL);
		shared.add(secondaryBox);
		
		return shared;	
	}
	
	private JPanel buildRangedPanel()
	{
		JPanel ranged = new JPanel();
		ranged.setLayout(new GridLayout(3, 4, 2, 2));
		ranged.setBorder(BorderFactory.createTitledBorder("Ranged Attributes"));
		
		// Set-Up Boxes
		isAoeBox = new JCheckBox("AoE", false);
		isPiercingBox = new JCheckBox("Piercing", false);
		ignoreHitsBox = new JCheckBox("Ignore Hits", false);
		aoeTypeBox = new JComboBox<String>(AOE_TYPES);
		aoeTypeBox.setSelectedIndex(0);
		numStarsBox = new JComboBox<String>(NUM_STARS);
		numStarsBox.setSelectedIndex(0);
		maxRangeBox = new JComboBox<String>(MAX_RANGES);
		maxRangeBox.setSelectedIndex(0);
		velocityBox = new JComboBox<String>(VELOCITY);
		velocityBox.setSelectedIndex(0);
		
		// Set Up Labels
		JLabel typeL = new JLabel("AoE Type");
		JLabel starL = new JLabel("Number of Stars");
		JLabel rangeL = new JLabel("Max Range");
		JLabel velocityL = new JLabel("Velocity");
		
		ranged.add(isAoeBox);
		ranged.add(isPiercingBox);
		ranged.add(ignoreHitsBox);
		ranged.add(new JLabel(""));
		ranged.add(typeL);
		ranged.add(aoeTypeBox);
		ranged.add(starL);
		ranged.add(numStarsBox);
		ranged.add(rangeL);
		ranged.add(maxRangeBox);
		ranged.add(velocityL);
		ranged.add(velocityBox);
		
		return ranged;		
	}
	
	private JPanel buildMeleePanel()
	{
		JPanel melee = new JPanel();
		melee.setLayout(new GridLayout(3, 4, 2, 2));
		melee.setBorder(BorderFactory.createTitledBorder("Melee Attributes"));
		
		// Set-Up Boxes
		arcRadiusBox = new JComboBox<String>(ARC_RADIUS);
		arcRadiusBox.setSelectedIndex(0);
		
		// Set Up Labels
		JLabel radiusL = new JLabel("Melee Range");

		melee.add(radiusL);
		melee.add(arcRadiusBox);
		
		return melee;	
	}
	
	private JPanel createSpawnerTab()
	{
		JPanel tab = new JPanel();
		
		return tab;
	}
	
	public int getAbilityType()
	{
		int type = typeBox.getSelectedIndex();
		
		if(type == 0) // Melee
		{
			return Ability.MELEE;
		}
		else // Ranged
		{
			return Ability.RANGED;
		}
	}
	
	public Melee buildMeleeAbility()
	{
		return new Melee("Test Melee", newMinDamage, newMaxDamage, newArcDegree, newMaxHits, newSecondaryEffect, newSectorRadius);
	}
	
	public Ranged buildRangedAbility()
	{
		int testRadius = 64;	// TODO Implement AoE radius editing
		
		return new Ranged("Test Ranged", newMinDamage, newMaxDamage, newArcDegree, newMaxHits, newSecondaryEffect, newIsAoe
							, newAoeType, newIgnoreHits, newIsPiercing, newMaxRange, newVelocity, newNumStars, testRadius);
	}
	
	public void setAbilityValues()
	{
		// Get Type
		int type = typeBox.getSelectedIndex();
		if(type == 0) // Melee
		{
			newAbilityType = Ability.MELEE;
		}
		else // Ranged
		{
			newAbilityType = Ability.RANGED;
		}
		
		// Get Angle
		int angle = arcAngleBox.getSelectedIndex();
		if(angle == 0) // 30 Degrees
		{
			newArcDegree = (Math.PI / 6);
		}
		else if(angle == 1) // 45 Degrees
		{
			newArcDegree = (Math.PI / 4);
		}
		else if(angle == 2) // 60 Degrees
		{
			newArcDegree = (Math.PI / 3);
		}
		else if(angle == 3) // 90 Degrees
		{
			newArcDegree = (Math.PI / 2);
		}
		else if(angle == 4) // 180 Degrees
		{
			newArcDegree = (Math.PI);
		}
		else if(angle == 5) // 270 Degrees
		{
			newArcDegree = ((3 *Math.PI) / 2);
		}
		else // 360 Degrees
		{
			newArcDegree = (2 *Math.PI);
		}
		
		// Get Min Damage
		newMinDamage = (Integer.parseInt((String)minDamBox.getSelectedItem()));

		
		// Get Number of Dice
		newMaxDamage = (Integer.parseInt((String)maxDamBox.getSelectedItem()));
		
		// Get Max Hits
		newMaxHits = (Integer.parseInt((String)maxHitsBox.getSelectedItem()));
		
		// Get Secondary
		newSecondaryEffect = null;

		// Set AoE Flag
		if(isAoeBox.isSelected())
		{
			newIsAoe = true;
		}
		else
		{
			newIsAoe = false;
		}
		
		// Set Piercing Flag
		if(isPiercingBox.isSelected())
		{
			newIsPiercing = true;
		}
		else
		{
			newIsPiercing = false;
		}
		
		// Set Ignore Hits Flag
		if(ignoreHitsBox.isSelected())
		{
			newIgnoreHits = true;
		}
		else
		{
			newIgnoreHits = false;
		}
		
		// Get AoE Type
		int aoeType = aoeTypeBox.getSelectedIndex();
		
		if(aoeType == 0) // Star
		{
			newAoeType = Ranged.STAR_AOE;
		}
		else // Arc
		{
			newAoeType = Ranged.ARC_AOE;
		}
		
		// Get Number of Stars
		newNumStars = (Integer.parseInt((String)numStarsBox.getSelectedItem()));

		
		// Get Max Range
		newMaxRange = (Integer.parseInt((String)maxRangeBox.getSelectedItem()));

		
		// Get Velocity
		newVelocity = (Integer.parseInt((String)velocityBox.getSelectedItem()));

		// Get Arc Radius
		newSectorRadius = (Integer.parseInt((String)arcRadiusBox.getSelectedItem()));
	}
}
