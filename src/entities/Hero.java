// ================================================================================================
// Class: Hero
// Primary Author: Travis Smith
// Last Updated: Dec 23, 2013
// ================================================================================================
// The Hero class will be used to represent the player in game.
// ================================================================================================

package entities;

import java.awt.Point;

import combat.Ability;

public class Hero extends Entity
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Turn Members
	private Ability nextAbility;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Hero(String name, int professionTag, String imagePath)
	{
		super(name, professionTag, HERO, imagePath);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void takeTurn()
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	public void move(boolean[][] pathing)
	{		
		if(!moving[UP] && !moving[DOWN] && !moving[RIGHT] && !moving[LEFT])
		{
			return;
		}
		
		if(moving[UP] && !moving[DOWN])
		{
			location.y -= moveRate;
		}
		if(moving[DOWN] && !moving[UP])
		{
			location.y += moveRate;
		}

		if(moving[RIGHT] && !moving[LEFT])
		{
			location.x += moveRate;
		}
		if(moving[LEFT] && !moving[RIGHT])
		{
			location.x -= moveRate;
		}
		
		// TODO Implement Moving with new Feet Class and Boolean Array
	}
	
	public void respawn(Point location)
	{
		currHealth = maxHealth;
		currEnergy = maxEnergy;
		setLocation(location);
		dead = false;
	}
	
	// Moving Toggle Methods
	public void movingUp(boolean toggle)
	{
		moving[UP] = toggle;
	}
	public void movingLeft(boolean toggle)
	{
		moving[LEFT] = toggle;
	}
	public void movingDown(boolean toggle)
	{
		moving[DOWN] = toggle;
	}
	public void movingRight(boolean toggle)
	{
		moving[RIGHT] = toggle;
	}

	// Getters
	public Ability getNextAbility()
	{
		return nextAbility;
	}

	// Setters
	public void setNextAbility(Ability nextAbility)
	{
		this.nextAbility = nextAbility;
	}
}
