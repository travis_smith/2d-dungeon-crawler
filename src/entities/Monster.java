// ================================================================================================
// Class: Monster
// Primary Author: Travis Smith
// Last Updated: Dec 23, 2013
// ================================================================================================
// The Monster class will be used to represent the enemies in game.
// ================================================================================================

package entities;

import combat.CombatMath;

public class Monster extends Entity
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Constants
	public static final int LOOPS_UNTIL_SWITCH = 120;
	
	// AI Data
	private Entity heroTarget;
	private double moveAngle;
	private boolean needsTarget;
	private boolean needsToMove;
	private boolean canMelee;
	private boolean canRanged;
	private int counter;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Monster(String name, int professionTag, String imagePath)
	{
		super(name, professionTag, MONSTER, imagePath);
		this.canMelee = false;
		this.canRanged = false;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void takeTurn()
	{
		if(heroTarget == null)
		{
			needsTarget = true;
		}
		else if(CombatMath.getDistanceTo(location, heroTarget.location) <= CombatMath.MELEE_RANGE)
		{
			// canMelee = true;	//TODO Implement
		}
		else if(CombatMath.getDistanceTo(location, heroTarget.location) <= CombatMath.SHOOT_RANGE)
		{
			// canRanged = true; // TODO Implement
		}
		else
		{
			needsToMove = true;
		}
	}
	
	@Override
	public void move(boolean[][] pathing)
	{	
		for(int index = 0; index < 4; index++)
		{
			moving[index] = false;
		}
		
		// TODO Implement Moving with new Feet Class and Boolean Array

		counter++;
		
		if(counter >= LOOPS_UNTIL_SWITCH)
		{
			needsTarget = true;
		}
	}
	
	public void targetHero(Entity target)
	{
		this.heroTarget = target;
		this.moveAngle = CombatMath.getAngleTo(this.location, target.location);
		this.needsTarget = false;
		this.counter = 0;
	}
	
	// Getters
	public boolean needsTarget()
	{
		return needsTarget;
	}
	public boolean needsToMove()
	{
		return needsToMove;
	}
	public boolean canMelee()
	{
		return canMelee;
	}
	public boolean canRanged()
	{
		return canRanged;
	}
	
	// Setters
}
