// ================================================================================================
// Class: Profession
// Primary Author: Travis Smith
// Last Updated: Dec 23, 2013
// ================================================================================================
// The Profession class will be used to create specific hero and monster types within game.
// Each profession will be assigned specific base stats and Abilities.
// 
// To add a Profession to an Entity, the Entity needs to specify the desired Profession in the 
// contructor. The profession class will then set the appropriate values which will be available 
// through the interface.
// ================================================================================================

package entities;

import combat.Ability;

public class Profession
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Constants
	public static final int HERO_KNIGHT = 0;
	public static final int HERO_ARCHER = 1;
	public static final int HERO_WIZARD = 2;
	public static final int HERO_CLERIC = 3;	
	
	public static final int MINION_MELEE = 4;
	public static final int MINION_RANGED = 5;	
	
	public static final int CAPTAIN_MELEE = 6;
	public static final int CAPTAIN_RANGED = 7;	
	
	public static final int BOSS_MELEE = 8;
	public static final int BOSS_RANGED = 9;
	
	// Entity Data
	private int professionTag;
	private String title;
	private int startHealth;
	private int startEnergy;
	private int baseMoveRate;
	private int baseArmor;
	private int baseAttak;
	
	// Abilities
	private Ability primary;
	private Ability secondary;
	private Ability specialOne;
	private Ability specialTwo;
	private Ability specialThree;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Profession(int professionTag)
	{
		this.professionTag = professionTag;
		
		if(professionTag == HERO_KNIGHT)
		{
			buildHeroKnight();
		}
		else if(professionTag == HERO_ARCHER)
		{
			buildHeroArcher();
		}
		else if(professionTag == HERO_WIZARD)
		{
			buildHeroWizard();
		}
		else if(professionTag == HERO_CLERIC)
		{
			buildHeroCleric();
		}
		else if(professionTag == MINION_MELEE)
		{
			buildMinionMelee();
		}
		else if(professionTag == MINION_RANGED)
		{
			buildMinionRanged();
		}
		else if(professionTag == CAPTAIN_MELEE)
		{
			buildCaptainMelee();
		}
		else if(professionTag == CAPTAIN_RANGED)
		{
			buildCaptainRanged();
		}
		else if(professionTag == BOSS_MELEE)
		{
			buildBossMelee();
		}
		else if(professionTag == BOSS_RANGED)
		{
			buildBossRanged();
		}
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void buildHeroKnight()
	{
		// Initialize Entity Data
		this.title = "Knight";
		this.startHealth = 150;
		this.startEnergy = 50;
		this.baseMoveRate = 2;
		this.baseArmor = 4;
		this.baseAttak = 3;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildHeroArcher()
	{
		// Initialize Entity Data
		this.title = "Archer";
		this.startHealth = 75;
		this.startEnergy = 125;
		this.baseMoveRate = 4;
		this.baseArmor = 2;
		this.baseAttak = 3;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildHeroWizard()
	{
		// Initialize Entity Data
		this.title = "Wizard";
		this.startHealth = 50;
		this.startEnergy = 150;
		this.baseMoveRate = 3;
		this.baseArmor = 2;
		this.baseAttak = 4;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildHeroCleric()
	{
		// Initialize Entity Data
		this.title = "Cleric";
		this.startHealth = 125;
		this.startEnergy = 75;
		this.baseMoveRate = 3;
		this.baseArmor = 2;
		this.baseAttak = 2;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildMinionMelee()
	{
		// Initialize Entity Data
		this.title = "Fighter";
		this.startHealth = 10;
		this.startEnergy = 0;
		this.baseMoveRate = 2;
		this.baseArmor = 1;
		this.baseAttak = 1;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildMinionRanged()
	{
		// Initialize Entity Data
		this.title = "Skirmisher";
		this.startHealth = 50;
		this.startEnergy = 0;
		this.baseMoveRate = 2;
		this.baseArmor = 1;
		this.baseAttak = 1;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildCaptainMelee()
	{
		// Initialize Entity Data
		this.title = "Warrior";
		this.startHealth = 75;
		this.startEnergy = 25;
		this.baseMoveRate = 2;
		this.baseArmor = 3;
		this.baseAttak = 2;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildCaptainRanged()
	{
		// Initialize Entity Data
		this.title = "Archer";
		this.startHealth = 75;
		this.startEnergy = 25;
		this.baseMoveRate = 2;
		this.baseArmor = 2;
		this.baseAttak = 3;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildBossMelee()
	{
		// Initialize Entity Data
		this.title = "Weapon Master";
		this.startHealth = 150;
		this.startEnergy = 50;
		this.baseMoveRate = 2;
		this.baseArmor = 4;
		this.baseAttak = 2;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	private void buildBossRanged()
	{
		// Initialize Entity Data
		this.title = "Ranger";
		this.startHealth = 100;
		this.startEnergy = 100;
		this.baseMoveRate = 2;
		this.baseArmor = 1;
		this.baseAttak = 4;
		
		// Initialize Abilities
		this.primary = null;
		this.secondary = null;
		this.specialOne = null;
		this.specialTwo = null;
		this.specialThree = null;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Setters
	public void setProfessionTag(int professionTag)
	{
		this.professionTag = professionTag;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public void setStartHealth(int startHealth)
	{
		this.startHealth = startHealth;
	}
	public void setStartEnergy(int startEnergy)
	{
		this.startEnergy = startEnergy;
	}
	public void setBaseMoveRate(int baseMoveRate)
	{
		this.baseMoveRate = baseMoveRate;
	}
	public void setBaseArmor(int baseArmor)
	{
		this.baseArmor = baseArmor;
	}
	public void setBaseAttack(int baseAttack)
	{
		this.baseAttak = baseAttack;
	}
	public void setPrimary(Ability primary)
	{
		this.primary = primary;
	}
	public void setSecondary(Ability secondary)
	{
		this.secondary = secondary;
	}
	public void setSpecialOne(Ability specialOne)
	{
		this.specialOne = specialOne;
	}
	public void setSpecialTwo(Ability specialTwo)
	{
		this.specialTwo = specialTwo;
	}
	public void setSpecialThree(Ability specialThree)
	{
		this.specialThree = specialThree;
	}
	
	// Getters

	public int getProfessionTag()
	{
		return professionTag;
	}
	public String getTitle()
	{
		return title;
	}

	public int getStartHealth()
	{
		return startHealth;
	}

	public int getStartEnergy()
	{
		return startEnergy;
	}

	public int getBaseMoveRate()
	{
		return baseMoveRate;
	}
	public int getBaseArmor()
	{
		return baseArmor;
	}
	public int getBaseAttack()
	{
		return baseAttak;
	}

	public Ability getPrimary()
	{
		return primary;
	}

	public Ability getSecondary()
	{
		return secondary;
	}

	public Ability getSpecialOne()
	{
		return specialOne;
	}

	public Ability getSpecialTwo()
	{
		return specialTwo;
	}

	public Ability getSpecialThree()
	{
		return specialThree;
	}
}
