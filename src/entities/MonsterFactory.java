package entities;

import java.util.ArrayList;

public class MonsterFactory
{
	public static ArrayList<Monster> spawnMonsterWave(int waveSize, int monsterType)
	{
		ArrayList<Monster> monsterWave = new ArrayList<Monster>(waveSize);
		
		for(int index = 0; index < waveSize; index++)
		{
			monsterWave.add(new Monster("Monster", Profession.MINION_MELEE, "/Entities/Mobs/Minions/OwlSpriteSheet1D.png"));
		}
		
		return monsterWave;
	}
}
