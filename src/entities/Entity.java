// ================================================================================================
// Class: Entity
// Primary Author: Travis Smith
// Last Updated: Dec 19, 2013
// ================================================================================================
// The Entity class will serve as the abstract Base class of all Heroes and Monsters in the game. 
// It will contain all shared data members and functions as well as the prototypes for the abstract
// methods required by the child classes.
// ================================================================================================

package entities;

import java.awt.Point;

public abstract class Entity
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size Constants
	public static final int ENTITY_WIDTH = 64;
	public static final int ENTITY_HEIGHT = 64;
	
	// Move Direction Constants
	public static final int UP = 0;
	public static final int LEFT = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;
	
	// Type Constants
	public static final int HERO = 0;
	public static final int MONSTER = 1;
	
	// Combat Data
	protected String name;
	protected Profession profession;
	protected int type;
	protected int maxHealth;
	protected int currHealth;
	protected boolean dead;
	protected int maxEnergy;
	protected int currEnergy;
	protected int moveRate;
	protected int armor;
	protected int attack;
	
	// Move Data
	protected Point location;
	protected boolean moving[];

	// Spritesheet Path
	protected String imagePath;

	// ============================================================================================
	// COnstructor
	// ============================================================================================

	public Entity(String name, int professionTag, int type, String imagePath)
	{
		// Initialize Game Data Members
		this.name = name;
		this.profession = new Profession(professionTag);
		this.type = type;
		this.currHealth = this.maxHealth = profession.getStartHealth();	
		this.dead = false;
		this.currEnergy = this.maxEnergy = profession.getStartEnergy();
		this.moveRate = profession.getBaseMoveRate();
		this.armor = profession.getBaseArmor();
		this.attack = profession.getBaseAttack();
		this.location = null;
		
		this.moving = new boolean[4];
		
		for(int index = 0; index < 4; index++)
		{
			moving[index] = false;
		}
		
		// Initialize Image Path
		this.imagePath = imagePath;
	}
	
	// ============================================================================================
	// Abstract Methods
	// ============================================================================================

	public abstract void takeTurn();
	public abstract void move(boolean pathing[][]);

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void takeDamage(int rawDamage)
	{
		int damage = (rawDamage - armor);
		
		currHealth -= damage;
		
		if(currHealth <= 0)
		{
			dead = true;
		}
	}
	
	public Point getCenter()
	{
		return new Point((location.x + (ENTITY_WIDTH / 2)), (location.y + (ENTITY_HEIGHT / 2)));
	}

	// Setters
	public void setName(String name)
	{
		this.name = name;
	}
	public void setProfession(Profession profession)
	{
		this.profession = profession;
	}	
	public void setType(int type)
	{
		this.type = type;
	}
	public void setMaxHealth(int maxHealth)
	{
		this.maxHealth = maxHealth;
	}
	public void setCurrHealth(int currHealth)
	{
		this.currHealth = currHealth;
	}
	public void setDead(boolean dead)
	{
		this.dead = dead;
	}
	public void setMaxEnergy(int maxEnergy)
	{
		this.maxEnergy = maxEnergy;
	}
	public void setCurrEnergy(int currEnergy)
	{
		this.currEnergy = currEnergy;
	}
	public void setMoveRate(int moveRate)
	{
		this.moveRate = moveRate;
	}
	public void setArmor(int armor)
	{
		this.armor = armor;
	}
	public void setAttack(int attack)
	{
		this.attack = attack;
	}
	public void setLocation(Point location)
	{
		this.location = location;
	}
	public void setImagePath(String imagePath)
	{
		this.imagePath = imagePath;
	}
	
	// Getters
	public String getName()
	{
		return name;
	}
	public Profession getProfession()
	{
		return profession;
	}
	public int getType()
	{
		return type;
	}	
	public int getMaxHealth()
	{
		return maxHealth;
	}
	public int getCurrHealth()
	{
		return currHealth;
	}
	public boolean isDead()
	{
		return dead;
	}
	public int getMaxEnergy()
	{
		return maxEnergy;
	}
	public int getCurrEnergy()
	{
		return currEnergy;
	}
	public int getMoveRate()
	{
		return moveRate;
	}
	public int getArmor()
	{
		return armor;
	}
	public int getAttack()
	{
		return attack;
	}
	public Point getLocation()
	{
		return location;
	}
	public boolean[] getMoveArray()
	{
		return moving;
	}
	public String getImagePath()
	{
		return imagePath;
	}
}
