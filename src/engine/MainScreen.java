package engine;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Arc2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import mapData.DungeonRoom;

@SuppressWarnings("serial")
public class MainScreen extends Canvas
{	
	// Main Data
	public static final String GAME_NAME = "Excursion";
	private Engine gameEngine;
	
	// Image Data
	private BufferedImage mainImage;
	private BufferStrategy bufferStrat;
	private int canvasWidth;
	private int canvasHeight;
	
	public MainScreen(final Engine gameEngine, int width, int height, DungeonRoom testRoom)
	{
		super();
		this.gameEngine = gameEngine;
		this.canvasWidth = width;
		this.canvasHeight = height;
		
		this.setMinimumSize(new Dimension(width, height));
		this.setPreferredSize(new Dimension(width, height));
		this.setMaximumSize(new Dimension(width, height));
		this.setIgnoreRepaint(true);
		this.requestFocus();
		this.setFocusTraversalKeysEnabled(false);
		this.addMouseListener(new ScreenMouseAdapter());
		
//		mainImage = testRoom.buildBackground();
	}
	
	public void initialize()
	{
		this.createBufferStrategy(3);
		bufferStrat = this.getBufferStrategy();
	}

	public void render()
	{
		int index = 0;
		
		Graphics gfx = bufferStrat.getDrawGraphics();
		if(mainImage == null)
		{
			gfx.setColor(Color.BLACK);
			gfx.fillRect(0, 0, canvasWidth, canvasHeight);	
		}
		else
		{
			gfx.setColor(Color.BLACK);
			gfx.fillRect(0, 0, canvasWidth, canvasHeight);	
			
			gfx.drawImage(mainImage, 0, 0, null);
		}
		
		for(index = 0; index < gameEngine.getEntitySprites().size(); index++)
		{
			gameEngine.getEntitySprites().get(index).render(gfx);
		}
		
		for(index = 0; index < gameEngine.getSpawners().size(); index++)
		{
			//TODO Render Spawners
		}
		
		for(index = 0; index < gameEngine.getPlayerProjectiles().size(); index++)
		{
			// TODO
		}
		
		for(index = 0; index < gameEngine.getPlayerArcs().size(); index++)
		{
			gameEngine.getPlayerArcs().get(index).render(gfx); 
		}
		
		bufferStrat.show();
		
		gfx.dispose();
	}
	
	private class ScreenMouseAdapter extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent event) 
		{
			if(SwingUtilities.isLeftMouseButton(event))
			{
				gameEngine.leftClick(event.getPoint());
			}
			else
			{
				gameEngine.rightClick(event.getPoint());
			}
		}
	}
}
