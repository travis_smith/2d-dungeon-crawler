// ================================================================================================
// Class: RegionNode
// ================================================================================================
// The RegionNode will represent a region of the game map and maintain an array of monsters that
// are currently inside the represented region. The majority of the classes methods will operate
// recursively.
// ================================================================================================

package engine;

import java.awt.Rectangle;
import java.util.ArrayList;

import entities.Entity;


public class RegionNode 
{
	// Child Region Constants
	public static final int NW = 0;
	public static final int NE = 0;
	public static final int SW = 0;
	public static final int SE = 0;
	
	// Data Members
	private ArrayList<Entity> currentMonsters;
	private Rectangle region;
	private boolean hasSplit;
	
	// Node Pointers
	private RegionNode parentNode;
	private RegionNode northWest;
	private RegionNode northEast;
	private RegionNode southWest;
	private RegionNode southEast;
	
	/* ==== Default Constructor ===================================================================
	 * The default constructor will be used to create the region this node will represent.
	 * ========================================================================================= */
	public RegionNode(int xPos, int yPos, int regionWidth, int regionHeight, RegionNode parentNode)
	{
		this.currentMonsters = new ArrayList<Entity>(0);
		this.region = new Rectangle(xPos, yPos, regionWidth, regionHeight);
		this.hasSplit = false;
		
		this.parentNode = parentNode;
		this.northWest = null;
		this.northEast = null;
		this.southWest = null;
		this.southEast = null;
	}

	/* ==== addMonster =============================================================================
	 * Will add a sprite to the array of sprites contained within this region unless the array 
	 * contains more than the max number specified by the RegionControl class. If the node
	 * contains too many spites and has not reached the minimum size specified by RegionControl, it
	 * will split. After splitting, the current sprites will be sent to the new child regions.
	 * ========================================================================================= */
	public void addMonster(Entity newMonster)
	{
		if(hasSplit)
		{
			if(newMonster.getLocation().x <= (region.width / 2))
			{
				if(newMonster.getLocation().y <= (region.height / 2))
				{
					northWest.addMonster(newMonster);
				}
				else
				{
					southWest.addMonster(newMonster);
				}
			}
			else
			{
				if(newMonster.getLocation().y <= (region.height / 2))
				{
					northEast.addMonster(newMonster);
				}
				else
				{
					southEast.addMonster(newMonster);
				}
			}
		}
		else if(currentMonsters.size() >= RegionControl.MAX_REGION_UNITS)
		{
			currentMonsters.add(newMonster);
			
			if((region.width > RegionControl.MIN_REGION_SIZE) && (region.height > RegionControl.MIN_REGION_SIZE))
			{
				split();
			}
		}
		else
		{
			currentMonsters.add(newMonster);
		}
	}
	
	/* ==== removeMonster =========================================================================
	 * The method will attempt to remove the passed sprite from the nodes array. If the sprite 
	 * has moved out of the region since the last time this node was updated, it will tell the
	 * parent node that the sprite is missing.
	 * ========================================================================================= */
	public boolean removeMonster(Entity monster)
	{
		if(hasSplit)
		{
			if(northWest.getRegion().contains(monster.getLocation()))
			{
				return northWest.removeMonster(monster);
			}		
			else if(northEast.getRegion().contains(monster.getLocation()))
			{
				return northEast.removeMonster(monster);
			}
			
			else if(southWest.getRegion().contains(monster.getLocation()))
			{
				return southWest.removeMonster(monster);
			}
			else // if(southEast.getRegion().contains(sprite.getLocation()))
			{
				return southEast.removeMonster(monster);
			}

		}
		else
		{
			if(currentMonsters.contains(monster))
			{
				currentMonsters.remove(monster);
				return true;
			}
			else
			{
				if(parentNode != null)
				{
					if(parentNode.monsterMissing(monster))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					System.out.println("Reached Parent Node for REMOVE: Sprite Not Found");
					return false;
				}
			}
		}
	}
	
	/* ==== monsterMissing =========================================================================
	 * This method will be called by a child node when it cannot find a sprite that it needs to 
	 * remove. The method will do a quick check of all its children to see if the sprite has not
	 * moved far. If the parent region cannot find the missing sprite it will call the huntSprite
	 * method.
	 * ========================================================================================= */
	private boolean monsterMissing(Entity monster)
	{
		if(hasSplit)
		{
			if(northWest.monsterMissing(monster))
			{
				return true;
			}
			else if(northEast.monsterMissing(monster))
			{
				return true;
			}
			else if(southWest.monsterMissing(monster))
			{
				return true;
			}
			else if(southEast.monsterMissing(monster))
			{
				return true;
			}
			else
			{
				if(parentNode != null)
				{
					if(parentNode.huntMonster(monster, this))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					System.out.println("Reached Parent Node for MISSING: Sprite Not Found");
					return false;
				}
			
			}
		}
		else
		{
			if(currentMonsters.contains(monster))
			{
				currentMonsters.remove(monster);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	/* ==== huntMonster ============================================================================
	 * huntSprite will systematically search for the missing sprite, it will eventually check all
	 * nodes for the missing sprite and return false if it is not found.
	 * ========================================================================================= */
	private boolean huntMonster(Entity monster, RegionNode callingChild)
	{
		if(callingChild != northWest)
		{
			if(northWest.searchForMonster(monster))
			{
				return true;
			}
		}
			
		if(callingChild != northEast)
		{
			if(northEast.searchForMonster(monster))
			{
				return true;
			}
		}
			
		if(callingChild != southWest)
		{
			if(southWest.searchForMonster(monster))
			{
				return true;
			}
		}
			
		if(callingChild != southEast)
		{
			if(southEast.searchForMonster(monster))
			{
				return true;
			}
		}
			
		// The Sprite is no longer in this branch.
		if(parentNode != null)
		{
			if(parentNode.huntMonster(monster, this))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			System.out.println("Reached Parent Node for HUNT: Sprite Not Found");
			return false;
		}
	}
	
	/* ==== searchForMonster =======================================================================
	 * This function is used by the huntSprite method. It will travel down to the leaf nodes of
	 * this branch and check if the sprite is located within it. If it is, it will remove the
	 * sprite and return true, otherwise it will return false.
	 * ========================================================================================= */
	private boolean searchForMonster(Entity monster)
	{
		if(hasSplit)
		{
			if(northWest.searchForMonster(monster))
			{
				return true;
			}
			else if(northEast.searchForMonster(monster))
			{
				return true;
			}
			else if(southWest.searchForMonster(monster))
			{
				return true;
			}
			else if(southEast.searchForMonster(monster))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if(currentMonsters.contains(monster))
			{
				currentMonsters.remove(monster);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	/* ==== updateSprites =========================================================================
	 * This method checks if a sprite currently contained within the array has moved outside of
	 * the nodes region. If it has it will call the parentNodes' spriteLeft method.
	 * ========================================================================================= */
	public void updateMonsters()
	{
		if(hasSplit)
		{
			northWest.updateMonsters();
			northEast.updateMonsters();
			southWest.updateMonsters();
			southEast.updateMonsters();
		}
		else
		{
			// Check if Sprite has Moved Outside the Region
			for(int index = 0; index < currentMonsters.size(); index++)
			{
				if(!(region.contains(currentMonsters.get(index).getLocation())))
				{
					if(parentNode != null)
					{
						parentNode.monsterLeft(currentMonsters.get(index));
						currentMonsters.remove(index);
					}
					else
					{
						// Sprite is not in region tree
						System.out.println("Reached Parent Node for UPDATE: Sprite Not Found");
					}
				}
			}
		}
	}
	
	/* ==== getNumAllMonsters ======================================================================
	 * Will return all sprites contained in this node and all child nodes arrays.
	 * ========================================================================================= */
	public int getNumAllMonsters()
	{
		if(hasSplit)
		{
			int temp = 0;
			
			temp += northWest.getNumAllMonsters();
			temp += northEast.getNumAllMonsters();
			temp += southWest.getNumAllMonsters();
			temp += southEast.getNumAllMonsters();
			
			return temp;		
		}
		else
		{
			System.out.println("Current Region Num Sprites: " + currentMonsters.size());
			return currentMonsters.size();
		}
	}
	
	/* ==== regionCheck ===========================================================================
	 * Return an array containing all regionNodes whose regions intersect with the passed zone.
	 * ========================================================================================= */
	public void regionCheck(Rectangle zone, ArrayList<RegionNode> affectedRegions) 
	{
		// Add Leaf Nodes (Base Case)
		if(!hasSplit)
		{
			affectedRegions.add(this);
			return;
		}
		
		// Check Child Nodes
		if(zone.intersects(northWest.getRegion()))
		{
			northWest.regionCheck(zone, affectedRegions);
		}
		if(zone.intersects(northEast.getRegion()))
		{
			northEast.regionCheck(zone, affectedRegions);
		}
		if(zone.intersects(southWest.getRegion()))
		{
			southWest.regionCheck(zone, affectedRegions);
		}
		if(zone.intersects(southEast.getRegion()))
		{
			southEast.regionCheck(zone, affectedRegions);
		}
	}
	
	/* ==== split =================================================================================
	 * Method splits the region into four parts and divides its array of sprites among them.
	 * ========================================================================================= */
	private void split()
	{
		System.out.println("Region Split");
		hasSplit = true;
		
		// Create Child Nodes
		northWest = new RegionNode(region.x, region.y, (region.width / 2), (region.height / 2), this);
		northEast = new RegionNode((region.x + (region.width / 2)), region.y, (region.width / 2), (region.height / 2), this);
		southWest = new RegionNode(region.x, (region.y + (region.height / 2)), (region.width / 2), (region.height / 2), this);
		southEast = new RegionNode((region.x + (region.width / 2)), (region.y + (region.height / 2)), (region.width / 2), (region.height / 2), this);
		
		// Split Sprite Array
		for(int index = 0; index < currentMonsters.size(); index++)
		{
			addMonster(currentMonsters.get(index));
			currentMonsters.remove(index);
		}
	}
	
	/* ==== spriteLeft ============================================================================
	 * Method checks if a moving sprite is still contained within the nodes region. If it has not
	 * it will ask the parentNode to check. This is called when updating the nodes.
	 * ========================================================================================= */
	private void monsterLeft(Entity monster)
	{
		if(region.contains(monster.getLocation()))
		{
			addMonster(monster);
		}
		else
		{
			if(parentNode != null)
			{
				parentNode.monsterLeft(monster);
			}
			else
			{
				System.out.println("Sprite Location: " + monster.getLocation().x + ", " + monster.getLocation().y 
													+ "Region Cords: " + region.x + ", " + region.y + "Width: "
													+ region.width + "Height: " + region.height);
				System.out.println("Reached Parent Node for spriteLeft: Sprite Not Found");
				// Sprite Not In Region
			}
		}
	}
	
	// Getters
	public Rectangle getRegion()
	{
		return region;
	}
	public ArrayList<Entity> getSprites()
	{
		return currentMonsters;
	}
	public int getNumSprites()
	{
		return currentMonsters.size();
	}
}
