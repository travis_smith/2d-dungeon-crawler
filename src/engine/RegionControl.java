// ================================================================================================
// Class: RegionControl
// ================================================================================================
// The RegionControl class will act as the intermediary between the game engine and the actual
// quad tree used for collision checking. It will contain the root node as well as the public
// data members that control the trees behavior. 
// ================================================================================================

package engine;

import java.awt.Rectangle;
import java.util.ArrayList;

import entities.Entity;


public class RegionControl 
{
	// Class Constants
	public final static int MIN_REGION_SIZE = 256;
	public final static int MAX_REGION_UNITS = 5;
	public final static int FULL_UPDATE_LOOPS = 20;
	
	// Root Node
	private RegionNode root;
	
	// Main Data
	private int mapWidth;
	private int mapHeight;
	private int updateLoops;
	
	/* ==== Default Constructor ===================================================================
	 * The default constructor will take the width and height of the map and use that to initialize
	 * the root nodes region. 
	 * ========================================================================================= */
	public RegionControl(int mapWidth, int mapHeight)
	{
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.updateLoops = 0;
		
		System.out.println("Root Region Width: " + mapWidth + " Height: " + mapHeight);
		this.root = new RegionNode(0, 0, this.mapWidth, this.mapHeight, null);
	}
	
	/* ==== getSpritesInZone ======================================================================
	 * The function will return all the sprites that fall within or near the passed zone. 
	 * ========================================================================================= */
	public ArrayList<Entity> getMonstersInZone(Rectangle zone)
	{
		ArrayList<Entity> affectedMonsters = new ArrayList<Entity>(1);
		ArrayList<RegionNode> affectedRegions = new ArrayList<RegionNode>(0);
		
		root.regionCheck(zone, affectedRegions);
		
		for(int index = 0; index < affectedRegions.size(); index++)
		{
			affectedMonsters.addAll(affectedRegions.get(index).getSprites());
		}
		
		return affectedMonsters;
	}
	
	/* ==== update ================================================================================
	 * The update method will check if the updateLoops counter has reached the maximum loops
	 * required for a full update. The full update will go through each and every node, updating
	 * the sprite arrays and rearranging them to maintain accuracy as the sprites move. If the 
	 * loops have not reached the maximum, then only the nodes representing the regions contained 
	 * in, or near the passed playerZone will be updated.
	 * ========================================================================================= */
	public void update(Rectangle playerZone)
	{
		if(updateLoops >= FULL_UPDATE_LOOPS)
		{
			root.updateMonsters();
			updateLoops = 0;
		}
		else
		{
			ArrayList<RegionNode> affectedRegions = new ArrayList<RegionNode>(0);
			root.regionCheck(playerZone, affectedRegions);
			
			for(int index = 0; index < affectedRegions.size(); index++)
			{
				affectedRegions.get(index).updateMonsters();
			}
			
			updateLoops++;
		}
	}
	
	/* ==== getNumSprites =========================================================================
	 * Return the total number of sprites currently contained in ALL RegionNodes sprite arrays.
	 * ========================================================================================= */
	public int getNumMonsters()
	{
		return root.getNumAllMonsters();
	}
	
	// Add/Remove Sprites
	public void addSprite(Entity newSprite)
	{
		root.addMonster(newSprite);
	}
	public void removeSprite(Entity sprite)
	{
		root.removeMonster(sprite);
	}
}
