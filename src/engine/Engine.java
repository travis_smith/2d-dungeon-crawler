// ================================================================================================
// Class: Engine
// ================================================================================================
// The engine class will be responsible for controlling the game loops, updating data, and calling
// graphical class' render functions.
// ================================================================================================

package engine;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import objects.roomObjects.Spawner;

import mapData.DungeonRoom;
import sprites.EntitySprite;
import sprites.SectorSprite;
import sprites.Projectile;

import combat.Ability;

import entities.Entity;
import entities.Hero;
import entities.Monster;
import entities.Profession;

public class Engine implements Runnable
{
	// Loop Constants
	private static final double UPDATE_RATE = 60D; 
	
	private boolean running;
	
	// Region Tree
	private RegionControl regionTree;
	
	// Sprites
	private Hero player;
	private EntitySprite playerSprite;
	private ArrayList<Monster> mobs;
	private ArrayList<EntitySprite> entitySprites;
	private ArrayList<Projectile> playerProj;
	private ArrayList<Spawner> spawns;
	private ArrayList<SectorSprite> playerArcs;
	
	// Test Members
	private Rectangle testPathing;
	private MainScreen mainScreen;
	private int numMobsSpawned;
	public int mobsRemoved;
	private Ability meleeAbility;
	private Ability rangedAbility;
	
	public Engine(DungeonRoom testRoom)
	{
		running = false;
//		testPathing = testRoom.getPathingRectangle();
		spawns = new ArrayList<Spawner>(1);
		createTestSpawner();
		createTestAbility();

		player = new Hero("Hero Name", Profession.HERO_WIZARD, "/Entities/Heroes/Wizard/WizardSpriteSheet4D.png");	
		playerSprite = new EntitySprite(player);
		entitySprites = new ArrayList<EntitySprite>(0);
		entitySprites.add(playerSprite);
		
		// Set-Up Region Controller
//		regionTree = new RegionControl(testRoom.getPathingRectangle().width, testRoom.getPathingRectangle().height);
		
		mobs = new ArrayList<Monster>(0);
		playerProj = new ArrayList<Projectile>(0);
		playerArcs = new ArrayList<SectorSprite>(0);
		numMobsSpawned = 0;
		mobsRemoved = 0;
	}
	
	private void createTestAbility()
	{

	}
	
	/* ==== run ===================================================================================
	 * This function contains the main game loop.
	 * ========================================================================================= */
	@Override
	public void run() 
	{
		// Prepare Loop Control Variables
		long lastLoopStartTime = System.nanoTime();				// Use NanoTime for More Accuracy; used for function calls
		double nanoSecPerLoop = (1000000000D / UPDATE_RATE); 	// One Billion nanoseconds = 1 second
																// This updates "UPDATE_RATE" times per second
		double loopTimer = 0;
		long currentLoopStartTime;
		
		// Prepare Counter Variables
		long counterStartTime = System.currentTimeMillis();
		int frames = 0;
		int updates = 0;

		// Main Loop
		while(running)
		{
			currentLoopStartTime = System.nanoTime();
			loopTimer += (currentLoopStartTime - lastLoopStartTime) / nanoSecPerLoop;
			lastLoopStartTime = currentLoopStartTime;
			
			if(loopTimer >= 1)
			{
				updates++;
				updateData();
				loopTimer -= 1;
			}
			
			// Render 
			frames++;
			mainScreen.render();
					
			if((System.currentTimeMillis() - counterStartTime) >= 1000)
			{
				counterStartTime = System.currentTimeMillis();
				System.out.println("Updates: " + updates + " Frames: " + frames);
				updates = 0;
				frames = 0;
			}
		}
	}
	
	/* ==== run ===================================================================================
	 * The update function will be called after the game loops reach the UPDATE_RATE constant. It
	 * will tell all contained classes to update their data such as locations and if they are dead.
	 * ========================================================================================= */
	private void updateData()
	{
		// TODO
	}
	
	private void checkCollision(Projectile proj) 
	{
		// TODO
	}
	
	private void checkCollision(EntitySprite mob) 
	{
		// TODO
	}
	
	private void checkCollision(SectorSprite playerArc)
	{
		//TODO
	}
	
	private void createTestSpawner()
	{
		Spawner tempSpawn = new Spawner();
	//	tempSpawn.setController(this);
		tempSpawn.setSpawnRate(60);
		tempSpawn.setSpawnType(Spawner.MINION);
		tempSpawn.setWaveSize(3);
		tempSpawn.setLocation(new Point((testPathing.x + testPathing.width - 250), (testPathing.y + testPathing.height - 250)));
		spawns.add(tempSpawn);
	}
	
	/* ==== start =================================================================================
	 * Function will start the game, should only be called after all data has been initialized.
	 * ========================================================================================= */
	public synchronized void start() 
	{
		running = true;
		new Thread(this).start();
	}
	
	/* ==== stop ==================================================================================
	 * Causes the game engine to exit its' infinite loop, this should be called prior to exiting, 
	 * or when switching levels.
	 * ========================================================================================= */
	public synchronized void stop() 
	{
		running = false;
	}
	
	public void removeProjectile(Projectile projectile) 
	{
		playerProj.remove(projectile);
	} 
	
	public Hero getHero()
	{
		return player;
	}
	
	public ArrayList<Monster> getMobs()
	{
		return mobs;
	}
	
	public ArrayList<EntitySprite> getEntitySprites()
	{
		return entitySprites;
	}
	
	public ArrayList<Projectile> getPlayerProjectiles()
	{
		return playerProj;
	}
	
	public ArrayList<Spawner> getSpawners()
	{
		return spawns;
	}
	
	public ArrayList<SectorSprite> getPlayerArcs()
	{
		return playerArcs;
	}

	public void spawnMob(int spawnType, Point location) 
	{
		//TODO
	}
	
	public RegionControl getRegionController()
	{
		return regionTree;
	}
	
	public void leftClick(Point loc)
	{
	
	}
	
	public void rightClick(Point loc)
	{

	}
	
	public void setMainScreen(MainScreen screen)
	{
		this.mainScreen = screen;
	}
	
	public int getNumMobsSpawned()
	{
		return numMobsSpawned;
	}
	
}


