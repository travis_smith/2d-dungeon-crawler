// ================================================================================================
// Class: Projectile
// Primary Author: Travis Smith
// Last Updated: Dec 24, 2013
// ================================================================================================
// The Projectile class will be used to represent the ranged abilities of heros and monsters. The
// class will calculate its' movement based on the angle it is passed in its' constructor and move
// at the specified movement rate. Projectiles are created by Abilities that use them, and then
// added to the game by the engine.
// ================================================================================================

package sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import combat.Ability;
import combat.CombatMath;


public class Projectile
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Constants
	public static final int PROXIMITY_TRIGGER = 10;
	public static final int COLLISION_RADIUS = 10;
	
	// Parent Ability 
	private Ability parent;
	
	// Location Data
	private double currentX;
	private double currentY;
	private double fireAngle;
	
	// Movement Data
	private Point destination;
	private double velocity;
	private int maxRange;
	private int distanceMoved;
	private boolean removeNextLoop;
	private boolean destinationReached;
	
	// Target Data
	private boolean ignoreHits;
	private boolean piercing;
	private int targetsHit;
	private int maxHits;
	private int arcRadius;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Projectile(Ability parentAbility, Point start, Point dest, double angle, int maxRange
						, int velocity, boolean ignoreHits, boolean piercing, int maxHits, int arcRadius)
	{
		// Initialize Parent
		this.parent = parentAbility;
		
		// Initialize Location Data	
		this.currentX = (double)start.x;
		this.currentY = (double)start.y;
		this.fireAngle = angle;
		
		// Initialize Movement Data
		this.destination = dest;
		this.velocity = velocity;
		this.maxRange = maxRange;
		this.distanceMoved = 0;
		this.removeNextLoop = false;
		this.destinationReached = false;
		
		// Initialize Target Data
		this.ignoreHits = ignoreHits;
		this.piercing = piercing;
		this.targetsHit = 0;
		this.maxHits = maxHits;
		this.arcRadius = arcRadius;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public Rectangle getBounds()
	{
		return new Rectangle((int)currentX, (int)currentY, COLLISION_RADIUS, COLLISION_RADIUS);
	}
	
	/* ==== move ==================================================================================
	 * The move method will update the currentX and currentY members by adding the product of the 
	 * velocity and the cos/sin of the firing angle. 
	 * 
	 * Each time move is called, the distanceMoved member is incremented by the velocity and the 
	 * function should check if the destination has been reached, if the maxRange or destination 
	 * has been reached, the function should return true, otherwise false. 
	 * ========================================================================================= */
	public void move()
	{	
		double xMove;
		double yMove;
		
		if(fireAngle == 0)
		{
			xMove = velocity;
			yMove = 0;
		}
		else if(fireAngle == 90)
		{
			xMove = 0;
			yMove = -velocity;
		}
		else if(fireAngle == 180)
		{
			xMove = -velocity;
			yMove = 0;
		}
		else if(fireAngle == 270)
		{
			xMove = 0;
			yMove = velocity;
		}
		else
		{
			xMove = (velocity * Math.cos(fireAngle));
			yMove = -(velocity * Math.sin(fireAngle));
		}
		
		currentX += xMove;
		currentY += yMove;
		
		distanceMoved += velocity;
		
		int distanceToDest = CombatMath.getDistanceTo(new Point((int)currentX, (int)currentY), destination);
		
		if((distanceMoved >= maxRange) || (distanceToDest <= PROXIMITY_TRIGGER))
		{
			destinationReached = true;
		}
	}
	
	public void render(Graphics gfx)
	{
		gfx.setColor(Color.RED);
		
		gfx.fillRect((int)currentX, (int)currentY, COLLISION_RADIUS, COLLISION_RADIUS);

	}
	
	public void targetHit()
	{
		if(!piercing)
		{
			removeNextLoop = true;
		}
		else
		{
			targetsHit++;
		
			if(targetsHit >= maxHits)
			{
				removeNextLoop = true;
			}
		}
	}
	
	public SectorSprite buildProjectileAoE()
	{
		return new SectorSprite(parent, new Point((int)currentX, (int)currentY), new Point((int)currentX, (int)currentY), arcRadius, (2 * Math.PI));
	}
	
	public void removeNextLoop()
	{
		removeNextLoop = true;
	}
	
	// Getters
	public Ability getParent()
	{
		return parent;
	}
	public boolean desitnationReached()
	{
		return destinationReached;
	}
	public boolean needsRemoval()
	{
		return removeNextLoop;
	}
	public boolean isIgnoringHits()
	{
		return ignoreHits;
	}
	public boolean isPiercing()
	{
		return piercing;
	}
	public int getTargetsHit()
	{
		return targetsHit;
	}
	public int getMaxHits()
	{
		return maxHits;
	}
}
