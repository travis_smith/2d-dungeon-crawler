// ================================================================================================
// Class: EntitySprite
// Primary Author: Travis Smith
// Last Updated: Dec 23, 2013
// ================================================================================================
// The EntitySprite class will serve as the SuperClass to all HeroSprites and MobSprites. It will 
// be used to load the proper images, maintain data relative to both heros and monsters, as well 
// as handle the graphical movement of heros and monsters.
// ================================================================================================

package sprites;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import entities.Entity;

public class EntitySprite 
{	
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Class Constants
	public static final int ANIM_FRAME_WIDTH = 64;
	public static final int ANIM_FRAME_HEIGHT = 64;
	public static final int COLLISION_RADIUS = 64;
	
	// Animation Members
	private String imagePath;
	private BufferedImage animations[][]; 
	private int directionIndex;
	private int animDirections;
	private int animationIndex;
	private int framesCount;
	private int maxAnimLoops;
	private BufferedImage currModel;
	private int animLoopCount;
	
	// Entity
	private Entity entity;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public EntitySprite(Entity entity)
	{	
		// Initialize Animation Data
		this.entity = entity;
		this.imagePath = entity.getImagePath();
		initializeAnimationData();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	/* ==== initializeAnimationData ===============================================================
	 * This method will attempt to load the sprite sheet referenced by the imagePath. Using
	 * the dimensions of the sprite sheet it will create an array of appropriate size and 
	 * initialize each array member with a buffered image pulled from the spritesheet.
	 * 
	 * Spritesheets will be built with either 1, 2, or 4 rows. The columns are not restricted.
	 * 
	 * In 1 row sheets, the sprite will only "face" one direction and will have a frame for each 
	 * column.
	 * 
	 * In 2 row sheets, the sprite will have a left, and right animation sequence with the number
	 * of frames for each specified by the longest column of the two rows. 
	 * 
	 * In 4 row sheets, the sprite will have an up, left, down, and right animation sequence
	 * with the number of frames per direction specified by the longest column in the sheet.
	 * ========================================================================================= */
	private void initializeAnimationData()
	{
		BufferedImage spriteSheet;
		
		try 
		{
			spriteSheet = ImageIO.read(getClass().getResourceAsStream(imagePath));
		} 
		catch (IOException e) 
		{
			animations = new BufferedImage[1][1];
			animations[0][0] = null;
			directionIndex = 0;
			animDirections = 1;
			animationIndex = 0;
			maxAnimLoops = 0;
			framesCount = 1;
			currModel = null;
			animLoopCount = 0;
			return;
		}
		
		animDirections = (spriteSheet.getHeight() / ANIM_FRAME_HEIGHT);
		framesCount = (spriteSheet.getWidth() / ANIM_FRAME_WIDTH);
		
		animations = new BufferedImage[animDirections][framesCount];
		
		int rIndex = 0;
		int cIndex = 0;
		
		if((animDirections == 1) || (animDirections == 2) || (animDirections == 4))
		{
			for(rIndex = 0; rIndex < animDirections; rIndex++)
			{
				for(cIndex = 0; cIndex < framesCount; cIndex++)
				{
					animations[rIndex][cIndex] = new BufferedImage(ANIM_FRAME_WIDTH, ANIM_FRAME_HEIGHT, BufferedImage.TYPE_INT_ARGB);
				
					Graphics2D gfx = animations[rIndex][cIndex].createGraphics();
				
					gfx.drawImage(spriteSheet, 0, 0, (ANIM_FRAME_WIDTH - 1), (ANIM_FRAME_HEIGHT - 1)
							, (cIndex * ANIM_FRAME_WIDTH), (rIndex * ANIM_FRAME_HEIGHT)
							, (((cIndex + 1) * ANIM_FRAME_WIDTH) - 1), (((rIndex + 1) * ANIM_FRAME_HEIGHT) - 1)
							, null);
				
					gfx.dispose();
				}
				
				cIndex = 0;
			}
			
			directionIndex = 0;
			animationIndex = 0;
			currModel = animations[directionIndex][animationIndex];
			maxAnimLoops = (32 / framesCount);
			animLoopCount = 0;
		}
		else
		{
			animations = new BufferedImage[1][1];
			animations[0][0] = null;
			directionIndex = 0;
			animDirections = 1;
			animationIndex = 0;
			maxAnimLoops = 0;
			framesCount = 1;
			currModel = null;
			animLoopCount = 0;
			return;
		}
	}
	
	/* ==== animate ===============================================================================
	 * This method will adjust the currModel based on the direction index and the animation index. 
	 * Each time this method is called it will check if enough calls have been made to change the
	 * animation, this prevents the frames from changing to rapidly. If enough frames have gone by
	 * the animation index will increment and the direction index will be adjusted by the moving
	 * booleans.
	 * 
	 * In a two directional animation sheets, 0 = LEFT and 1 = RIGHT
	 * ========================================================================================= */
	private void animate()
	{
		if(animLoopCount < maxAnimLoops)
		{
			animLoopCount++;
			return;
		}
		
		boolean moving[] = entity.getMoveArray();
		
		animationIndex ++;
		
		if(animationIndex >= framesCount)
		{
			animationIndex = 0;
		}
		
		if(animDirections == 1)
		{
			currModel = animations[directionIndex][animationIndex];
		}
		else if(animDirections == 2)
		{
			if(moving[Entity.LEFT])
			{
				directionIndex = 0;
			}
			else if(moving[Entity.RIGHT])
			{
				directionIndex = 1;
			}
		}	
		else if(animDirections == 4)
		{
			if(moving[Entity.UP])
			{
				directionIndex = Entity.UP;
			}	
			else if(moving[Entity.LEFT])
			{
				directionIndex = Entity.LEFT;
			}	
			else if(moving[Entity.DOWN])
			{
				directionIndex = Entity.DOWN;
			}
			else if(moving[Entity.RIGHT])
			{
				directionIndex = Entity.RIGHT;
			}	
		}
		
		currModel = animations[directionIndex][animationIndex];
		animLoopCount = 0;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public Rectangle getHitBox()
	{
		if(currModel != null)
		{
			return new Rectangle(entity.getLocation().x, entity.getLocation().y, currModel.getWidth(), currModel.getHeight());
		}
		else
		{
			return new Rectangle(entity.getLocation().x, entity.getLocation().y, COLLISION_RADIUS, COLLISION_RADIUS);
		}
	}
	
	public void render(Graphics gfx)
	{
		animate();
		
		if(currModel != null)
		{
			gfx.drawImage(currModel, entity.getLocation().x, entity.getLocation().y, null);
		}
		else
		{
			if(entity.getType() == Entity.HERO)
			{
				gfx.setColor(Color.BLUE);
			}
			else
			{
				gfx.setColor(Color.RED);
			}
			gfx.fillRect(entity.getLocation().x, entity.getLocation().y, COLLISION_RADIUS, COLLISION_RADIUS);
		}
	}
	
	public Entity getEntity()
	{
		return entity;
	}
}
