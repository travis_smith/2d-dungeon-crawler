// ================================================================================================
// Class: Launcher
// ================================================================================================
// The launcher is where program execution will begin. It will use Swing to create the LaunchFrame
// that will allow the user to choose the next step.
// ================================================================================================

package launcher;

import javax.swing.SwingUtilities;

public class Launcher 
{
	/* ===== main =================================================================================
	 * The main function will prepare a runnable to be passed into Swing's Dispatching Thread.
	 * The runnable will be used to create the LaunchFrame.
	 * ========================================================================================= */
	public static void main(String args[])
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run() 
			{
				new LaunchFrame();
			}
			
		});
	}
}
