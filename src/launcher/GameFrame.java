package launcher;

import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import mapData.DungeonRoom;
import engine.Engine;
import engine.MainScreen;

@SuppressWarnings("serial")
public class GameFrame extends JFrame
{
	// Size Constants
	public static final int GAME_WIDTH = 1024;
	public static final int GAME_HEIGHT = 768;
	
	// Canvas
	private MainScreen gameScreen;
	private Engine gameEngine;
	
	public GameFrame(GraphicsDevice gDev, DungeonRoom testRoom)
	{
		super(gDev.getDefaultConfiguration());
	
		this.setResizable(false);
		this.setUndecorated(true);
		this.setIgnoreRepaint(true);	
		this.getContentPane().setLayout(new BorderLayout());
		this.gameEngine = new Engine(testRoom);
	
		createInputMap();
		
		boolean canFullScreen = gDev.isFullScreenSupported();
		
		if(canFullScreen)
		{
			this.gameScreen = new MainScreen(gameEngine, gDev.getDisplayMode().getWidth(), gDev.getDisplayMode().getHeight(), testRoom);
			this.getContentPane().add(gameScreen, BorderLayout.CENTER);	
			gDev.setFullScreenWindow(this);
			this.gameScreen.initialize();
			this.gameEngine.setMainScreen(gameScreen);
			this.gameEngine.start();
		}
		else
		{
			this.gameScreen = new MainScreen(gameEngine, GAME_WIDTH, GAME_HEIGHT, testRoom);
			this.getContentPane().add(gameScreen, BorderLayout.CENTER);	
			this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			this.pack();
			this.setVisible(true);
			this.gameScreen.initialize();
			this.gameEngine.setMainScreen(gameScreen);
			this.gameEngine.start();
		}
	}
	
	private void createInputMap()
	{
		InputMap inputMap = ((JPanel)this.getContentPane()).getInputMap();
		ActionMap actionMap = ((JPanel)this.getContentPane()).getActionMap();
		
		Action escapePressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.stop();
				System.out.println("Mobs Spawned: " + gameEngine.getNumMobsSpawned() + " Mobs Removed: " + gameEngine.mobsRemoved 
													+ " Mobs In Region Tree: " + gameEngine.getRegionController().getNumMonsters()
													+ " Mobs In Engine Array: " + gameEngine.getMobs().size());
				System.exit(0);
			}
		};
		Action enterPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// TODO
			}
		};
		Action wPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingUp(true);
			}
		};
		Action wReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingUp(false);
			}
		};
		
		Action aPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingLeft(true);
			}
		};
		Action aReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingLeft(false);
			}
		};
		
		Action sPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingDown(true);
			}
		};
		Action sReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingDown(false);
			}
		};
		
		Action dPressed = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingRight(true);
			}
		};
		Action dReleased = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent e)
			{
				gameEngine.getHero().movingRight(false);
			}
		};
		
		inputMap.put(KeyStroke.getKeyStroke("ESCAPE"), "Escape Pressed");
		inputMap.put(KeyStroke.getKeyStroke("ENTER"), "Enter Pressed");
		inputMap.put(KeyStroke.getKeyStroke('w'), "W Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released W"), "W Released");
		inputMap.put(KeyStroke.getKeyStroke('a'), "A Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released A"), "A Released");
		inputMap.put(KeyStroke.getKeyStroke('s'), "S Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released S"), "S Released");
		inputMap.put(KeyStroke.getKeyStroke('d'), "D Pressed");
		inputMap.put(KeyStroke.getKeyStroke("released D"), "D Released");
		
		actionMap.put("Escape Pressed", escapePressed);
		actionMap.put("Enter Pressed", enterPressed);
		actionMap.put("W Pressed", wPressed);
		actionMap.put("W Released", wReleased);
		actionMap.put("A Pressed", aPressed);
		actionMap.put("A Released", aReleased);
		actionMap.put("S Pressed", sPressed);
		actionMap.put("S Released", sReleased);
		actionMap.put("D Pressed", dPressed);	
		actionMap.put("D Released", dReleased);
	}
}
