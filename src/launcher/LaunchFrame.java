// ================================================================================================
// Class: LaunchFrame
// ================================================================================================
// Class extends and displays a JFrame when the program first launches. This frame will provide
// options to the user and allow them to choose between the editor and the game. It will also
// provide an interface that allows the user to select the preferred graphics configuration. The
// class will never be saved out and therefore a serialized ID is not used.
// ================================================================================================

package launcher;

import editor.roomModule.RoomEditorMain;

import java.awt.BorderLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import mapData.DungeonRoom;

@SuppressWarnings("serial")
public class LaunchFrame extends JFrame 
{	
	/* ===== LaunchFrame: Default Constructor =====================================================
	 * The default constructor will initialize the frame and set up its main content pane. It will
	 * position the launcher at the center of the screen.
	 * ========================================================================================= */
	public LaunchFrame()
	{
		super();
		
		// Set-Up Main Content Pane
		JPanel content = new JPanel();
		content.setLayout(new BorderLayout(5, 5));
		content.add(createTitlePanel(), BorderLayout.CENTER);
		content.add(createButtonPanel(), BorderLayout.SOUTH);
		
		// Set-Up Frame
		this.setContentPane(content);
		this.setResizable(false);
		this.pack();
		this.setLocation(getCenter());
		this.setVisible(true);		
	}
	
	/* ===== createButtonPanel ====================================================================
	 * Function will initialize all buttons needed for the user to select what he/she wants and
	 * add them to the JPanel that will be returned.
	 * ========================================================================================= */
	private JPanel createButtonPanel()
	{
		JPanel panel = new JPanel();
		
		JButton testButton = new JButton("Test Room");
		testButton.setEnabled(false);
		testButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				startTester();
			}
			
		});
		
		JButton editButton = new JButton("Editor");
		editButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				startEditor();
			}
			
		});
		
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(0);
			}
			
		});
		
		panel.add(testButton);
		panel.add(editButton);
		panel.add(exitButton);
		
		return panel;
	}
	
	/* ===== createTitlePanel =====================================================================
	 * Function creates a panel containing a label with the games title.
	 * ========================================================================================= */
	private JPanel createTitlePanel()
	{
		JPanel panel = new JPanel();
		panel.add(new JLabel("AWSOME GAME NAME"));
		
		return panel;
	}
	
	/* ===== getCenter ============================================================================
	 * Function uses the screens center along with the frames size to find the point that the
	 * frame's location should be set to. 
	 * ========================================================================================= */
	private Point getCenter()
	{
		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point center = gEnv.getCenterPoint();
		
		center.x -= (this.getWidth() / 2);
		center.y -= (this.getHeight() / 2);
		
		return center;
	}
	
	/* ===== startGame ============================================================================
	 * Function will load the games MainFrame, after it will dispose of the LaunchFrame
	 * ========================================================================================= */
	private void startTester()
	{
		// TODO
	}
	
	/* ===== startEditor ==========================================================================
	 * Function will load the editor, after it will dispose of the LaunchFrame
	 * ========================================================================================= */
	private void startEditor()
	{			
		new RoomEditorMain();
		
		this.dispose();
	}
}
