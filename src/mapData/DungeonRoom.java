// ================================================================================================
// Class: DungeonRoom
// ================================================================================================
// The DungeonRoom class encapsulates a single room inside a dungeon. It will maintain a grid of
// tiles that each contain an image path. Each room will maintain its' own pathing rectangle(s) as
// well as an arraylist of spawners. Dungeon rooms will be exported by the editor and then randomly 
// placed through the dungeon levels by the engine. 
// ================================================================================================

package mapData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class DungeonRoom implements Serializable
{
	private static final long serialVersionUID = 1;
	
	public String getName()
	{
		return "Name";
	}
	
	// Save/Load
	public void saveRoom()
	{
		String filePath = new String("resources\\rooms\\" + this.getName() + ".gmr");
		File fileName = new File(filePath);

		try 
		{
			FileOutputStream outStream = new FileOutputStream(fileName);
			
			try 
			{
				ObjectOutputStream save = new ObjectOutputStream(outStream);
				save.writeObject(this);
				save.flush();
				save.close();
			} 
			catch (InvalidClassException e) 
			{
				System.out.println("Invalid Class");
			}
			catch (IOException e) 
			{
				System.out.println("Error Starting Object Stream");
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("Error With File Stream");
		}
	}
	public static DungeonRoom loadRoom()
	{
		File filePath = new File("resources\\rooms\\");

		// Set-Up File Chooser Frame
		JFrame loadFrame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(filePath);
		int returnVal;
		File fileName;;
		
		returnVal = fileChooser.showOpenDialog(loadFrame);
		
		// Check User Selection and Load File
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			fileName = fileChooser.getSelectedFile();
			
			try
			{
				FileInputStream inStream = new FileInputStream(fileName);
				ObjectInputStream load = new ObjectInputStream(inStream);

				DungeonRoom newRoom = (DungeonRoom)load.readObject();
				load.close();
			
				System.out.println("Load Success");
				return newRoom;				
			}
			catch (FileNotFoundException e)
			{
				System.out.println(fileName.toString() + " :: was not Found");
				return null;
			} 
			catch (IOException e) 
			{
				System.out.println("IO Exception");
				return null;
			} 
			catch (ClassNotFoundException e) 
			{
				System.out.println("Class Not Found Exception");
				return null;
			}		
		}	
		else if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			System.out.println("Canceled");
			return null;
		}
		else
		{
			return null;
		}
	}

}
