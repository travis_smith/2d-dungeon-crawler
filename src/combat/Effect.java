package combat;

public class Effect
{
	// Secondary Effects Constants
	public static final int NONE = 0;
	public static final int PUSH = 1;
	
	public static final int WEAK = 0;
	public static final int MEDIUM = 1;
	public static final int STRONG = 2;
}
