// ================================================================================================
// Class: Melee
// Primary Author: Travis Smith
// Last Updated: Dec 24, 2013
// ================================================================================================
// The Melee class extends Ability and will contain data/methods needed for melee abilities.
// ================================================================================================

package combat;

import java.awt.Point;

import sprites.SectorSprite;

public class Melee extends Ability
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// Melee Data Members
	private int sectorRadius;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	public Melee(String name, int minDamage, int maxDamage, double arcDegree, int maxHits, Effect secondaryEffect, int sectorRadius)
	{
		super(name, MELEE, minDamage, maxDamage, arcDegree, maxHits, secondaryEffect);
		this.sectorRadius = sectorRadius;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public SectorSprite buildSectorSprite(Point origin, Point dest)
	{
		SectorSprite impact = new SectorSprite(this, origin, dest, sectorRadius, arcDegree);
		
		return impact;
	}

	// Getters
	public int getSectorRadius()
	{
		return sectorRadius;
	}
	
	// Setters
	public void setSectorRadius(int sectorRadius)
	{
		this.sectorRadius = sectorRadius;
	}
}
