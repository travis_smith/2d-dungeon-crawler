// ================================================================================================
// Class: Ability
// Primary Author: Travis Smith
// Last Updated: Dec 24, 2013
// ================================================================================================
// The Ability class will hold all information regarding the specific hero/mob abilities that can
// be executed in game. The class will contain all shared data members and functions. Ability will 
// be abstract.
// ================================================================================================

package combat;

import java.io.Serializable;

public abstract class Ability implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Ability Type Constants
	public static final int MELEE = 0;
	public static final int RANGED = 1;
	
	// Shared Data Members
	protected String name;
	protected int abilityType;
	protected int minDamage;
	protected int maxDamage;
	protected double arcDegree;
	protected int maxHits;
	protected Effect secondaryEffect;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Ability(String name, int type, int minDamage, int maxDamage, double arcDegree, int maxHits, Effect secondaryEffect)
	{
		this.name = name;
		this.abilityType = type;
		this.minDamage = minDamage;
		this.maxDamage = maxDamage;
		this.arcDegree = arcDegree;
		this.maxHits = maxHits;
		this.secondaryEffect = secondaryEffect;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Getters
	public String getName()
	{
		return name;
	}
	public int getAbilityType()
	{
		return abilityType;
	}
	public int getMinDamage()
	{
		return minDamage;
	}
	public int getMaxDamage()
	{
		return maxDamage;
	}
	public double getArcDegree()
	{
		return arcDegree;
	}
	public int getMaxHits()
	{
		return maxHits;
	}
	public Effect getSecondaryEffect()
	{
		return secondaryEffect;
	}

	// Setters
	public void setName(String name)
	{
		this.name = name;
	}
	public void setAbilityType(int abilityType)
	{
		this.abilityType = abilityType;
	}
	public void setMinDamage(int minDamage)
	{
		this.minDamage = minDamage;
	}
	public void setMaxDamage(int maxDamage)
	{
		this.maxDamage = maxDamage;
	}
	public void setArcDegree(double arcDegree)
	{
		this.arcDegree = arcDegree;
	}
	public void setMaxHits(int maxHits)
	{
		this.maxHits = maxHits;
	}
	public void setSecondaryEffect(Effect secondaryEffect)
	{
		this.secondaryEffect = secondaryEffect;
	}
}
