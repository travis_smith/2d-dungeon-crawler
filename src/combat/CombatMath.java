// ================================================================================================
// Class: CombatMath
// ================================================================================================
// The CombatMath class will contain various static functions that will be used for the games
// math related systems. This class will not contain a constructor and all methods should be
// static. 
// ================================================================================================

package combat;

import java.awt.Point;
import java.util.Random;

public class CombatMath 
{
	// Constants
	public static final int MELEE_RANGE = 10;
	public static final int SHOOT_RANGE = 10;
	
	/* ==== getAngelTo ============================================================================
	 * This method is used to find the angle from the origin to the destination points passed to
	 * the method. The Vertical Math may appear in reverse because the java GUI y-values
	 * increase as you move "down" on the screen. The angle returned will be from 0 to 2PI.
	 * ========================================================================================= */
	static public double getAngleTo(Point origin, Point dest)
	{
		double deltaY;
		double deltaX;
		
		double angle;
		
		// Check for 90 and 270
		if(origin.x == dest.x)
		{
			deltaY = dest.y - origin.y;
			
			if(deltaY < 0)
			{
				return (Math.PI / 2);
			}
			else
			{
				return ((3 * Math.PI) / 2);
			}
		}
		
		if(origin.y == dest.y)
		{
			deltaX = dest.x - origin.x;
			
			if(deltaX >= 0)
			{
				return 0;
			}
			else
			{
				return Math.PI;
			}
		}
		
		deltaY = -(dest.y - origin.y);
		deltaX = dest.x - origin.x;
		
		angle = (Math.atan2(deltaY, deltaX));		
		
		if(angle < 0)
		{
			angle += (2 * (Math.PI));
		}
		
		return angle;
	}
	
	/* ==== roller ================================================================================
	 * This method is used to generate a random integer between the minimum, inclusive, and the
	 * maximum, exclusive, that are passed to the method.
	 * ========================================================================================= */
	public static int roller(int min, int max)
	{
		if((min == 0) || (max == 0))
		{
			return 0;
		}
		
		Random rand = new Random(System.currentTimeMillis());
		
		int temp = rand.nextInt(max) + 1;
	
		if(temp < min)
		{
			temp = min;
		}
		
		return temp;
	}
	
	/* ==== getDistanceTo =========================================================================
	 * This method is used to find the distance between two points.
	 * ========================================================================================= */
	public static int getDistanceTo(Point origin, Point dest)
	{
		int xDist = (dest.x - origin.x);
		xDist *= xDist;
		
		int yDist = (dest.y - origin.y);
		yDist *= yDist;
		
		int totalDist = (int)Math.sqrt((double)(xDist + yDist));
		
		return totalDist;
	}
	
	/* ==== getDestination ========================================================================
	 * Given the origin, distance, and angle, this function will calculate the (x,y) of the end of 
	 * the line segment.
	 * ========================================================================================= */
	public static Point getDestination(Point origin, int distance, double angle)
	{
		int destX = (int)(origin.x + (distance * Math.cos(angle)));
		int destY = (int)(origin.y + (distance * Math.sin(angle)));
		
		return new Point(destX, destY);
	}
}
