// ================================================================================================
// Class: Ranged
// Primary Author: Travis Smith
// Last Updated: Dec 24, 2013
// ================================================================================================
// The Ranged class extends Ability and will contain data/methods needed for Ranged abilities.
// ================================================================================================

package combat;

import java.awt.Point;

import java.util.ArrayList;

import sprites.Projectile;

public class Ranged extends Ability
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// AoE Type Constants
	public static final int STAR_AOE = 0;
	public static final int ARC_AOE = 1;
	
	// Ranged Data Members
	private boolean isAoe;
	private int aoeType;
	private boolean ignoreHits;
	private boolean isPiercing;
	private int maxRange;
	private int velocity;
	private int numStars;
	private int arcRadius;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================

	public Ranged(String name, int minDamage, int maxDamage, double arcDegree, int maxHits, Effect secondaryEffect
					, boolean isAoe, int aoeType, boolean ignoreHits, boolean isPiercing, int maxRange, int velocity
					, int numStars, int arcRadius)
	{
		super(name, RANGED, minDamage, maxDamage, arcDegree, maxHits, secondaryEffect);

		this.isAoe = isAoe;
		this.aoeType = aoeType;
		this.ignoreHits = ignoreHits;
		this.isPiercing = isPiercing;
		this.maxRange = maxRange;
		this.velocity = velocity;
		this.numStars = numStars;
		this.arcRadius = arcRadius;
		

	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private double[] calcStarAngles(double facingAngle)
	{
		double angles[] = new double[numStars];
		double offset = (arcDegree / numStars);
		int numSideStars = (numStars / 2);
		double startAngle;
		
		if((numStars % 2) == 0)
		{
			startAngle = (facingAngle - (offset * numSideStars));
			startAngle += (offset / 2);
		}
		else
		{
			startAngle = (facingAngle - (offset * numSideStars));
		}

		for(int index = 0; index < numStars; index++)
		{
			angles[index] = (startAngle + (offset * index));
		}
		
		return angles;
	}
	
	private Point[] calcStarDestinations(Point origin, double angles[])
	{
		Point destinations[] = new Point[numStars];
		
		for(int index = 0; index < numStars; index++)
		{
			destinations[index] = CombatMath.getDestination(origin, maxRange, angles[index]);
		}
		
		return destinations;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public ArrayList<Projectile> buildProjectiles(Point origin, Point destination)
	{
		ArrayList<Projectile> projectiles = new ArrayList<Projectile>(1);
		
		if((!isAoe) || (aoeType == ARC_AOE))
		{
			Projectile newProj = new Projectile(this, origin, destination, CombatMath.getAngleTo(origin, destination),
												maxRange, velocity, ignoreHits, isPiercing, maxHits, arcRadius);
			projectiles.add(newProj);
			return projectiles;
		}
		else
		{
			if(aoeType == STAR_AOE)
			{
				double starAngles[] = calcStarAngles(CombatMath.getAngleTo(origin, destination));
				Point starDest[] = calcStarDestinations(origin, starAngles);
				
				Projectile tempProj;
				
				for(int index = 0; index < numStars; index++)
				{
					tempProj = new Projectile(this, origin, starDest[index], starAngles[index], maxRange,
												velocity, ignoreHits, isPiercing, maxHits, arcRadius);
					
					projectiles.add(tempProj);
				}
				
				return projectiles;
			}
			else
			{
				return null;
			}
		}
	}
	
	// Getters

	public boolean isAoe()
	{
		return isAoe;
	}

	public int getAoeType()
	{
		return aoeType;
	}

	public boolean isIgnoringHits()
	{
		return ignoreHits;
	}

	public boolean isPiercing()
	{
		return isPiercing;
	}

	public int getMaxRange()
	{
		return maxRange;
	}

	public int getVelocity()
	{
		return velocity;
	}

	public int getNumStars()
	{
		return numStars;
	}
	
	public int getArcRadius()
	{
		return arcRadius;
	}
	
	// Setters

	public void setAoe(boolean isAoe)
	{
		this.isAoe = isAoe;
	}

	public void setAoeType(int aoeType)
	{
		this.aoeType = aoeType;
	}

	public void setIgnoringHits(boolean ignoreHits)
	{
		this.ignoreHits = ignoreHits;
	}

	public void setPiercing(boolean isPiercing)
	{
		this.isPiercing = isPiercing;
	}

	public void setMaxRange(int maxRange)
	{
		this.maxRange = maxRange;
	}

	public void setVelocity(int velocity)
	{
		this.velocity = velocity;
	}

	public void setNumStars(int numStars)
	{
		this.numStars = numStars;
	}
	
	public void setArcRadius(int arcRadius)
	{
		this.arcRadius = arcRadius;
	}
}
