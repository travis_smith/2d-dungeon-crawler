// ================================================================================================
// Class: ObjectManager
// Primary Author: Travis Smith
// Last Updated: Dec 26, 2013
// ================================================================================================
// The object manager will contain an ArrayList of all types of objects available to the game. It
// will be responsible for updating, checking collisions, creating, modifying, removing, and 
// retrieving RoomObjects. Each DungeonRoom could have its' own ObjectManager or the Engine could 
// use one that contains the RoomObjects from all DungeonRooms.
// ================================================================================================
package objects.manager;

import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;

import objects.roomObjects.Door;
import objects.roomObjects.Hallway;
import objects.roomObjects.RoomGoal;
import objects.roomObjects.RoomObject;
import objects.roomObjects.Spawner;
import objects.roomObjects.hazards.MovingHazard;
import objects.roomObjects.hazards.StaticHazard;
import objects.roomObjects.hazards.TimedHazard;
import objects.roomObjects.triggers.MoveTrigger;
import objects.roomObjects.triggers.ObjectTrigger;
import sprites.EntitySprite;
import editor.roomModule.objects.ObjectKit;
import entities.Entity;

public class ObjectManager implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// ArrayLists
	private ArrayList<ObjectTrigger> objectTriggers;
	private ArrayList<MoveTrigger> moveTriggers;
	private ArrayList<StaticHazard> staticHazards;
	private ArrayList<TimedHazard> timedHazards;
	private ArrayList<MovingHazard> movingHazards;
	private ArrayList<Door> doors;
	private ArrayList<Hallway> hallways;
	private ArrayList<Spawner> spawners;
	private ArrayList<RoomGoal> roomGoals;
	
	// Worker Classes
	private ObjectFactory factory;
	private ObjectRenderer renderer;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public ObjectManager()
	{
		// Initialize Arrays
		this.objectTriggers = new ArrayList<ObjectTrigger>(0);
		this.moveTriggers = new ArrayList<MoveTrigger>(0);		
		this.staticHazards = new ArrayList<StaticHazard>(0);
		this.timedHazards = new ArrayList<TimedHazard>(0);
		this.movingHazards = new ArrayList<MovingHazard>(0);
		this.doors = new ArrayList<Door>(0);
		this.hallways = new ArrayList<Hallway>(0);
		this.spawners = new ArrayList<Spawner>(0);
		this.roomGoals = new ArrayList<RoomGoal>(0);

		// Initialize Worker Classes
		this.factory = new ObjectFactory();
		this.renderer = new ObjectRenderer(this);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Add Object
	public void addObject(RoomObject object)
	{
		if(object.getType() == RoomObject.OBJ_TRIGGER)
		{
			objectTriggers.add((ObjectTrigger)object);
		}
		else if(object.getType() == RoomObject.MOVE_TRIGGER)
		{
			moveTriggers.add((MoveTrigger)object);
		}
		else if(object.getType() == RoomObject.STATIC_HAZARD)
		{
			staticHazards.add((StaticHazard)object);
		}
		else if(object.getType() == RoomObject.TIMED_HAZARD)
		{
			timedHazards.add((TimedHazard)object);
		}
		else if(object.getType() == RoomObject.MOVING_HAZARD)
		{
			movingHazards.add((MovingHazard)object);
		}
		else if(object.getType() == RoomObject.DOOR)
		{
			doors.add((Door)object);
		}
		else if(object.getType() == RoomObject.HALLWAY)
		{
			hallways.add((Hallway)object);
		}
		else if(object.getType() == RoomObject.SPAWNER)
		{
			spawners.add((Spawner)object);
		}
		else if(object.getType() == RoomObject.ROOM_GOAL)
		{
			// TODO
		}
	}
	
	// Remove Object
	public void removeObject(RoomObject object)
	{
		// Remove Object From Any ObjectTrigger's activate list
		for(int index = 0; index < objectTriggers.size(); index++)
		{
			if(objectTriggers.get(index).getActivateList().contains(object))
			{
				objectTriggers.get(index).getActivateList().remove(object);
			}
		}	
		
		if(object.getType() == RoomObject.OBJ_TRIGGER)
		{
			objectTriggers.remove((ObjectTrigger)object);
		}
		else if(object.getType() == RoomObject.MOVE_TRIGGER)
		{
			// Remove This MoveTrigger from other MoveTrigger's Destinations
			for(int index = 0; index < moveTriggers.size(); index++)
			{
				if(moveTriggers.get(index).getDestination() == object)
				{
					moveTriggers.get(index).setDestination(null);
				}
			}	
					
			moveTriggers.remove((MoveTrigger)object);
		}
		else if(object.getType() == RoomObject.STATIC_HAZARD)
		{
			staticHazards.remove((StaticHazard)object);
		}
		else if(object.getType() == RoomObject.TIMED_HAZARD)
		{
			timedHazards.remove((TimedHazard)object);
		}
		else if(object.getType() == RoomObject.MOVING_HAZARD)
		{
			movingHazards.remove((MovingHazard)object);
		}
		else if(object.getType() == RoomObject.DOOR)
		{
			doors.remove((Door)object);
		}
		else if(object.getType() == RoomObject.HALLWAY)
		{
			hallways.remove((Hallway)object);
		}
		else if(object.getType() == RoomObject.SPAWNER)
		{
			spawners.remove((Spawner)object);
		}
		else if(object.getType() == RoomObject.ROOM_GOAL)
		{
			// TODO
		}
	}
	
	// ============================================================================================
	// Engine Related Methods
	// ============================================================================================
	
	// Update Objects
	public void updateObjects()
	{
		int index = 0;
		
		for(index = 0; index < timedHazards.size(); index++)
		{
			timedHazards.get(index).update();
		}
		
		for(index = 0; index < movingHazards.size(); index++)
		{
			movingHazards.get(index).update();
		}
		
		for(index = 0; index < spawners.size(); index++)
		{
			spawners.get(index).update();
		}
	}
	
	// CheckObject Collisions
	public void checkCollisionsWith(EntitySprite sprite)
	{
		int index = 0;
		
		if(sprite.getEntity().getType() == Entity.HERO)
		{
			// Check Triggers
			for(index = 0; index < objectTriggers.size(); index ++)
			{
				if(sprite.getHitBox().intersects(objectTriggers.get(index).getBounds()))
				{
					objectTriggers.get(index).activatedBy(sprite.getEntity());
				}
			}
			
			for(index = 0; index < moveTriggers.size(); index ++)
			{
				if(sprite.getHitBox().intersects(moveTriggers.get(index).getBounds()))
				{
					moveTriggers.get(index).activatedBy(sprite.getEntity());
				}
			}
		}

		// Check Hazards		
		for(index = 0; index < staticHazards.size(); index ++)
		{
			if(sprite.getHitBox().intersects(staticHazards.get(index).getBounds()))
			{
				staticHazards.get(index).activatedBy(sprite.getEntity());
			}
		}
		
		for(index = 0; index < timedHazards.size(); index ++)
		{
			if(sprite.getHitBox().intersects(timedHazards.get(index).getBounds()))
			{
				timedHazards.get(index).activatedBy(sprite.getEntity());
			}
		}
		
		for(index = 0; index < movingHazards.size(); index ++)
		{
			if(sprite.getHitBox().intersects(movingHazards.get(index).getBounds()))
			{
				movingHazards.get(index).activatedBy(sprite.getEntity());
			}
		}
	}

	// Get "Ready" Spawners
	public ArrayList<Spawner> getReadySpawns()
	{
		ArrayList<Spawner> readySpawns = new ArrayList<Spawner>(0);
		
		for(int index = 0; index < spawners.size(); index++)
		{
			if(spawners.get(index).spawnReady())
			{
				readySpawns.add(spawners.get(index));		
			}
		}
		
		return readySpawns;
	}
	
	// ============================================================================================
	// Factory Related Methods
	// ============================================================================================
	
	public RoomObject createObjectFromKit(ObjectKit kit)
	{
		RoomObject temp = factory.buildObjectFromKit(kit);
		
		if(temp != null)
		{
			addObject(temp);
			return temp;
		}
		else
		{
			// Error With Kit
			return null;
		}
	}
	
	// ============================================================================================
	// Renderer Related Methods
	// ============================================================================================

	public void initializeRendererImages(String objectSpriteSheetPath)
	{
		renderer.initializeImageData(objectSpriteSheetPath);
	}
	
	public void renderObjects(Graphics gfx)
	{
		renderer.drawObjects(gfx);
	}
	
	// ============================================================================================
	// Getters and Setters
	// ============================================================================================
	
	// Getters
	public ArrayList<ObjectTrigger> getObjectTriggers()
	{
		return objectTriggers;
	}
	public ArrayList<MoveTrigger> getMoveTriggers()
	{
		return moveTriggers;
	}
	public ArrayList<StaticHazard> getStaticHazards()
	{
		return staticHazards;
	}
	public ArrayList<TimedHazard> getTimedHazards()
	{
		return timedHazards;
	}
	public ArrayList<MovingHazard> getMovingHazards()
	{
		return movingHazards;
	}
	public ArrayList<Door> getDoors()
	{
		return doors;
	}
	public ArrayList<Hallway> getHallways()
	{
		return hallways;
	}
	public ArrayList<Spawner> getSpawners()
	{
		return spawners;
	}
	public ArrayList<RoomGoal> getRoomGoals()
	{
		return roomGoals;
	}
	public ArrayList<RoomObject> getAllObjects()
	{
		ArrayList<RoomObject> objects = new ArrayList<RoomObject>(0);
		
		objects.addAll(objectTriggers);
		objects.addAll(moveTriggers);
		objects.addAll(staticHazards);
		objects.addAll(timedHazards);
		objects.addAll(movingHazards);
		objects.addAll(doors);
		objects.addAll(hallways);		
		objects.addAll(spawners);	
		objects.addAll(roomGoals);
	
		return objects;
	}
	
	// Setters
	public void setObjectTriggers(ArrayList<ObjectTrigger> objectTriggers)
	{
		this.objectTriggers = objectTriggers;
	}
	public void setMoveTriggers(ArrayList<MoveTrigger> moveTriggers)
	{
		this.moveTriggers = moveTriggers;
	}
	public void setStaticHazards(ArrayList<StaticHazard> staticHazards)
	{
		this.staticHazards = staticHazards;
	}
	public void setTimedHazards(ArrayList<TimedHazard> timedHazards)
	{
		this.timedHazards = timedHazards;
	}
	public void setMovingHazards(ArrayList<MovingHazard> movingHazards)
	{
		this.movingHazards = movingHazards;
	}
	public void setDoors(ArrayList<Door> doors)
	{
		this.doors = doors;
	}
	public void setHallways(ArrayList<Hallway> hallways)
	{
		this.hallways = hallways;
	}
	public void setSpawners(ArrayList<Spawner> spawners)
	{
		this.spawners = spawners;
	}
	public void setRoomGoals(ArrayList<RoomGoal> roomGoals)
	{
		this.roomGoals = roomGoals;
	}
}
