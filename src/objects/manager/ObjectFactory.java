// ================================================================================================
// Class: ObjectFactory
// Primary Author: Travis Smith
// Last Modified: Dec 26, 2013
// ================================================================================================
// The ObjectFactory will be responsible for returning instanced versions of all RoomObjects. The
// factory is designed to work with ObjectCreationKits, which are essentially blueprints of the
// object to be built. The factory will be capable of reversing this process, taking a RoomObject 
// and returning an ObjectCreationKit that can build it.
// ================================================================================================

package objects.manager;

import java.io.Serializable;

import objects.roomObjects.Door;
import objects.roomObjects.Hallway;
import objects.roomObjects.RoomObject;
import objects.roomObjects.Spawner;
import objects.roomObjects.hazards.MovingHazard;
import objects.roomObjects.hazards.StaticHazard;
import objects.roomObjects.hazards.TimedHazard;
import objects.roomObjects.triggers.MoveTrigger;
import objects.roomObjects.triggers.ObjectTrigger;
import editor.roomModule.objects.ObjectKit;
import editor.roomModule.tileMap.TileData;

public class ObjectFactory implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================

	protected ObjectFactory(){}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected RoomObject buildObjectFromKit(ObjectKit kit)
	{
		if(kit.getObjectType() == RoomObject.OBJ_TRIGGER)
		{
			return buildObjectTrigger(kit);
		}
		else if(kit.getObjectType() == RoomObject.MOVE_TRIGGER)
		{
			return buildMoveTrigger(kit);
		}
		else if(kit.getObjectType() == RoomObject.STATIC_HAZARD)
		{
			return buildStaticHazard(kit);
		}
		else if(kit.getObjectType() == RoomObject.TIMED_HAZARD)
		{
			return buildTimedHazard(kit);
		}
		else if(kit.getObjectType() == RoomObject.MOVING_HAZARD)
		{
			return buildMovingHazard(kit);
		}
		else if(kit.getObjectType() == RoomObject.DOOR)
		{
			return buildDoor(kit);
		}
		else if(kit.getObjectType() == RoomObject.HALLWAY)
		{
			return buildHallway(kit);
		}
		else if(kit.getObjectType() == RoomObject.SPAWNER)
		{
			return buildSpawner(kit);
		}
		else if(kit.getObjectType() == RoomObject.ROOM_GOAL)
		{
			return buildRoomGoal(kit);
		}
		else
		{
			// Error With Kit
			return null;
		}
		
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	// ObjectTrigger
	private RoomObject buildObjectTrigger(ObjectKit kit)
	{
		ObjectTrigger tempTrigger = new ObjectTrigger(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.isUseOnce(), kit.isEnabler(), kit.isDisabler());
//TODO		tempTrigger.setActivateList(kit.getObjTriggerTargets());
		tempTrigger.setLocation(kit.getLocation());
		
		return tempTrigger;
	}
	
	// MoveTrigger
	private RoomObject buildMoveTrigger(ObjectKit kit)
	{
		MoveTrigger tempTrigger = new MoveTrigger(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.isUseOnce());		
//TODO		tempTrigger.setDestination(kit.getMoveTriggerDestID());
		tempTrigger.setLocation(kit.getLocation());
		
		return tempTrigger;
	}
	
	// Static Hazard
	private RoomObject buildStaticHazard(ObjectKit kit)
	{
		StaticHazard tempObject = new StaticHazard(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.getMinDamage(), kit.getMaxDamage());
		tempObject.setLocation(kit.getLocation());

		return tempObject;
	}
	
	// TimedHazard
	private RoomObject buildTimedHazard(ObjectKit kit)
	{
		TimedHazard tempObject = new TimedHazard(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.getMinDamage(), kit.getMaxDamage(), kit.getTimedInterval());
		tempObject.setLocation(kit.getLocation());
		
		return tempObject;
	}
	
	// Moving Hazard
	private RoomObject buildMovingHazard(ObjectKit kit)
	{
		MovingHazard tempObject = new MovingHazard(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.getMinDamage(), kit.getMaxDamage(), kit.getTimedInterval());
		tempObject.setMoves(kit.getMoveInstructions());
		tempObject.setLocation(kit.getLocation());
		
		return tempObject;
	}
	
	// Door
	private RoomObject buildDoor(ObjectKit kit)
	{
		Door tempObject = new Door(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.isVertical());
		tempObject.setLocation(kit.getLocation());
		
		return tempObject;
	}
	
	// Hallway
	private RoomObject buildHallway(ObjectKit kit)
	{
		int height;
		int width;
		
		if(kit.isVertical())
		{
			height = (kit.getHallWidth());
			width = TileData.TILE_WIDTH;
		}
		else
		{
			width = (kit.getHallWidth());
			height = TileData.TILE_HEIGHT;			
		}
		
		Hallway tempObject = new Hallway(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.getHallType(), width, height);
		tempObject.setLocation(kit.getLocation());
		
		return tempObject;
	}
	
	// Spawner
	private RoomObject buildSpawner(ObjectKit kit)
	{
		Spawner tempObject = new Spawner(kit.getIdNumber(), kit.isUsingGraphics(), kit.isStartingEnabled(), kit.getSpawnType(), kit.getWaveSize(), kit.getSpawnRate());
		tempObject.setLocation(kit.getLocation());
		
		return tempObject;
	}
	
	// Room Goals
	private RoomObject buildRoomGoal(ObjectKit kit)
	{
		// TODO
		return null;
	}
}
