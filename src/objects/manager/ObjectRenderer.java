// ================================================================================================
// Class: ObjectRenderer
// Primary Author: Travis Smith
// Last Modified: Dec 26, 2013
// ================================================================================================
// The ObjectRenderer will be responsible for drawing RoomObjects to the screen. It will contain
// the images for objects that the level Generator picked.
//
// Before the renderer will work, the level generator needs to pick which object sprite sheet will
// be used for RoomObjects on the current level. The generator then needs to pass the file path to
// that image so that the renderer can load and draw the appropriate graphics. If the renderer is
// never initialized, then it will not perform any actions.
// ================================================================================================

package objects.manager;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

import objects.roomObjects.Door;
import objects.roomObjects.RoomGoal;
import objects.roomObjects.RoomObject;
import objects.roomObjects.Spawner;
import objects.roomObjects.hazards.Hazard;
import objects.roomObjects.hazards.TimedHazard;
import objects.roomObjects.triggers.MoveTrigger;
import objects.roomObjects.triggers.ObjectTrigger;
import editor.roomModule.tileMap.TileData;

public class ObjectRenderer implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 1L;
	
	// Sprite Location Constants	
	// Object Locations
	public static final int OBJ_TRIG_DIS_X = 0;
	public static final int OBJ_TRIG_ENB_X = 1;
	public static final int OBJ_TRIG_ROW = 2;
	
	public static final int MOV_TRIG_DIS_X = 2;
	public static final int MOV_TRIG_ENB_X = 3;
	public static final int MOV_TRIG_ROW = 2;	
	
	// Hazard Locations	
	public static final int STAT_HAZ_ON_X = 3;
	public static final int STAT_HAZ_ON_Y = 4;
	public static final int STAT_HAZ_OFF_X = 2;
	public static final int STAT_HAZ_OFF_Y = 4;
	
	public static final int TIME_HAZ_ON_X = 1;
	public static final int TIME_HAZ_ON_Y = 4;
	public static final int TIME_HAZ_OFF_X = 0;
	public static final int TIME_HAZ_OFF_Y = 4;
	
	public static final int MOVE_HAZ_ON_X = 1;
	public static final int MOVE_HAZ_ON_Y = 5;
	public static final int MOVE_OFF_OFF_X = 0;
	public static final int MOVE_HAZ_OFF_Y = 5;	
	
	// Door Locations
	public static final int DOOR1_CLOSED_X = 0;
	public static final int DOOR2_CLOSED_X = 1;
	public static final int DOOR1_OPEN_X = 2;
	public static final int DOOR2_OPEN_X = 3;
	public static final int DOOR_HORIZ_ROW = 0;
	public static final int DOOR_VERT_ROW = 1;
	
	// Spawn Locations
	public static final int SPAWN_DIS_X = 2;
	public static final int SPAWN_ENB_X = 3;
	public static final int SPAWN_ROW = 3;
	
	// Image Data
	private BufferedImage objectTiles;
	private boolean hasInitialized;
	
	// Manager
	private ObjectManager manager;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected ObjectRenderer(ObjectManager manager)
	{
		this.hasInitialized = false;
		this.manager = manager;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public boolean hasInitialized()
	{
		return hasInitialized;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void initializeImageData(String imagePath)
	{
		try 
		{
			objectTiles = ImageIO.read(getClass().getResourceAsStream(imagePath));
		} 
		catch(IOException e) 
		{
			this.hasInitialized = false;
			return;
		}	
		
		this.hasInitialized = true;
	}
	
	protected void drawObjects(Graphics gfx)
	{
		if(hasInitialized)
		{
			for(int index = 0; index < manager.getObjectTriggers().size(); index ++)
			{
				drawObjectTrigger(gfx, manager.getObjectTriggers().get(index));
			}
			
			for(int index = 0; index < manager.getMoveTriggers().size(); index ++)
			{
				drawMoveTrigger(gfx, manager.getMoveTriggers().get(index));
			}
			
			for(int index = 0; index < manager.getStaticHazards().size(); index ++)
			{
				drawHazard(gfx, manager.getStaticHazards().get(index));
			}
			
			for(int index = 0; index < manager.getTimedHazards().size(); index ++)
			{
				drawHazard(gfx, manager.getTimedHazards().get(index));
			}
			
			for(int index = 0; index < manager.getMovingHazards().size(); index ++)
			{
				drawHazard(gfx, manager.getMovingHazards().get(index));
			}
			
			for(int index = 0; index < manager.getDoors().size(); index ++)
			{
				drawDoor(gfx, manager.getDoors().get(index));
			}
	
			for(int index = 0; index < manager.getSpawners().size(); index ++)
			{
				drawSpawn(gfx, manager.getSpawners().get(index));
			}
			
			for(int index = 0; index < manager.getRoomGoals().size(); index ++)
			{
				drawRoomGoals(gfx, manager.getRoomGoals().get(index));
			}
		}
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================

	private void drawObjectTrigger(Graphics gfx, ObjectTrigger trigger)
	{
		if(trigger.isEnabled())
		{
			gfx.drawImage(objectTiles, trigger.getLocation().x, trigger.getLocation().y, (trigger.getLocation().x + TileData.TILE_WIDTH), (trigger.getLocation().y + TileData.TILE_HEIGHT)
					, (OBJ_TRIG_ENB_X * TileData.TILE_WIDTH), (OBJ_TRIG_ROW * TileData.TILE_HEIGHT), (((OBJ_TRIG_ENB_X + 1) * TileData.TILE_WIDTH)), (((OBJ_TRIG_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}
		else
		{
			gfx.drawImage(objectTiles, trigger.getLocation().x, trigger.getLocation().y, (trigger.getLocation().x + TileData.TILE_WIDTH), (trigger.getLocation().y + TileData.TILE_HEIGHT)
					, (OBJ_TRIG_DIS_X * TileData.TILE_WIDTH), (OBJ_TRIG_ROW * TileData.TILE_HEIGHT), (((OBJ_TRIG_DIS_X + 1) * TileData.TILE_WIDTH)), (((OBJ_TRIG_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}		
	}
	
	private void drawMoveTrigger(Graphics gfx, MoveTrigger trigger)
	{
		if(trigger.isEnabled())
		{
			gfx.drawImage(objectTiles, trigger.getLocation().x, trigger.getLocation().y, (trigger.getLocation().x + TileData.TILE_WIDTH), (trigger.getLocation().y + TileData.TILE_HEIGHT)
					, (MOV_TRIG_ENB_X * TileData.TILE_WIDTH), (MOV_TRIG_ROW * TileData.TILE_HEIGHT), (((MOV_TRIG_ENB_X + 1) * TileData.TILE_WIDTH)), (((MOV_TRIG_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}
		else
		{
			gfx.drawImage(objectTiles, trigger.getLocation().x, trigger.getLocation().y, (trigger.getLocation().x + TileData.TILE_WIDTH), (trigger.getLocation().y + TileData.TILE_HEIGHT)
					, (MOV_TRIG_DIS_X * TileData.TILE_WIDTH), (MOV_TRIG_ROW * TileData.TILE_HEIGHT), (((MOV_TRIG_DIS_X + 1) * TileData.TILE_WIDTH)), (((MOV_TRIG_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}		
	}
	
	private void drawHazard(Graphics gfx, Hazard hazard)
	{		
		if(hazard.isEnabled())
		{
			if(hazard.getType() == RoomObject.STATIC_HAZARD)
			{
				gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
						, (STAT_HAZ_ON_X * TileData.TILE_WIDTH), (STAT_HAZ_ON_Y * TileData.TILE_HEIGHT), (((STAT_HAZ_ON_X + 1) * TileData.TILE_WIDTH)), (((STAT_HAZ_ON_Y + 1) * TileData.TILE_HEIGHT)), null);
			}
			else if(hazard.getType() == RoomObject.TIMED_HAZARD)
			{
				if(((TimedHazard)hazard).isTurnedOn())
				{
					gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
							, (TIME_HAZ_ON_X * TileData.TILE_WIDTH), (TIME_HAZ_ON_Y * TileData.TILE_HEIGHT), (((TIME_HAZ_ON_X + 1) * TileData.TILE_WIDTH)), (((TIME_HAZ_ON_Y + 1) * TileData.TILE_HEIGHT)), null);	
				}
				else
				{
					gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
							, (TIME_HAZ_OFF_X * TileData.TILE_WIDTH), (TIME_HAZ_OFF_Y * TileData.TILE_HEIGHT), (((TIME_HAZ_OFF_X + 1) * TileData.TILE_WIDTH)), (((TIME_HAZ_OFF_Y + 1) * TileData.TILE_HEIGHT)), null);
				}

			}
			else if(hazard.getType() == RoomObject.MOVING_HAZARD)
			{
				gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
						, (MOVE_HAZ_ON_X * TileData.TILE_WIDTH), (MOVE_HAZ_ON_Y * TileData.TILE_HEIGHT), (((MOVE_HAZ_ON_X + 1) * TileData.TILE_WIDTH)), (((MOVE_HAZ_ON_Y + 1) * TileData.TILE_HEIGHT)), null);
			}
		}
		else
		{
			if(hazard.getType() == RoomObject.STATIC_HAZARD)
			{
				gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
						, (STAT_HAZ_OFF_X * TileData.TILE_WIDTH), (STAT_HAZ_OFF_Y * TileData.TILE_HEIGHT), (((STAT_HAZ_OFF_X + 1) * TileData.TILE_WIDTH)), (((STAT_HAZ_OFF_Y + 1) * TileData.TILE_HEIGHT)), null);
			}
			else if(hazard.getType() == RoomObject.TIMED_HAZARD)
			{
				gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
						, (TIME_HAZ_OFF_X * TileData.TILE_WIDTH), (TIME_HAZ_OFF_Y * TileData.TILE_HEIGHT), (((TIME_HAZ_OFF_X + 1) * TileData.TILE_WIDTH)), (((TIME_HAZ_OFF_Y + 1) * TileData.TILE_HEIGHT)), null);
			}
			else if(hazard.getType() == RoomObject.MOVING_HAZARD)
			{
				gfx.drawImage(objectTiles, hazard.getLocation().x, hazard.getLocation().y, (hazard.getLocation().x + TileData.TILE_WIDTH), (hazard.getLocation().y + TileData.TILE_HEIGHT)
						, (MOVE_OFF_OFF_X * TileData.TILE_WIDTH), (MOVE_HAZ_OFF_Y * TileData.TILE_HEIGHT), (((MOVE_OFF_OFF_X + 1) * TileData.TILE_WIDTH)), (((MOVE_HAZ_OFF_Y + 1) * TileData.TILE_HEIGHT)), null);
			}
		}
			
	}

	private void drawDoor(Graphics gfx, Door door)
	{		
		if(door.isVertical())
		{
			if(door.isEnabled())
			{
				// Draw Top
				gfx.drawImage(objectTiles, door.getLocation().x, door.getLocation().y, (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR1_OPEN_X * TileData.TILE_WIDTH)
						, (DOOR_VERT_ROW * TileData.TILE_HEIGHT), (((DOOR1_OPEN_X + 1) * TileData.TILE_WIDTH)), (((DOOR_VERT_ROW + 1) * TileData.TILE_HEIGHT)), null);
				
				// Draw Bottom
				gfx.drawImage(objectTiles, door.getLocation().x, (door.getLocation().y + TileData.TILE_HEIGHT), (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + (TileData.TILE_HEIGHT * 2)), (DOOR2_OPEN_X * TileData.TILE_WIDTH)
						, (DOOR_VERT_ROW * TileData.TILE_HEIGHT), (((DOOR2_OPEN_X + 1) * TileData.TILE_WIDTH)), (((DOOR_VERT_ROW + 1) * TileData.TILE_HEIGHT)), null);

			}
			else // Closed
			{
				// Draw Top
				gfx.drawImage(objectTiles, door.getLocation().x, door.getLocation().y, (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR1_CLOSED_X * TileData.TILE_WIDTH)
						, (DOOR_VERT_ROW * TileData.TILE_HEIGHT), (((DOOR1_CLOSED_X + 1) * TileData.TILE_WIDTH)), (((DOOR_VERT_ROW + 1) * TileData.TILE_HEIGHT)), null);
				
				// Draw Bottom
				gfx.drawImage(objectTiles, door.getLocation().x, (door.getLocation().y + TileData.TILE_HEIGHT), (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + (TileData.TILE_HEIGHT * 2)), (DOOR2_CLOSED_X * TileData.TILE_WIDTH)
						, (DOOR_VERT_ROW * TileData.TILE_HEIGHT), (((DOOR2_CLOSED_X + 1) * TileData.TILE_WIDTH)), (((DOOR_VERT_ROW + 1) * TileData.TILE_HEIGHT)), null);
			}
		}
		else
		{
			if(door.isEnabled())
			{
				// Draw Left
				gfx.drawImage(objectTiles, door.getLocation().x, door.getLocation().y, (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR1_OPEN_X * TileData.TILE_WIDTH)
						, (DOOR_HORIZ_ROW * TileData.TILE_HEIGHT), (((DOOR1_OPEN_X + 1) * TileData.TILE_WIDTH)), (((DOOR_HORIZ_ROW + 1) * TileData.TILE_HEIGHT)), null);
				
				// Draw Right
				gfx.drawImage(objectTiles, (door.getLocation().x + TileData.TILE_WIDTH), door.getLocation().y, (door.getLocation().x + (TileData.TILE_WIDTH * 2)), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR2_OPEN_X * TileData.TILE_WIDTH)
						, (DOOR_HORIZ_ROW * TileData.TILE_HEIGHT), (((DOOR2_OPEN_X + 1) * TileData.TILE_WIDTH)), (((DOOR_HORIZ_ROW + 1) * TileData.TILE_HEIGHT)), null);

			}
			else
			{
				// Draw Left
				gfx.drawImage(objectTiles, door.getLocation().x, door.getLocation().y, (door.getLocation().x + TileData.TILE_WIDTH), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR1_CLOSED_X * TileData.TILE_WIDTH)
						, (DOOR_HORIZ_ROW * TileData.TILE_HEIGHT), (((DOOR1_CLOSED_X + 1) * TileData.TILE_WIDTH)), (((DOOR_HORIZ_ROW + 1) * TileData.TILE_HEIGHT)), null);
				
				// Draw Right
				gfx.drawImage(objectTiles, (door.getLocation().x + TileData.TILE_WIDTH), door.getLocation().y, (door.getLocation().x + (TileData.TILE_WIDTH * 2)), (door.getLocation().y + TileData.TILE_HEIGHT), (DOOR2_CLOSED_X * TileData.TILE_WIDTH)
						, (DOOR_HORIZ_ROW * TileData.TILE_HEIGHT), (((DOOR2_CLOSED_X + 1) * TileData.TILE_WIDTH)), (((DOOR_HORIZ_ROW + 1) * TileData.TILE_HEIGHT)), null);
			}
		}		
	}
	
	private void drawSpawn(Graphics gfx, Spawner spawn)
	{		
		if(spawn.isEnabled())
		{
			gfx.drawImage(objectTiles, spawn.getLocation().x, spawn.getLocation().y, (spawn.getLocation().x + TileData.TILE_WIDTH), (spawn.getLocation().y + TileData.TILE_HEIGHT)
					, (SPAWN_ENB_X * TileData.TILE_WIDTH), (SPAWN_ROW * TileData.TILE_HEIGHT), (((SPAWN_ENB_X + 1) * TileData.TILE_WIDTH)), (((SPAWN_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}
		else
		{
			gfx.drawImage(objectTiles, spawn.getLocation().x, spawn.getLocation().y, (spawn.getLocation().x + TileData.TILE_WIDTH), (spawn.getLocation().y + TileData.TILE_HEIGHT)
					, (SPAWN_DIS_X * TileData.TILE_WIDTH), (SPAWN_ROW * TileData.TILE_HEIGHT), (((SPAWN_DIS_X + 1) * TileData.TILE_WIDTH)), (((SPAWN_ROW + 1) * TileData.TILE_HEIGHT)), null);
		}
	}
	
	private void drawRoomGoals(Graphics gfx, RoomGoal roomGoal)
	{
		// TODO Auto-generated method stub
		
	}
}
