// ================================================================================================
// Class: Door
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// Doors are RoomObjects that block pathing when "closed" and are activated by an ObjectTrigger.
//
// The boolean member "enabled" is used to tell if the door is open(enabled) or closed(!enabled).
// ================================================================================================

package objects.roomObjects;

import java.awt.Point;
import java.awt.Rectangle;

import entities.Entity;


public class Door extends RoomObject 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Constants
	public static final int LONG_DIMENSION = 128;
	public static final int SHORT_DIMENSION = 64;
	public static final int LONG_PATHING = 96;
	public static final int SHORT_PATHING = 64;

	// Members
	protected boolean isVertical;
	protected Rectangle pathingArea; 
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public Door() 
	{
		super((-1), DOOR, "Door", true, true, LONG_DIMENSION, SHORT_DIMENSION);
		this.isVertical = false;
		this.pathingArea = null;
	}
	
	public Door(int idNumber, boolean useGraphics, boolean startEnabled, boolean isVertical) 
	{
		super(idNumber, DOOR, "Door", useGraphics, startEnabled, LONG_DIMENSION, SHORT_DIMENSION);
		
		if(isVertical)
		{
			this.isVertical = true;
			this.width = SHORT_DIMENSION;
			this.height = LONG_DIMENSION;
			this.pathingArea = null;
		}
		else
		{
			this.isVertical = false;
			this.pathingArea = null;
		}
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void activatedBy(Entity activator) 
	{
		/* Doors Behavior is dictated by its' enabled boolean. To open or close it, enabled
		 * should be set as true or false respectively. 
		 * 
		 * This should normally be done by an ObjectTrigger flagged as enabler or disabler. 		
		 */
	}
	
	// Setters
	public void makeVertical()
	{
		if(!isVertical)
		{
			isVertical = true;
			width = LONG_DIMENSION;
			height = SHORT_DIMENSION;
		
			calculateBounds();
			calculatePathingArea();
		}
	}
	public void makeHorizontal()
	{
		if(isVertical)
		{
			isVertical = false;
			width = SHORT_DIMENSION;
			height = LONG_DIMENSION;
			
			calculateBounds();
			calculatePathingArea();
		}
	}
	@Override
	public void setLocation(Point newLoc)
	{
		currentLocation = newLoc;
		calculateBounds();
		calculatePathingArea();
	}
	
	// Getters
	public boolean isVertical()
	{
		return isVertical;
	}
	public Rectangle getPathingArea()
	{
		return pathingArea;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void calculatePathingArea()
	{
		if(currentLocation != null)
		{
			int startX;
			int startY;
			int pathingBuffer = 16;
		
			if(isVertical)
			{
				startX = currentLocation.x;
				startY = (currentLocation.y + pathingBuffer);
				width = SHORT_PATHING;
				height = LONG_PATHING;
			}
			else
			{
				startX = (currentLocation.x + pathingBuffer);
				startY = currentLocation.y;
				width = LONG_PATHING; 
				height = SHORT_PATHING;
			}	
		
			pathingArea = new Rectangle(startX, startY, width, height);
		}
		else
		{
			pathingArea = null;
		}
	}
}
