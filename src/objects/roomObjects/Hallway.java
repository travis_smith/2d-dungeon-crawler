// ================================================================================================
// Class: Hallway
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// Hallways are RoomObjects that can have varying length, and contain flags that represent if
// the hallway is an entrance/exit to either a DungeonRoom or a entire Level.
//
// Hallways can be flagged as either an entrance hallway or an exit hallway. This tells the level
// generator what order to connect the rooms so that the players are forced through the room the
// way the designers intended to room to be completed. A hallway flagged as NEITHER will be used as
// the level generator dictates.
// ================================================================================================

package objects.roomObjects;

import java.awt.Point;
import java.awt.Rectangle;

import entities.Entity;


public class Hallway extends RoomObject 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Type Constants
	public static final int ENTRANCE = 0;
	public static final int EXIT = 1;
	public static final int EITHER = 2;
	
	// Tile-Map location Constants
	public static final Point LEFT_WALL = new Point(0, 1);
	public static final Point RIGHT_WALL = new Point(2, 1);
	public static final Point TOP_WALL = new Point(1, 0);
	public static final Point BOTTOM_WALL = new Point(1, 2);
	public static final Point FLOOR = new Point(1, 1);
	
	// Object-Map location Constants
	public static final Point HORIZ_ON = new Point(2, 5);
	public static final Point HORIZ_OFF = new Point(3, 5);
	public static final Point VERT_ON = new Point(0, 6);
	public static final Point VERT_OFF = new Point(1, 6);

	// Hall Attributes
	private int hallType;
	private Rectangle pathingArea;
	
	// Flags
	private boolean isLevelExit;
	private boolean exitReached;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public Hallway() 
	{
		super((-1), HALLWAY, "Hallway", true, true, 0, 0);
		
		// Initialize Hall Attributes		
		this.hallType = EITHER;
		this.pathingArea = null;
		
		// Initialize Flags
		this.isLevelExit = false;
		this.exitReached = false;
	}
	
	public Hallway(int idNumber, boolean useGraphics, boolean startEnabled, int hallType, int width, int height) 
	{
		super(idNumber, HALLWAY, "Hallway", useGraphics, startEnabled, width, height);
		
		// Initialize Hall Attributes		
		this.hallType = hallType;
		this.pathingArea = null;
		
		// Initialize Flags
		this.isLevelExit = false;
		this.exitReached = false;
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void activatedBy(Entity activator) 
	{
		if((isLevelExit) && (enabled))
		{
			exitReached = true;
		}
	}
	
	// Setters
	public void setHallType(int newType)
	{
		hallType = newType;
	}
	public void makeLevelExit()
	{
		isLevelExit = true;
	}
	@Override
	public void setWidth(int newWidth)
	{
		width = newWidth;
		calculateBounds();
		calculatePathingArea();
	}
	@Override
	public void setHeight(int newHeight)
	{
		height = newHeight;
		calculateBounds();
		calculatePathingArea();
	}
	@Override
	public void setLocation(Point newLoc)
	{
		currentLocation = newLoc;
		calculateBounds();
		calculatePathingArea();
	}
	
	// Getters
	public int getHallType()
	{
		return hallType;
	}
	public Rectangle getPathingArea()
	{
		return pathingArea;
	}
	public boolean isLevelExit()
	{
		return isLevelExit;
	}
	public boolean exitReached()
	{
		return exitReached;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void calculatePathingArea()
	{
		if(currentLocation != null)
		{
			int pathingOffset = 64;
			int startX;
			int startY;
			int endX;
			int endY;
			
			if(width > height) // Horizontal Hall
			{
				startX = (currentLocation.x + pathingOffset);
				startY = currentLocation.y;
				endX = (currentLocation.x + width - pathingOffset);
				endY = height;
			}
			else // Vertical Hall
			{
				startX = currentLocation.x;
				startY = (currentLocation.y + pathingOffset);
				endX = width;
				endY = (currentLocation.y + height - pathingOffset);
			}
		
			pathingArea = new Rectangle(startX, startY, endX, endY);
		}
		else
		{
			pathingArea = null;
		}
	}
}	

