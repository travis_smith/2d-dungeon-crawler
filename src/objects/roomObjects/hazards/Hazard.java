// ================================================================================================
// Class: Hazard
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// The Hazard class will serve as the Parent class of the various types of Hazards that can be
// placed in game. It will contain shared functions and constants. Hazard is an abstract class 
// that extends the abstract class RoomObject.
//
// The child Trigger types will be used to implement the abstract activated() function.
//
// Trigger Types:
//
//		STATIC_HAZARD:	These hazards are always on.
//
//		TIMED_HAZARD: 	Timed hazards will turn on and off at a set interval.
//				
//		MOVING_HAZARD:	Moving hazards are static hazards that move between two points at a set
//						interval or after being triggered.
//
//	All hazards can be enabled/disabled using an ObjectTrigger.
// ================================================================================================

package objects.roomObjects.hazards;

import java.awt.Point;

import objects.roomObjects.RoomObject;

import combat.CombatMath;

import entities.Entity;

public abstract class Hazard extends RoomObject
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Trigger Size Constants
	public static final int WIDTH= 64;
	public static final int HEIGHT = 64;
	
	// Damage Members
	protected int minDamage;
	protected int maxDamage;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected Hazard(int idNumber, int type, String typeString, boolean useGraphics, boolean startEnabled, int minDamage, int maxDamage)
	{		
		super(idNumber, type, typeString, useGraphics, startEnabled, WIDTH, HEIGHT);
		this.minDamage = minDamage;
		this.maxDamage = maxDamage;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected Point getCenter()
	{
		Point center = new Point(currentLocation.x + (WIDTH /2), currentLocation.y + (HEIGHT / 2));
		
		return center;
	}
	
	protected void knockBack(Entity activator)
	{
		// Temporary Function For Testing Purposes ONLY
		double angle = CombatMath.getAngleTo(getCenter(), activator.getCenter());
		int kbStrength = 64;
		int xMove = (int)(kbStrength * Math.cos(angle));
		int yMove = -(int)(kbStrength * Math.sin(angle));
		activator.setLocation(new Point(activator.getLocation().x + xMove, activator.getLocation().y + yMove));
	}
	
	// ============================================================================================
	// Abstract Methods
	// ============================================================================================
	
	public abstract void update();
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
		
	// Setters
	public void setMinDamage(int newDamage)
	{
		minDamage = newDamage;
	}
	public void setMaxDamage(int newDamage)
	{
		maxDamage = newDamage;
	}
	
	// Getters
	public int getMinDamage()
	{
		return minDamage;
	}
	public int getMaxDamage()
	{
		return maxDamage;
	}
}
