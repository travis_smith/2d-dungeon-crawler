// ================================================================================================
// Class: StaticHazard
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// Static hazards are always on if they are enabled.
// ================================================================================================

package objects.roomObjects.hazards;

import combat.CombatMath;
import entities.Entity;


public class StaticHazard extends Hazard
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;

	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public StaticHazard()
	{
		super((-1), STATIC_HAZARD, "Static Hazard", true, true, 0, 0);
	}
	
	public StaticHazard(int idNumber, boolean usesGraphics, boolean startEnabled, int minDamage, int maxDamage)
	{
		super(idNumber, STATIC_HAZARD, "Static Hazard", usesGraphics, startEnabled, minDamage, maxDamage);
	}
	
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void update()
	{
		// Nothing to Update
	}
	
	@Override
	public void activatedBy(Entity activator) 
	{
		if(enabled && (activator != null))
		{
			int damage = CombatMath.roller(minDamage, maxDamage);
			activator.takeDamage(damage);
			
			//TODO Implement KnockBack
			knockBack(activator);
		}
	}
}
