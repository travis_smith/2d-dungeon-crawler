// ================================================================================================
// Class: TimedHazard
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// When TimedHazards are enabled, they will periodically switch between on and off. When a player
// steps on a TimedHazard, the class will check if it is on or off and act accordingly.
// ================================================================================================

package objects.roomObjects.hazards;


import combat.CombatMath;
import entities.Entity;

public class TimedHazard extends Hazard
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Interval Constants
	public static final int HALF_SECOND = 30;
	public static final int SECOND = 60;
	public static final int TWO_SECONDS = 120;
	
	// Timer Members
	private int interval;
	private int timePassed;
	private boolean turnedOn;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public TimedHazard()
	{
		super((-1), TIMED_HAZARD, "Timed Hazard", true, true, 0, 0);
		this.interval = 0;
		this.timePassed = 0;
		this.turnedOn = false;
	}
	
	public TimedHazard(int idNumber, boolean usesGraphics, boolean startEnabled, int minDamage, int maxDamage, int interval)
	{
		super(idNumber, TIMED_HAZARD, "Timed Hazard", usesGraphics, startEnabled, minDamage, maxDamage);
		this.interval = interval;
		this.timePassed = 0;
		this.turnedOn = false;
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void update()
	{
		if(enabled)
		{
			if(timePassed >= interval)
			{
				if(turnedOn)
				{
					turnedOn = false;
				}
				else
				{
					turnedOn = true;
				}
			
				timePassed = 0;
			}
			else
			{
				timePassed++;
			}	
		}
	}
	
	@Override
	public void activatedBy(Entity activator) 
	{
		if((enabled) && (activator != null) && (turnedOn))
		{
			int damage = CombatMath.roller(minDamage, maxDamage);
			activator.takeDamage(damage);
			
			//TODO Implement KnockBack
			knockBack(activator);
		}
	}
	
	// Setters
	public void setInterval(int newInterval)
	{
		interval = newInterval;
	}
	public void turnHazardOn()
	{
		turnedOn = true;
	}
	public void turnHazardOff()
	{
		turnedOn = false;
	}
	
	// Getters
	public int getInterval()
	{
		return interval;
	}
	public boolean isTurnedOn()
	{
		return turnedOn;
	}
}
