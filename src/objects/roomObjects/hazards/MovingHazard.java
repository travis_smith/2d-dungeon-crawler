// ================================================================================================
// Class: MovingHazard
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// MovingHazards will always be "on", and will contain an array of move instructions. The class
// will also maintain an interval member that will control how quickly the hazard moves from one 
// location to the next.
// ================================================================================================

package objects.roomObjects.hazards;

import java.awt.Point;
import java.util.ArrayList;

import combat.CombatMath;
import entities.Entity;

public class MovingHazard extends Hazard
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Constants
	public static final int SLOW = 2;
	public static final int MEDIUM = 3;
	public static final int FAST = 4;
	
	// Moving Members
	private int moveRate;
	private Point destination;
	private Double moveAngle;
	
	// Move Instructions
	private ArrayList<MoveInstruction> moves;
	private int currentMoveIndex;
	private boolean finalized;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public MovingHazard()
	{
		super((-1), MOVING_HAZARD, "Moving Hazard", true, true, 0, 0);
		this.moveRate = SLOW;
		this.destination = null;
		this.moveAngle = null;
		this.moves = new ArrayList<MoveInstruction>(0);
		this.currentMoveIndex = 0;
		this.finalized = false;
	}
	
	public MovingHazard(int idNumber, boolean usesGraphics, boolean startEnabled, int minDamage, int maxDamage, int moveRate)
	{
		super(idNumber, MOVING_HAZARD, "Moving Hazard", usesGraphics, startEnabled, minDamage, maxDamage);
		this.moveRate = moveRate;
		this.destination = null;
		this.moveAngle = null;
		this.moves = new ArrayList<MoveInstruction>(0);
		this.currentMoveIndex = 0;
		this.finalized = false;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void update()
	{
		if(enabled)
		{			
			if(destination == null)
			{
				if(!getNextDestination())
				{
					// No Move Instructions: Do Nothing
					return;
				}
			}
			
			moveHazard();	
		}
	}
	
	@Override
	public void activatedBy(Entity activator) 
	{
		if((enabled) && (activator != null))
		{
			int damage = CombatMath.roller(minDamage, maxDamage);
			activator.takeDamage(damage);
			
			//TODO Implement KnockBack
			knockBack(activator);
		}
	}
	
	// MoveInstruction Methods
	public void addMoveInstruction(int xMove, int yMove)
	{
		MoveInstruction newInstruction = new MoveInstruction(xMove, yMove);
		moves.add(newInstruction);
	}
	public void deleteMoveInstruction(MoveInstruction instruction)
	{
		moves.remove(instruction);
	}
	public void finalizeMoves()
	{
		// Traverse Path to Find End Point
		Point finish = new Point(currentLocation);
		
		for(int index = 0; index < moves.size(); index++)
		{
			finish.x += moves.get(index).xMove;
			finish.y += moves.get(index).yMove;
		}
		
		// Check if Path is a complete circuit
		if((finish.x == currentLocation.x) && (finish.y == currentLocation.y))
		{
			System.out.println("Finalized: Loop");
		}
		else // Path becomes a 2-way path: The MovingHazard will move to the end, then back to start and repeat.
		{
			int counter = moves.size();
			int newX;
			int newY;
			
			for(int index = (counter - 1); index >= 0; index--)
			{	
				newX = ((-1) * (moves.get(index).xMove));
				newY = ((-1) * (moves.get(index).yMove));
				
				moves.add(new MoveInstruction(newX, newY));
			}
			System.out.println("Finalized: Path");
		}	
		
		finalized = true;
	}
	
	// Setters
	public void setMoves(ArrayList<MoveInstruction> moves)
	{
		this.moves = moves;
	}
	public void setMoveRate(int newRate)
	{
		moveRate = newRate;
	}
	public void resetMoves()
	{
		moves = new ArrayList<MoveInstruction>(0);
		finalized = false;
	}
	
	// Getters
	public int getMoveRate()
	{
		return moveRate;
	}
	public boolean hasFinalized()
	{
		return finalized;
	}
	public ArrayList<MoveInstruction> getMoves()
	{
		return moves;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================

	private boolean getNextDestination()
	{
		if(moves.size() == 0)
		{
			return false;
		}
		
		int newX = currentLocation.x + (moves.get(currentMoveIndex).xMove);
		int newY = currentLocation.y + (moves.get(currentMoveIndex).yMove);
		
		destination = new Point(newX, newY);
		moveAngle = CombatMath.getAngleTo(currentLocation, destination);
		
		currentMoveIndex++;
		
		if(currentMoveIndex >= moves.size())
		{
			currentMoveIndex = 0;
		}
		
		return true;
	}
	
	private void moveHazard()
	{
		double xMove = (moveRate * Math.cos(moveAngle));
		double yMove = -(moveRate * Math.sin(moveAngle));
		
		Point newLoc = new Point((int)(currentLocation.x + xMove), (int)(currentLocation.y + yMove));
		
		setLocation(newLoc);
		
		// Check If Destination Reached
		if(CombatMath.getDistanceTo(currentLocation, destination) <= (moveRate * 2))
		{
			getNextDestination();
		}
	}
}

