package objects.roomObjects.hazards;

import java.io.Serializable;

public class MoveInstruction implements Serializable
{
	// Version
	private static final long serialVersionUID = 1L;
	
	// Members
	public int xMove;
	public int yMove;
	
	public MoveInstruction(int xMove, int yMove)
	{
		this.xMove = xMove;
		this.yMove = yMove;
	}
}
