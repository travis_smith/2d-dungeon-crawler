// ================================================================================================
// Class: RoomObject
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// The RoomObject class will serve as the Parent class of all interactable objects that can be 
// placed inside rooms. This includes triggers, doors, locks, transitions, and spawners. RoomObject
// will handle the location, placement, and graphical data of all subclasses while allowing them to
// implement unique actions based on the subclasses role.
//
// The child Classes will need to implement the abstract activated() function. 
//
// Child Types:
//
//		TRIGGERS:	Triggers are objects that can activate with or without player interaction. They
//					will often contain a reference to a RoomEvent, location, or even another
//					RoomObject and will be able to start or activate them remotely.
//
//		HAZARDS:	Hazards will do damage to any player who comes in contact with them. They can be
//					designed as a static, timed, or moving type.
//
//		DOORS: 		Doors are RoomObjects that block pathing when "closed" and may be set to activate
//					by a player click or remotely from a Trigger. Doors will contain a boolean member
//					call "locked", this member can be manipulated for the desired effect.
//
//		HALLWAYS: 	Halls will be used by the Generator to connect the room to other rooms. Halls
//					flagged as LevelExits will be responsible for telling the game engine to start
//					the next level.
//
//		SPAWNERS:	The Spawners will generate monsters for the player to defeat. They will hold
//					all data related to the spawn "waves" and the AI package attached to them.
//
// ================================================================================================

package objects.roomObjects;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.Serializable;

import entities.Entity;


public abstract class RoomObject implements Serializable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Type Constants
	public static final int SPAWNER = 0;
	public static final int OBJ_TRIGGER = 1;
	public static final int MOVE_TRIGGER = 2;
	public static final int DOOR = 3;
	public static final int HALLWAY = 4;
	public static final int STATIC_HAZARD = 5;
	public static final int TIMED_HAZARD = 6;
	public static final int MOVING_HAZARD = 7;
	public static final int ROOM_GOAL = 8;
	
	// ID and Type
	protected int idNumber;
	protected int type;
	protected String typeString;
	
	// Graphical Data Members
	protected boolean usesGraphics;
	
	// Activation Data Members
	protected boolean enabled;
	
	// Location Data Members
	protected Point currentLocation;
	protected int width;
	protected int height;
	protected Rectangle bounds;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public RoomObject(int idNumber, int type, String typeString, boolean useGraphics, boolean startEnabled, int width, int height)
	{		
		// ID
		this.idNumber = idNumber;
		this.type = type;
		this.typeString = typeString;
		
		// Initialize Graphical Members
		this.usesGraphics = useGraphics;
		
		// Initialize Activation Data Members
		this.enabled = startEnabled;
		
		// Initialize Location Members
		this.currentLocation = null;
		this.width = width;
		this.height = height;
		this.bounds = null;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void calculateBounds()
	{
		if(currentLocation != null)
		{
			bounds = new Rectangle(currentLocation.x, currentLocation.y, width, height);
		}
		else
		{
			bounds = null;
		}
	}
	
	// ============================================================================================
	// Abstract Methods
	// ============================================================================================
	
	public abstract void activatedBy(Entity activator);
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	

	// Setters
	public void setIDNumber(int idNumber)
	{
		this.idNumber = idNumber;
	}
	public void setUsingGraphics(boolean state)
	{
		usesGraphics = state;
	}
	public void setEnabled(boolean state)
	{
		enabled = state;
	}
	public void setLocation(Point newLoc)
	{
		currentLocation = newLoc;
		calculateBounds();
	}
	public void setWidth(int newWidth)
	{
		width = newWidth;
		calculateBounds();
	}
	public void setHeight(int newHeight)
	{
		height = newHeight;
		calculateBounds();
	}

	// Getters
	public int getID()
	{
		return idNumber;
	}
	public int getType()
	{
		return type;
	}
	public String getTypeString()
	{
		return typeString;
	}
	public boolean usesGraphics()
	{
		return usesGraphics;
	}
	public boolean isEnabled()
	{
		return enabled;
	}
	public Point getLocation()
	{
		return currentLocation;
	}
	public Rectangle getBounds()
	{
		return bounds;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
}
