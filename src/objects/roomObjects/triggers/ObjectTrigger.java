// ================================================================================================
// Class: ObjectTrigger
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// The ObjectTrigger is a Trigger that is attached to an arry of RoomObjects and will call their
// activate() function when it is activated.
//
// ObjectTriggers contains two boolean members called "enabler" and "disabler". These two booleans
// cannot both be true, however they can both be false. If enabler is true, this trigger will 
// enable its' object before calling activate, useful for turning on hazards or opening doors.
// If disabler is true, the trigger will disable its' object, useful for turning off hazards or
// closing doors. When both are set to false, the trigger will simplpy call the objects activate
// function.
// ================================================================================================

package objects.roomObjects.triggers;

import java.util.ArrayList;

import objects.roomObjects.RoomObject;

import entities.Entity;


public class ObjectTrigger extends Trigger 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Data Members
	private ArrayList<RoomObject> activateList;
	private boolean enabler;
	private boolean disabler;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public ObjectTrigger() 
	{
		super((-1), OBJ_TRIGGER, "Object Trigger", true, true, true);
		this.activateList = new ArrayList<RoomObject>(0);
		this.enabler = false;
		this.disabler = false;
	}
	
	public ObjectTrigger(int idNumber, boolean usesGraphics, boolean startEnabled, boolean useOnce, boolean enabler, boolean disabler)
	{
		super(idNumber, OBJ_TRIGGER, "Object Trigger", usesGraphics, startEnabled, useOnce);
		this.activateList = new ArrayList<RoomObject>(0);
		this.enabler = enabler;
		this.disabler = disabler;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	@Override
	public void activatedBy(Entity activator) 
	{
		if(enabled && (activateList.size() > 0))
		{
			for(int index = 0; index < activateList.size(); index++)
			{
				if(enabler)
				{
					activateList.get(index).setEnabled(true);
					activateList.get(index).activatedBy(activator);
				}	
				else if(disabler)
				{
					activateList.get(index).setEnabled(false);
					activateList.get(index).activatedBy(activator);
				}
				else
				{
					activateList.get(index).activatedBy(activator);	
				}
			}
			
			if(useOnce)
			{
				enabled = false;
			}
		}
		else if(enabled)
		{
			if(useOnce)
			{
				enabled = false;
			}
		}
	}
	
	// Object Methods
	public void addObjectToActivateList(RoomObject newObject)
	{
		activateList.add(newObject);
	}
	public void removeObjectFromActivateList(RoomObject object)
	{
		activateList.remove(object);
	}
	
	// Setters
	public void setActivateList(ArrayList<RoomObject> objects)
	{
		this.activateList = objects;
	}
	public void setEnabling(boolean state)
	{
		enabler = state;
	}
	public void setDisabling(boolean state)
	{
		disabler = state;
	}
	
	// Getters
	public ArrayList<RoomObject> getActivateList()
	{
		return activateList;
	}
	public boolean willEnable()
	{
		return enabler;
	}
	public boolean willDisable()
	{
		return disabler;
	}
}
