// ================================================================================================
// Class: Trigger
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// The Trigger class will serve as the Parent class of the various types of Triggers that can be
// placed in game. It will contain shared functions and constants. Trigger is an abstract class 
// that extends the abstract class RoomObject.
//
// The child Trigger types will be used to implement the abstract activated() function.
//
// Trigger Types:
//
//		OBJECT_TRIGGER:	These Triggers will be used to interact with a RoomObject such as a door or
//						spawner. When activated, the Trigger will pass the relevant
//						information and the RoomObject will perform its' action. 
//
//		MOVE_TRIGGER: 	These Triggers will move the entity that activates the them to a preset
//						location. 
// ================================================================================================

package objects.roomObjects.triggers;

import objects.roomObjects.RoomObject;

public abstract class Trigger extends RoomObject
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Trigger Size Constants
	public static final int WIDTH= 64;
	public static final int HEIGHT = 64;
	
	// Activation Data Members
	protected boolean useOnce;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected Trigger(int idNumber, int type, String typeString, boolean useGraphics, boolean startEnabled, boolean useOnce)
	{
		super(idNumber, type, typeString, useGraphics, startEnabled, WIDTH, HEIGHT);
		this.useOnce = useOnce;
	}
		
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	// Setters
	public void setOneTimeUse(boolean flag)
	{
		useOnce = flag;
	}
	
	// Getters
	public boolean isOneTimeUse()
	{
		return useOnce;
	}
}
