// ================================================================================================
// Class: MoveTrigger
// Primary Author: Travis Smith
// Last Updated: Dec 20, 2013
// ================================================================================================
// The MoveTrigger is a Trigger that will move the entity that activates them to a preset 
// destination. 
// ================================================================================================

package objects.roomObjects.triggers;

import entities.Entity;

public class MoveTrigger extends Trigger 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Destination
	private MoveTrigger destination;
	
	// Cooldown
	private static final int COOL_DOWN = 120;	// One Second Wait for every 60
	private int callsSinceLast;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public MoveTrigger()
	{
		super((-1), MOVE_TRIGGER, "Move Trigger", true, true, true);	
		this.destination = null;
		this.callsSinceLast = 0;
	}
	
	public MoveTrigger(int idNumber, boolean usesGraphics, boolean startEnabled, boolean useOnce)
	{
		super(idNumber, MOVE_TRIGGER, "Move Trigger", usesGraphics, startEnabled, useOnce);
		this.destination = null;
		this.callsSinceLast = 0;
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================

	@Override
	public void activatedBy(Entity activator) 
	{
		if(enabled)
		{
			if((destination != null) && (activator != null))
			{
				if(this.callsSinceLast >= COOL_DOWN)
				{
					activator.setLocation(destination.getLocation());
					this.callsSinceLast = 0;
					destination.callsSinceLast = 0;
					
					if(useOnce)
					{
						enabled = false;
					}
				}
				else
				{
					this.callsSinceLast++;
				}
			}
		}
	}
	
	// Setters
	public void setDestination(MoveTrigger newDest)
	{
		destination = newDest;
	}
	
	// Getters
	public MoveTrigger getDestination()
	{
		return destination;
	}
}
