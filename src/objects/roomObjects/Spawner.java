// ================================================================================================
// Class: Spawner
// Primary Author: Travis Smith
// Last Modified: Dec 20, 2013
// ================================================================================================
// The Spawner class will hold the data needed to generate monsters in game. When activated, they 
// will increment a counter that flags "spawnReady" once it reaches the specified rate. When the
// spawnReady flag is set, the engine should get the Monster data from the spawner and use it to 
// create monsters and place them on the game map.
// ================================================================================================

package objects.roomObjects;

import entities.Entity;

public class Spawner extends RoomObject
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Version
	private static final long serialVersionUID = 2L;
	
	// Size Constants
	public final static int WIDTH = 64;
	public final static int HEIGHT = 64;
	
	// Mob Type Constants
	public static final int MINION = 0;
	public static final int CAPTAIN = 1;
	public static final int BOSS = 2;
	
	// Spawning Data
	private int waveSize;
	private int spawnRate;
	private int spawnType;
	private int updatesSinceLastSpawn;
	private boolean spawnReady;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================

	public Spawner()
	{
		super((-1), SPAWNER, "Spawner", true, true, WIDTH, HEIGHT);
		
		// Initialize Spawning Data
		this.waveSize = -1;
		this.spawnRate = -1;
		this.spawnType = -1;
		this.updatesSinceLastSpawn = 0;
		this.spawnReady = false;
	}
	
	public Spawner(int idNumber, boolean useGraphics, boolean startEnabled, int spawnType, int waveSize, int spawnRate) 
	{
		super(idNumber, SPAWNER, "Spawner", useGraphics, startEnabled, WIDTH, HEIGHT);
		
		// Initialize Spawning Data
		this.spawnType = spawnType;
		this.waveSize = waveSize;
		this.spawnRate = spawnRate;
		this.updatesSinceLastSpawn = 0;
		this.spawnReady = false;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public void update()
	{
		if((enabled) && (!spawnReady))
		{
			updatesSinceLastSpawn += 1;
		
			if(updatesSinceLastSpawn >= spawnRate)
			{
				spawnReady = true;
			}
		}
	}
	
	@Override
	public void activatedBy(Entity activator)
	{
		// Spawners Are not Activated
	}
	
	public void reset()
	{
		updatesSinceLastSpawn = 0;
		spawnReady = false;
	}
	
	// Setters
	public void setWaveSize(int size)
	{
		waveSize = size;
	}	
	public void setSpawnRate(int rate)
	{
		spawnRate = rate;
	}	
	public void setSpawnType(int type)
	{
		spawnType = type;
	}
	public boolean spawnReady()
	{
		return spawnReady;
	}
	
	// Getters
	public int getWaveSize()
	{
		return waveSize;
	}
	public int getSpawnRate()
	{
		return spawnRate;
	}
	public int getSpawnType()
	{
		return spawnType;
	}
}
